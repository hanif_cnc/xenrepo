import os
import cv2
import csv
from concurrent import futures
from tqdm import tqdm
from glare import GlareChannel

DIR_TRUE_GLARE = 'F:/Xendity/glare_detection/211108_dataset/glare_img'
DIR_NO_GLARE = 'F:/Xendity/glare_detection/211108_dataset/no_glare_img'
CSV_REPORT = 'bright_glare_result.csv'
# CONFIG_CSV = 'temp.csv'


def proc_glare(proc):
    bright_contrast = proc[0][0]
    crit_area = proc[0][1]
    print(f"Running proc: {proc[1]}")

    # prepare dict for bright contrast
    bright_contrast_dict = {"bright_min": bright_contrast[0],
                            "bright_max": 255, "contrast_min": 0,
                            "contrast_max": bright_contrast[1]}

    # prepare dict for critical area
    crit_dict = {"address": crit_area[0],
                 "name": crit_area[1],
                 "ic": crit_area[2],
                 "gray": crit_area[3],
                 "face": crit_area[4],
                 "nation": crit_area[5],
                 "all_area": crit_area[6]}

    # Examine True Glare images
    img_true_verdict = []
    for each_img in os.listdir(DIR_TRUE_GLARE):
        image_path = f'{DIR_TRUE_GLARE}/{each_img}'
        image_path = image_path.replace('\\', '/')
        image = cv2.imread(image_path)
        __, image_result, __, __ = GlareChannel(image, bright_contrast_dict, crit_dict).check_glare()

        report_list = ['0' for i in range(16)]
        report_list[0] = each_img
        for each_dict in image_result:
            if each_dict == "address":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[1] = val
                report_list[2] = verdict
            elif each_dict == "name":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[3] = val
                report_list[4] = verdict
            elif each_dict == "ic":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[5] = val
                report_list[6] = verdict
            elif each_dict == "gray":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[7] = val
                report_list[8] = verdict
            elif each_dict == "face":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[9] = val
                report_list[10] = verdict
            elif each_dict == "nation":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[11] = val
                report_list[12] = verdict
            elif each_dict == "all_area":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[13] = val
                report_list[14] = verdict
            elif each_dict == "final":
                verdict = image_result[each_dict].get('verdict')
                report_list[15] = verdict
                img_true_verdict.append(verdict)

    TP = len([i for i in img_true_verdict if i])
    FN = len([i for i in img_true_verdict if not i])

    # Examine Image with no glare
    img_false_verdict = []
    for each_img in os.listdir(DIR_NO_GLARE):
        image_path = f'{DIR_NO_GLARE}/{each_img}'
        image_path = image_path.replace('\\', '/')
        image = cv2.imread(image_path)

        __, image_result, __, __ = GlareChannel(image, bright_contrast_dict, crit_dict).check_glare()

        report_list = ['0' for i in range(16)]
        report_list[0] = each_img
        for each_dict in image_result:
            if each_dict == "address":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[1] = val
                report_list[2] = verdict
            elif each_dict == "name":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[3] = val
                report_list[4] = verdict
            elif each_dict == "ic":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[5] = val
                report_list[6] = verdict
            elif each_dict == "gray":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[7] = val
                report_list[8] = verdict
            elif each_dict == "face":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[9] = val
                report_list[10] = verdict
            elif each_dict == "nation":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[11] = val
                report_list[12] = verdict
            elif each_dict == "all_area":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[13] = val
                report_list[14] = verdict
            elif each_dict == "final":
                verdict = image_result[each_dict].get('verdict')
                report_list[15] = verdict
                img_false_verdict.append(verdict)

    TN = len([i for i in img_false_verdict if not i])
    FP = len([i for i in img_false_verdict if i])

    # Calculate metrics
    try:
        precision = TP / (TP + FP)
    except ZeroDivisionError:
        precision = 0
    try:
        recall = TP / (TP + FN)
    except ZeroDivisionError:
        recall = 0
    try:
        f1_score = (2 * precision * recall) / (precision + recall)
    except ZeroDivisionError:
        f1_score = 0
    try:
        far_score = FP / (FP + TN)
    except ZeroDivisionError:
        far_score = 0
    try:
        frr_score = FN / (TP + FN)
    except ZeroDivisionError:
        frr_score = 0

    result_list_by_category = [crit_dict["address"], crit_dict["name"], crit_dict["ic"],
                               crit_dict["gray"], crit_dict["face"], crit_dict["nation"],
                               crit_dict["all_area"], bright_contrast_dict["bright_max"],
                               bright_contrast_dict["bright_min"], bright_contrast_dict["contrast_max"],
                               bright_contrast_dict["contrast_min"],
                               TP, FP, FN, TN, precision, recall, f1_score, far_score, frr_score]

    return result_list_by_category


def main():
    # Prepare empty csv
    with open(CSV_REPORT, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["address_percent", "name_percent", "ic_percent", "gray_percent", "face_percent",
                         "nation_percent", "all_area_percent", "max_brightness", "min_brightness",
                         "max_contrast", "min_contrast", "TP", "FP", "FN", "TN", "Precision", "Recall", "F1-score",
                         "FAR", "FRR"])

    # with open(CONFIG_CSV, 'w', newline='') as csv_file:
    #     writer = csv.writer(csv_file, delimiter=',')
    #     writer.writerow(["max_brightness", "min_contrast", "address_percent", "name_percent", "ic_percent",
    #                      "gray_percent", "face_percent",
    #                      "nation_percent", "all_area_percent"])

    print("Preparing custom settings...")
    # Prepare custom brightness and contrast
    # bright 20 contrast 30
    brightness_list = [250 - i*10 for i in range(10)]
    contrast_list = [10 + i*10 for i in range(15)]
    bright_contrast_list = [[i, a] for i in brightness_list for a in contrast_list]
    print("Brightness and contrast done")

    # Prepare custom critical precentage area
    # Standard crit list is for every area except face
    # Std = 6 face = 10
    std_list = [round(0.005 + i*0.005, 3) for i in range(4)]
    face_list = [round(0.03 + i*0.01, 3) for i in range(3)]
    # crit_list = [[add, name, ic, gray, face, nation, area] for add in std_list for name in
    #              std_list for ic in std_list for gray in std_list for face in face_list
    #              for nation in std_list for area in std_list]
    crit_list = [[item, item, item, item, face, item, item2] for item in std_list for face in face_list for
                 item2 in std_list]
    print("Critical area done")

    # Group all list
    print("Grouping for all config...")
    # for a in tqdm(bright_contrast_list):
    #     for b in crit_list:
    #         combine_list = a + b
    #         with open(CONFIG_CSV, 'a', newline='') as csv_file:
    #             writer = csv.writer(csv_file, delimiter=',')
    #             writer.writerow(combine_list)
    proc_list1 = [[a, b] for a in bright_contrast_list for b in crit_list]
    proc_list = []
    for idx, each_item in enumerate(proc_list1):
        proc_list.append([each_item, idx])
    print("All config grouped and prepared")

    # all_result = []
    # print("Starting process")
    # for each_item in tqdm(range(2)):
    #     result = proc_glare(proc_list[each_item])
    #     all_result.append(result)
    print(f"Total configurations to be tested: {len(proc_list):,}")
    print("Starting process...")
    with futures.ProcessPoolExecutor() as executor:
        all_result = list(tqdm(executor.map(proc_glare, proc_list), total=len(proc_list)))

    with futures.ThreadPoolExecutor() as executor:
        all_result = list(tqdm(executor.map(proc_glare, proc_list), total=len(proc_list)))

    # all_result = [proc_glare(proc_list[0])]


    with open(CSV_REPORT, 'a', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        for each_report in all_result:
            writer.writerow(each_report)


if __name__ == '__main__':
    main()
