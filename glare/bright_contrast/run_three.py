import os
import cv2
import csv
import numpy as np
from tqdm import tqdm
from glare import GlareChannel
from collections import Counter


IN_IMG_DIR_TRUE = 'F:/Xendity/glare_detection/211108_dataset/glare_img'
IN_IMG_DIR_FALSE = 'F:/Xendity/glare_detection/211108_dataset/no_glare_img'
OUT_IMG_DIR_TRUE_API = 'F:/Xendity/glare_detection/211108_dataset/result_glare_bright_satur/True_glare_out_API'
OUT_IMG_DIR_FALSE_API = 'F:/Xendity/glare_detection/211108_dataset/result_glare_bright_satur/No_glare_out_API'
OUT_IMG_DIR_TRUE_INT = 'F:/Xendity/glare_detection/211108_dataset/result_glare_bright_satur/True_glare_out_INT'
OUT_IMG_DIR_FALSE_INT = 'F:/Xendity/glare_detection/211108_dataset/result_glare_bright_satur/No_glare_out_INT'
OUT_REPORT_TRUE = 'F:/Xendity/glare_detection/211108_dataset/result_glare_bright_satur/true_glare_data.csv'
OUT_REPORT_FALSE = 'F:/Xendity/glare_detection/211108_dataset/result_glare_bright_satur/no_glare_data.csv'
OUT_OVERALL_REPORT = 'F:/Xendity/glare_detection/211108_dataset/' \
                     'result_glare_bright_satur/bright_satur_overall_report.csv'


def main():
    os.makedirs(OUT_IMG_DIR_TRUE_API, exist_ok=True)
    os.makedirs(OUT_IMG_DIR_FALSE_API, exist_ok=True)
    os.makedirs(OUT_IMG_DIR_TRUE_INT, exist_ok=True)
    os.makedirs(OUT_IMG_DIR_FALSE_INT, exist_ok=True)

    with open(OUT_REPORT_TRUE, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["image", "address_percent_glare", "address_verdict",
                         "name_percent_glare", "name_verdict",
                         "ic_percent_glare", "ic_verdict",
                         "gray_percent_glare", "gray_verdict",
                         "face_percent_glare", "face_verdict",
                         "nation_percent_glare", "nation_verdict",
                         "overall_percent_glare", "overall_verdict",
                         "final_verdict"])

    with open(OUT_REPORT_FALSE, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["image", "address_percent_glare", "address_verdict",
                         "name_percent_glare", "name_verdict",
                         "ic_percent_glare", "ic_verdict",
                         "gray_percent_glare", "gray_verdict",
                         "face_percent_glare", "face_verdict",
                         "nation_percent_glare", "nation_verdict",
                         "overall_percent_glare", "overall_verdict",
                         "final_verdict"])

    # Examine True image glare
    img_true_verdict = []
    overall_proc_time = []
    print("Processing True Glare images...")
    for each_img in tqdm(os.listdir(IN_IMG_DIR_TRUE)):
        image_path = f'{IN_IMG_DIR_TRUE}/{each_img}'
        image_path = image_path.replace('\\', '/')
        image = cv2.imread(image_path)
        img_api, image_result, image_internal, proc_time = GlareChannel(image).check_glare()
        overall_proc_time.append(proc_time)

        img_output_int_path = f'{OUT_IMG_DIR_TRUE_INT}/{each_img}'
        cv2.imwrite(img_output_int_path, image_internal)

        img_output_api_path = f'{OUT_IMG_DIR_TRUE_API}/{each_img}'
        cv2.imwrite(img_output_api_path, img_api)

        report_list = ['0' for i in range(16)]
        report_list[0] = each_img
        for each_dict in image_result:
            if each_dict == "address":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[1] = val
                report_list[2] = verdict
            elif each_dict == "name":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[3] = val
                report_list[4] = verdict
            elif each_dict == "ic":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[5] = val
                report_list[6] = verdict
            elif each_dict == "gray":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[7] = val
                report_list[8] = verdict
            elif each_dict == "face":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[9] = val
                report_list[10] = verdict
            elif each_dict == "nation":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[11] = val
                report_list[12] = verdict
            elif each_dict == "all_area":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[13] = val
                report_list[14] = verdict
            elif each_dict == "final":
                verdict = image_result[each_dict].get('verdict')
                report_list[15] = verdict
                img_true_verdict.append(verdict)

        with open(OUT_REPORT_TRUE, 'a', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            writer.writerow(report_list)

    TP = 0
    FN = 0
    for each_key, each_val in zip(Counter(img_true_verdict).keys(), Counter(img_true_verdict).values()):
        if each_key is True:
            TP = each_val
        elif each_key is False:
            FN = each_val

    # Examine Image with no glare
    img_false_verdict = []
    print("Processing No Glare images...")
    for each_img in tqdm(os.listdir(IN_IMG_DIR_FALSE)):
        image_path = f'{IN_IMG_DIR_FALSE}/{each_img}'
        image_path = image_path.replace('\\', '/')
        image = cv2.imread(image_path)

        img_api, image_result, image_internal, proc_time = GlareChannel(image).check_glare()
        overall_proc_time.append(proc_time)

        img_output_int_path = f'{OUT_IMG_DIR_FALSE_INT}/{each_img}'
        cv2.imwrite(img_output_int_path, image_internal)

        img_output_api_path = f'{OUT_IMG_DIR_FALSE_API}/{each_img}'
        cv2.imwrite(img_output_api_path, img_api)

        report_list = ['0' for i in range(16)]
        report_list[0] = each_img
        for each_dict in image_result:
            if each_dict == "address":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[1] = val
                report_list[2] = verdict
            elif each_dict == "name":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[3] = val
                report_list[4] = verdict
            elif each_dict == "ic":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[5] = val
                report_list[6] = verdict
            elif each_dict == "gray":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[7] = val
                report_list[8] = verdict
            elif each_dict == "face":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[9] = val
                report_list[10] = verdict
            elif each_dict == "nation":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[11] = val
                report_list[12] = verdict
            elif each_dict == "all_area":
                val = float(image_result[each_dict].get('val'))
                val = f'{val:.3f}'
                verdict = image_result[each_dict].get('verdict')
                report_list[13] = val
                report_list[14] = verdict
            elif each_dict == "final":
                verdict = image_result[each_dict].get('verdict')
                report_list[15] = verdict
                img_false_verdict.append(verdict)

        with open(OUT_REPORT_FALSE, 'a', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            writer.writerow(report_list)

    TN = 0
    FP = 0
    for each_key, each_val in zip(Counter(img_false_verdict).keys(), Counter(img_false_verdict).values()):
        if each_key is True:
            FP = each_val
        elif each_key is False:
            TN = each_val

    # Calculate metrics
    precision = TP / (TP + FP)
    recall = TP / (TP + FN)
    f1_score = (2 * precision * recall) / (precision + recall)
    far_score = FP / (FP + TN)
    frr_score = FN / (TP + FN)

    # Calculate time taken
    arr_time_list = np.array(overall_proc_time)
    average_time = np.mean(arr_time_list)
    std_time = np.std(arr_time_list)

    result_list_by_category = [TP, FP, FN, TN, precision, recall, f1_score, far_score, frr_score, average_time,
                               std_time]

    with open(OUT_OVERALL_REPORT, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["TP", "FP", "FN", "TN", "Precision", "Recall", "F1-score",
                         "FAR", "FRR", "Average Time(secs)", "Std. Dev Time (secs)"])
        writer.writerow(result_list_by_category)


if __name__ == '__main__':
    main()

