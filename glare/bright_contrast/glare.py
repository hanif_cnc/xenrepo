import cv2
import numpy as np
import configparser
from time import time


class GlareChannel:

    def __init__(self, img, custom_threshold=False, custom_percent=False, config_path='glare_config.ini'):
        if custom_threshold is None:
            custom_threshold = {"": ""}
        self.img = img
        self.img_hsv = cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV)

        self.config = configparser.ConfigParser()
        self.config_path = config_path
        self.crit_area_dict = self.get_config()

        # Get thresholds
        self.bckgrd_excld = float(self.config['CroppingBackground']['BackgroundRatio'])
        self.max_bright = int(self.config['Brightness']['Maximum'])
        self.min_bright = int(self.config['Brightness']['Minimum'])
        self.max_saturate = int(self.config['Saturate']['Maximum'])
        self.min_saturate = int(self.config['Saturate']['Minimum'])

        if custom_threshold:
            self.max_bright = int(custom_threshold["bright_max"])
            self.min_bright = int(custom_threshold["bright_min"])
            self.max_saturate = int(custom_threshold["contrast_max"])
            self.min_saturate = int(custom_threshold["contrast_min"])

        if custom_percent:
            self.crit_area_dict['address']['glare_pct'] = custom_percent['address']
            self.crit_area_dict['name']['glare_pct'] = custom_percent['name']
            self.crit_area_dict['ic']['glare_pct'] = custom_percent['ic']
            self.crit_area_dict['gray']['glare_pct'] = custom_percent['gray']
            self.crit_area_dict['face']['glare_pct'] = custom_percent['face']
            self.crit_area_dict['nation']['glare_pct'] = custom_percent['nation']
            self.crit_area_dict['all_area']['glare_pct'] = custom_percent['all_area']

        # print("-------------------")
        # print(self.crit_area_dict)
        # print(self.max_bright)
        # print(self.min_bright)
        # print(self.max_saturate)
        # print(self.min_saturate)


    def get_config(self):
        self.config.read(self.config_path)

        point_dict = {
            'address': {
                'height_p1': self.config['CroppingAddress']['HeightP1'],
                'width_p1': self.config['CroppingAddress']['WidthP1'],
                'height_p2': self.config['CroppingAddress']['HeightP2'],
                'width_p2': self.config['CroppingAddress']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['AddressAreaPercent'],
            },

            'name': {
                'height_p1': self.config['CroppingName']['HeightP1'],
                'width_p1': self.config['CroppingName']['WidthP1'],
                'height_p2': self.config['CroppingName']['HeightP2'],
                'width_p2': self.config['CroppingName']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['NameAreaPercent'],
            },

            'ic': {
                'height_p1': self.config['CroppingIc']['HeightP1'],
                'width_p1': self.config['CroppingIc']['WidthP1'],
                'height_p2': self.config['CroppingIc']['HeightP2'],
                'width_p2': self.config['CroppingIc']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['IcAreaPercent'],
            },

            'gray': {
                'height_p1': self.config['CroppingGray']['HeightP1'],
                'width_p1': self.config['CroppingGray']['WidthP1'],
                'height_p2': self.config['CroppingGray']['HeightP2'],
                'width_p2': self.config['CroppingGray']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['GrayAreaPercent'],
            },

            'face': {
                'height_p1': self.config['CroppingFace']['HeightP1'],
                'width_p1': self.config['CroppingFace']['WidthP1'],
                'height_p2': self.config['CroppingFace']['HeightP2'],
                'width_p2': self.config['CroppingFace']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['FaceAreaPercent'],
            },

            'nation': {
                'height_p1': self.config['CroppingNation']['HeightP1'],
                'width_p1': self.config['CroppingNation']['WidthP1'],
                'height_p2': self.config['CroppingNation']['HeightP2'],
                'width_p2': self.config['CroppingNation']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['NationAreaPercent'],
            },

            'all_area': {
                'background_ratio': self.config['CroppingBackground']['BackgroundRatio'],
                'glare_pct': self.config['AreaThreshold']['OverallAreaPercent']
            }

        }

        return point_dict

    @staticmethod
    def extract_crit_coord(ratio_dict, each_dict, height, width):
        x1 = float(ratio_dict[each_dict].get('height_p1'))
        x2 = float(ratio_dict[each_dict].get('height_p2'))
        y1 = float(ratio_dict[each_dict].get('width_p1'))
        y2 = float(ratio_dict[each_dict].get('width_p2'))

        height_1 = int(height * x1)
        height_2 = int(height * x2)
        width_1 = int(width * y1)
        width_2 = int(width * y2)

        # Sort values for crop points
        height_list = [height_1, height_2]
        width_list = [width_1, width_2]
        height_list.sort()
        width_list.sort()

        return height_list, width_list

    def calculate_glare(self, img):
        result_dict = {}
        height, width = img.shape

        for each_dict in self.crit_area_dict:
            if each_dict != 'all_area':
                height_list, width_list = self.extract_crit_coord(self.crit_area_dict, each_dict, height, width)
                crit_img_crop = self.crop_img(img, (height_list[0], height_list[1], width_list[0], width_list[1]))
                unique, counts = np.unique(crit_img_crop, return_counts=True)
                pixel_count = dict(zip(unique, counts))

                total_white = 0
                if 255 in pixel_count:
                    total_white = pixel_count[255]

                try:
                    total_black = pixel_count[0]
                except KeyError:
                    total_black = 0
                percentage_glare = total_white / (total_black + total_white)

                glare_verdict = False
                if percentage_glare > float(self.crit_area_dict[each_dict].get('glare_pct')):
                    glare_verdict = True

                result_dict[f'{each_dict}'] = {'val': 0, 'verdict': 0}
                result_dict[f'{each_dict}']['val'] = percentage_glare
                result_dict[f'{each_dict}']['verdict'] = glare_verdict

            else:

                unique, counts = np.unique(img, return_counts=True)
                pixel_count = dict(zip(unique, counts))

                total_white = 0
                if 255 in pixel_count:
                    total_white = pixel_count[255]

                total_black = pixel_count[0]
                percentage_glare = total_white / (total_black + total_white)

                glare_verdict = False
                if percentage_glare > float(self.crit_area_dict[each_dict].get('glare_pct')):
                    glare_verdict = True

                result_dict[f'{each_dict}'] = {'val': 0, 'verdict': 0}
                result_dict[f'{each_dict}']['val'] = percentage_glare
                result_dict[f'{each_dict}']['verdict'] = glare_verdict

        return result_dict

    @staticmethod
    def brightness_mask(image):
        mask = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return mask

    @staticmethod
    def saturation_mask(image):
        mask = image[:, :, 1]
        # Inverse the value of saturation to match the relation
        # with brightness mask
        # full_mask = np.full(mask.shape, 255, 'uint8')
        # mask = full_mask - mask
        return mask

    @staticmethod
    def crop_img(img, crop_tuple: tuple):
        img = img[crop_tuple[0]: crop_tuple[1], crop_tuple[2]: crop_tuple[3]]
        return img

    @staticmethod
    def draw_box(img, start_point, end_point):
        color = (255, 0, 0)
        thickness = 2
        img = cv2.rectangle(img, start_point, end_point, color, thickness)
        return img

    def crop_coord_background(self, image):
        height, width, channel = image.shape

        height_1 = int(height * self.bckgrd_excld)
        height_2 = int(height - height_1)
        width_1 = int(width * self.bckgrd_excld)
        width_2 = int(width - width_1)

        return height_1, height_2, width_1, width_2

    def check_glare(self):
        start_time = time()

        # Crop background
        crop_points = self.crop_coord_background(self.img)
        crop_back_img = self.crop_img(self.img, crop_points)

        # Get glare mask (for cropped image)
        crop_bright_mask = self.brightness_mask(crop_back_img)
        _ret, thresh_crop_bright = cv2.threshold(crop_bright_mask, self.min_bright, self.max_bright, cv2.THRESH_BINARY)
        crop_satur_mask = self.saturation_mask(crop_back_img)
        _ret, thresh_crop_satur = cv2.threshold(crop_satur_mask, self.min_saturate,
                                                self.max_saturate, cv2.THRESH_BINARY)
        thresh_crop_satur = np.where(thresh_crop_satur != 0, 255, 0)
        crop_overall_mask = thresh_crop_bright & thresh_crop_satur

        # Get glare mask (for normal-sized image)
        bright_mask = self.brightness_mask(self.img)
        _ret, thresh_bright = cv2.threshold(bright_mask, self.min_bright, self.max_bright, cv2.THRESH_BINARY)
        satur_mask = self.saturation_mask(self.img)
        _ret, thresh_satur = cv2.threshold(satur_mask, self.min_saturate, self.max_saturate, cv2.THRESH_BINARY)
        thresh_satur = np.where(thresh_satur != 0, 255, 0)
        overall_mask = thresh_bright & thresh_satur

        # Calculate glare area
        result_glare_dict = self.calculate_glare(crop_overall_mask)

        # Check all results and get final verdict
        final_verdict = False
        verdict_list = [bool(result_glare_dict[i].get('verdict')) for i in result_glare_dict]
        if True in verdict_list:
            final_verdict = True
        result_glare_dict['final'] = {'verdict': final_verdict}

        # # Prep mask for merge with colored image
        # crop_overall_mask = np.where(crop_overall_mask == 0, 1, crop_overall_mask)
        # crop_overall_mask = np.where(crop_overall_mask == 255, 0, crop_overall_mask)
        # overall_mask = np.where(overall_mask == 0, 1, overall_mask)
        # overall_mask = np.where(overall_mask == 255, 0, overall_mask)
        #
        # # 3-channel mask
        # stacked_crop_mask = np.stack((crop_overall_mask,) * 3, axis=2)
        # stacked_mask = np.stack((overall_mask,) * 3, axis=2)
        #
        # # Blend mask with image
        # final_crop_image = crop_back_img * stacked_crop_mask
        # final_image = self.img * stacked_mask
        #
        # # Draw boxes at critical area
        # height, width, channel = crop_back_img.shape
        # for each_dict in self.crit_area_dict:
        #     if each_dict != 'all_area':
        #         height_list, width_list = self.extract_crit_coord(self.crit_area_dict, each_dict, height, width)
        #         final_crop_image = self.draw_box(final_crop_image, (width_list[1], height_list[1]),
        #                                          (width_list[0], height_list[0]))

        end_time = time() - start_time

        # return final_image, result_glare_dict, final_crop_image, end_time

        return overall_mask, result_glare_dict, overall_mask, end_time


def main():
    image_path = '1.png'

    img_proc_saturation = GlareChannel(cv2.imread(image_path)).check_glare()
    # img_proc_saturation = cv2.applyColorMap(img_proc_saturation, cv2.COLORMAP_TURBO)
    cv2.imwrite('1_overall.png', img_proc_saturation)

    # img_proc_saturation = GlareChannel(cv2.imread(image_path)).run_saturation()
    # img_proc_saturation = cv2.applyColorMap(img_proc_saturation, cv2.COLORMAP_TURBO)
    # cv2.imwrite('1_saturation.png', img_proc_saturation)

    # img_proc_bright = GlareChannel(cv2.imread(image_path)).run_brightness()
    # img_proc_bright = cv2.applyColorMap(img_proc_bright, cv2.COLORMAP_TURBO)
    # cv2.imwrite('1_bright.png', img_proc_bright)


if __name__ == '__main__':
    main()
