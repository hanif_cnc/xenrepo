import cv2
import numpy as np
import configparser
from time import time


class GlareCheck:

    def __init__(self, img,  custom_config=False, config_path='brightness.ini'):
        self.img = img

        self.config = configparser.ConfigParser()
        self.config_path = config_path
        self.config_dict = self.get_config()

        if custom_config:
            self.config_dict['address']['glare_pct'] = custom_config['address']
            self.config_dict['name']['glare_pct'] = custom_config['name']
            self.config_dict['ic']['glare_pct'] = custom_config['ic']
            self.config_dict['gray']['glare_pct'] = custom_config['gray']
            self.config_dict['face']['glare_pct'] = custom_config['face']
            self.config_dict['nation']['glare_pct'] = custom_config['nation']
            self.config_dict['all_area']['glare_pct'] = custom_config['all_area']
            self.config_dict['min_brightness']['denominator'] = custom_config['min_bright_denominator']

    def get_config(self):
        self.config.read(self.config_path)

        point_dict = {
            'address': {
                'height_p1': self.config['CroppingAddress']['HeightP1'],
                'width_p1': self.config['CroppingAddress']['WidthP1'],
                'height_p2': self.config['CroppingAddress']['HeightP2'],
                'width_p2': self.config['CroppingAddress']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['AddressAreaPercent'],
            },

            'name': {
                'height_p1': self.config['CroppingName']['HeightP1'],
                'width_p1': self.config['CroppingName']['WidthP1'],
                'height_p2': self.config['CroppingName']['HeightP2'],
                'width_p2': self.config['CroppingName']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['NameAreaPercent'],
            },

            'ic': {
                'height_p1': self.config['CroppingIc']['HeightP1'],
                'width_p1': self.config['CroppingIc']['WidthP1'],
                'height_p2': self.config['CroppingIc']['HeightP2'],
                'width_p2': self.config['CroppingIc']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['IcAreaPercent'],
            },

            'gray': {
                'height_p1': self.config['CroppingGray']['HeightP1'],
                'width_p1': self.config['CroppingGray']['WidthP1'],
                'height_p2': self.config['CroppingGray']['HeightP2'],
                'width_p2': self.config['CroppingGray']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['GrayAreaPercent'],
            },

            'face': {
                'height_p1': self.config['CroppingFace']['HeightP1'],
                'width_p1': self.config['CroppingFace']['WidthP1'],
                'height_p2': self.config['CroppingFace']['HeightP2'],
                'width_p2': self.config['CroppingFace']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['FaceAreaPercent'],
            },

            'nation': {
                'height_p1': self.config['CroppingNation']['HeightP1'],
                'width_p1': self.config['CroppingNation']['WidthP1'],
                'height_p2': self.config['CroppingNation']['HeightP2'],
                'width_p2': self.config['CroppingNation']['WidthP2'],
                'glare_pct': self.config['AreaThreshold']['NationAreaPercent'],
            },

            'all_area': {
                'background_ratio': self.config['CroppingBackground']['BackgroundRatio'],
                'glare_pct': self.config['AreaThreshold']['OverallAreaPercent']
            },

            'min_brightness': {
                'denominator': self.config['MinBrightness']['DenominatorFactor']
            },

        }

        return point_dict

    @staticmethod
    def find_brightness(img_gray):
        """
        Measure brightness by average grayscale
        """
        brightness = np.average(img_gray)
        return brightness

    @staticmethod
    def crop_img(img, crop_tuple: tuple):
        img = img[crop_tuple[0]: crop_tuple[1], crop_tuple[2]: crop_tuple[3]]
        return img

    def crop_coord_background(self, image):
        height, width, channel = image.shape
        limit = float(self.config_dict['all_area']['background_ratio'])

        height_1 = int(height * limit)
        height_2 = int(height - height_1)
        width_1 = int(width * limit)
        width_2 = int(width - width_1)

        return height_1, height_2, width_1, width_2

    @staticmethod
    def extract_crit_coord(ratio_dict, each_dict, height, width):
        x1 = float(ratio_dict[each_dict].get('height_p1'))
        x2 = float(ratio_dict[each_dict].get('height_p2'))
        y1 = float(ratio_dict[each_dict].get('width_p1'))
        y2 = float(ratio_dict[each_dict].get('width_p2'))

        height_1 = int(height * x1)
        height_2 = int(height * x2)
        width_1 = int(width * y1)
        width_2 = int(width * y2)

        # Sort values for crop points
        height_list = [height_1, height_2]
        width_list = [width_1, width_2]
        height_list.sort()
        width_list.sort()

        return height_list, width_list

    def calculate_glare(self, img, ratio_dict):
        result_dict = {}
        height, width = img.shape

        for each_dict in ratio_dict:
            if each_dict not in ['all_area', 'min_brightness']:
                height_list, width_list = self.extract_crit_coord(ratio_dict, each_dict, height, width)
                crit_img_crop = self.crop_img(img, (height_list[0], height_list[1], width_list[0], width_list[1]))
                unique, counts = np.unique(crit_img_crop, return_counts=True)
                pixel_count = dict(zip(unique, counts))

                total_white = 0
                if 255 in pixel_count:
                    total_white = pixel_count[255]

                try:
                    total_black = pixel_count[0]
                except KeyError:
                    total_black = 0
                percentage_glare = total_white / (total_black + total_white)

                glare_verdict = False
                if percentage_glare > float(ratio_dict[each_dict].get('glare_pct')):
                    glare_verdict = True

                result_dict[f'{each_dict}'] = {'val': 0, 'verdict': 0}
                result_dict[f'{each_dict}']['val'] = percentage_glare
                result_dict[f'{each_dict}']['verdict'] = glare_verdict

            else:
                if each_dict == 'all_area':
                    unique, counts = np.unique(img, return_counts=True)
                    pixel_count = dict(zip(unique, counts))

                    total_white = 0
                    if 255 in pixel_count:
                        total_white = pixel_count[255]

                    total_black = pixel_count[0]
                    percentage_glare = total_white / (total_black + total_white)

                    glare_verdict = False
                    if percentage_glare > float(ratio_dict[each_dict].get('glare_pct')):
                        glare_verdict = True

                    result_dict[f'{each_dict}'] = {'val': 0, 'verdict': 0}
                    result_dict[f'{each_dict}']['val'] = percentage_glare
                    result_dict[f'{each_dict}']['verdict'] = glare_verdict

        return result_dict

    @staticmethod
    def draw_box(img, start_point, end_point):

        color = (255, 0, 0)
        thickness = 2
        img = cv2.rectangle(img, start_point, end_point, color, thickness)

        return img

    def check_glare(self):
        start_time = time()

        # Crop background
        crop_points = self.crop_coord_background(self.img)
        crop_back_img = self.crop_img(self.img, crop_points)

        # Convert to gray
        crop_image_gray = cv2.cvtColor(crop_back_img, cv2.COLOR_BGR2GRAY)
        image_gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)

        # Find average brightness and set brightness threshold
        brightness_val = self.find_brightness(crop_image_gray)
        max_thresh = 255
        denominator_min = float(self.config_dict['min_brightness']['denominator'])
        min_thresh = int(255 - (255 - brightness_val) / denominator_min)

        # Apply Gaussian blur to gray image
        cropped_gray_gauss = cv2.GaussianBlur(crop_image_gray, (3, 3), cv2.BORDER_DEFAULT)
        gray_gauss = cv2.GaussianBlur(image_gray, (3, 3), cv2.BORDER_DEFAULT)

        # Get brightness mask
        _ret, thresh_crop = cv2.threshold(cropped_gray_gauss, min_thresh, max_thresh, cv2.THRESH_BINARY)
        _ret, thresh_img = cv2.threshold(gray_gauss, min_thresh, max_thresh, cv2.THRESH_BINARY)

        # Calculate glare area
        result_glare_dict = self.calculate_glare(thresh_crop, self.config_dict)

        # Check all results and get final verdict
        final_verdict = False
        verdict_list = [bool(result_glare_dict[i].get('verdict')) for i in result_glare_dict]
        if True in verdict_list:
            final_verdict = True

        # Prep mask for merge with colored image
        thresh_crop = np.where(thresh_crop == 0, 1, thresh_crop)
        thresh_crop = np.where(thresh_crop == 255, 0, thresh_crop)
        thresh_img = np.where(thresh_img == 0, 1, thresh_img)
        thresh_img = np.where(thresh_img == 255, 0, thresh_img)

        # 3-channel mask
        stacked_crop_mask = np.stack((thresh_crop,) * 3, axis=2)
        stacked_mask = np.stack((thresh_img,) * 3, axis=2)

        # Blend mask with image
        final_crop_image = crop_back_img * stacked_crop_mask
        final_image = self.img * stacked_mask

        # Draw boxes at critical area
        height, width, channel = crop_back_img.shape
        for each_dict in self.config_dict:
            if each_dict not in ['all_area', 'min_brightness']:
                height_list, width_list = self.extract_crit_coord(self.config_dict, each_dict, height, width)
                final_crop_image = self.draw_box(final_crop_image,
                                                 (width_list[1], height_list[1]),
                                                 (width_list[0], height_list[0]))

        result_glare_dict['final'] = {'verdict': final_verdict}

        end_time = time() - start_time

        return final_image, result_glare_dict, final_crop_image, end_time
