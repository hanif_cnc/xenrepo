import os
import cv2
import numpy as np
import math
import random
from tqdm import tqdm

IMG_DIR = 'F:/Xendity/glare_detection/images/No_glare'
OUTPUT_DIR = 'F:/Xendity/glare_detection/augmented'


def get_img(file_dir):
    list_img = [f'{file_dir}/{i}' for i in os.listdir(file_dir) if i.endswith(('.png', '.jpg', '.jpeg'))]
    return list_img


def circular_mask(img, centre, rad):
    # Color in BGR
    color = (255, 255, 255)
    thickness = -1
    return cv2.circle(img, centre, rad, color, thickness)


def one_triangle(img, pt1, pt2, pt3):
    triangle_cnt = np.array([pt1, pt2, pt3])
    return cv2.drawContours(img, [triangle_cnt], 0, (255, 255, 255), -1)


def triangle_coord(centre, circle_rad, degree):
    length = circle_rad * 3
    spacing = int(circle_rad/2)

    # P1
    x1 = int(math.cos(degree) * spacing) + centre[0]
    y1 = int(math.sin(degree) * spacing) + centre[1]

    # P2
    spacing_2 = spacing * 2
    x2 = x1 - int(math.cos(degree) * spacing_2)
    y2 = y1 - int(math.sin(degree) * spacing_2)

    # P3
    new_ang = 90 - degree
    x3 = centre[0] - int(math.cos(new_ang) * length)
    y3 = centre[1] + int(math.sin(new_ang) * length)

    pt1 = (x1, y1)
    pt2 = (x2, y2)
    pt3 = (x3, y3)

    return pt1, pt2, pt3


def triangle_mask(img, centre, circle_rad):
    # Get point coordinates
    degree_list = [i*20 for i in range(11)]
    img_tri_mask = np.zeros(img.shape, dtype="uint8")
    circle_rad2 = int(circle_rad/2)

    # Shadow
    for each_deg in degree_list:
        pt1, pt2, pt3 = triangle_coord(centre, circle_rad, each_deg)
        img_tri_mask = one_triangle(img_tri_mask, pt1, pt2, pt3)

    img_tri_mask = cv2.addWeighted(img_tri_mask, 0.5, img_tri_mask, 0, 0)

    # Main
    for each_deg in degree_list:
        main1, main2, main3 = triangle_coord(centre, circle_rad2, each_deg)
        img_tri_mask = one_triangle(img_tri_mask, main1, main2, main3)

    return img_tri_mask


def empty_mask(img):
    mask = np.zeros(img.shape, dtype="uint8")
    return mask


def create_glare_star(img, glare_center, circular_rad):
    img_mask = empty_mask(img)
    img_mask = circular_mask(img_mask, glare_center, circular_rad)
    img_mask = triangle_mask(img_mask, glare_center, circular_rad)

    final_image = cv2.addWeighted(img, 1, img_mask, .6, 0)

    return final_image


def create_glare_line(img):
    height, width, channel = img.shape
    img_mask = empty_mask(img)

    line_width = (random.randint(20, 40))

    y1 = random.randint(height - 500, height - 100)
    y2 = random.randint(y1 + 10, height + 30)

    # Randomize line drawn
    if random.randint(0, 1):
        pt1 = (0, y1)
        pt2 = (width, y2)
        pt3 = (0, y1 - line_width)
        pt4 = (width, y2 - line_width)

        pt1_shd = (0, y1 + 10)
        pt2_shd = (width, y2 + 10)
        pt3_shd = (0, y1 - line_width - 10)
        pt4_shd = (width, y2 - line_width - 10)

    else:
        pt1 = (0, y2)
        pt2 = (width, y1)
        pt3 = (0, y2 - line_width)
        pt4 = (width, y1 - line_width)

        pt1_shd = (0, y2 + 10)
        pt2_shd = (width, y1 + 10)
        pt3_shd = (0, y2 - line_width - 10)
        pt4_shd = (width, y1 - line_width - 10)

    rect_cnt_shd = np.array([pt1_shd, pt3_shd, pt4_shd, pt2_shd])
    rect_cnt = np.array([pt1, pt3, pt4, pt2])

    img_mask = cv2.drawContours(img_mask, [rect_cnt_shd], 0, (255, 255, 255), -1)
    img_mask = cv2.addWeighted(img_mask, .5, img_mask, 0, 0)
    img_mask = cv2.drawContours(img_mask, [rect_cnt], 0, (255, 255, 255), -1)

    final_image = cv2.addWeighted(img, 1, img_mask, .6, 0)

    return final_image


def main():
    img_files = get_img(IMG_DIR)
    print("Modifying image with glare")

    for each_idx, each_file in tqdm(enumerate(img_files), total=len(img_files)):
        img = cv2.imread(each_file)
        height, width, channel = img.shape

        if random.randint(0, 1):
            final_image = create_glare_star(img,
                                            (random.randint(100, width - 50), random.randint(100, height - 50)),
                                            random.randint(25, 45))
        else:
            final_image = create_glare_line(img)

        img_basename = each_file.split('/')[-1]
        img_basename = f"mod_{img_basename}"
        output_path = f"{OUTPUT_DIR}/{img_basename}"

        cv2.imwrite(output_path, final_image)


if __name__ == '__main__':
    main()
