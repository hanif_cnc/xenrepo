import cv2
import numpy as np
import configparser
from time import time


def extract_crit_coord(ratio_dict, each_dict, height, width):
    x1 = float(ratio_dict[each_dict].get('height_p1'))
    x2 = float(ratio_dict[each_dict].get('height_p2'))
    y1 = float(ratio_dict[each_dict].get('width_p1'))
    y2 = float(ratio_dict[each_dict].get('width_p2'))

    height_1 = int(height * x1)
    height_2 = int(height * x2)
    width_1 = int(width * y1)
    width_2 = int(width * y2)

    # Sort values for crop points
    height_list = [height_1, height_2]
    width_list = [width_1, width_2]
    height_list.sort()
    width_list.sort()

    return height_list, width_list


def calculate_glare(img, ratio_dict):
    result_dict = {}
    height, width = img.shape

    for each_dict in ratio_dict:
        if each_dict not in ['all_area', 'min_brightness']:
            height_list, width_list = extract_crit_coord(ratio_dict, each_dict, height, width)
            crit_img_crop = crop_img(img, (height_list[0], height_list[1], width_list[0], width_list[1]))
            unique, counts = np.unique(crit_img_crop, return_counts=True)
            pixel_count = dict(zip(unique, counts))

            total_white = 0
            if 255 in pixel_count:
                total_white = pixel_count[255]

            total_black = pixel_count[0]
            percentage_glare = total_white/(total_black + total_white)

            glare_verdict = False
            if percentage_glare > float(ratio_dict[each_dict].get('glare_pct')):
                glare_verdict = True

            result_dict[f'{each_dict}'] = {'val': 0, 'verdict': 0}
            result_dict[f'{each_dict}']['val'] = percentage_glare
            result_dict[f'{each_dict}']['verdict'] = glare_verdict

        else:
            if each_dict == 'all_area':
                unique, counts = np.unique(img, return_counts=True)
                pixel_count = dict(zip(unique, counts))

                total_white = 0
                if 255 in pixel_count:
                    total_white = pixel_count[255]

                total_black = pixel_count[0]
                percentage_glare = total_white / (total_black + total_white)

                glare_verdict = False
                if percentage_glare > float(ratio_dict[each_dict].get('glare_pct')):
                    glare_verdict = True

                result_dict[f'{each_dict}'] = {'val': 0, 'verdict': 0}
                result_dict[f'{each_dict}']['val'] = percentage_glare
                result_dict[f'{each_dict}']['verdict'] = glare_verdict

    return result_dict


def draw_box(img, start_point, end_point):

    color = (255, 0, 0)
    thickness = 2
    img = cv2.rectangle(img, start_point, end_point, color, thickness)

    return img


def crop_coord_background(img, excl_percent):
    height, width, channel = img.shape

    height_1 = int(height * excl_percent)
    height_2 = int(height - height_1)
    width_1 = int(width * excl_percent)
    width_2 = int(width - width_1)

    return height_1, height_2, width_1, width_2


def crop_img(img, crop_tuple: tuple):
    img = img[crop_tuple[0]: crop_tuple[1], crop_tuple[2]: crop_tuple[3]]
    return img


def find_brightness(img_gray):
    """
    Measure brightness by average grayscale
    """
    brightness = np.average(img_gray)
    return brightness


def config_brightness(config_path):
    config = configparser.ConfigParser()
    config.read(config_path)

    point_dict = {
        'address': {
            'height_p1': config['CroppingAddress']['HeightP1'],
            'width_p1': config['CroppingAddress']['WidthP1'],
            'height_p2': config['CroppingAddress']['HeightP2'],
            'width_p2': config['CroppingAddress']['WidthP2'],
            'glare_pct': config['AreaThreshold']['AddressAreaPercent'],
        },

        'name': {
            'height_p1': config['CroppingName']['HeightP1'],
            'width_p1': config['CroppingName']['WidthP1'],
            'height_p2': config['CroppingName']['HeightP2'],
            'width_p2': config['CroppingName']['WidthP2'],
            'glare_pct': config['AreaThreshold']['NameAreaPercent'],
        },

        'ic': {
            'height_p1': config['CroppingIc']['HeightP1'],
            'width_p1': config['CroppingIc']['WidthP1'],
            'height_p2': config['CroppingIc']['HeightP2'],
            'width_p2': config['CroppingIc']['WidthP2'],
            'glare_pct': config['AreaThreshold']['IcAreaPercent'],
        },

        'gray': {
            'height_p1': config['CroppingGray']['HeightP1'],
            'width_p1': config['CroppingGray']['WidthP1'],
            'height_p2': config['CroppingGray']['HeightP2'],
            'width_p2': config['CroppingGray']['WidthP2'],
            'glare_pct': config['AreaThreshold']['GrayAreaPercent'],
        },

        'face': {
            'height_p1': config['CroppingFace']['HeightP1'],
            'width_p1': config['CroppingFace']['WidthP1'],
            'height_p2': config['CroppingFace']['HeightP2'],
            'width_p2': config['CroppingFace']['WidthP2'],
            'glare_pct': config['AreaThreshold']['FaceAreaPercent'],
        },

        'nation': {
            'height_p1': config['CroppingNation']['HeightP1'],
            'width_p1': config['CroppingNation']['WidthP1'],
            'height_p2': config['CroppingNation']['HeightP2'],
            'width_p2': config['CroppingNation']['WidthP2'],
            'glare_pct': config['AreaThreshold']['NationAreaPercent'],
        },

        'all_area': {
            'background_ratio': config['CroppingBackground']['BackgroundRatio'],
            'glare_pct': config['AreaThreshold']['OverallAreaPercent']
        },

        'min_brightness': {
            'denominator': config['MinBrightness']['DenominatorFactor']
        }

    }

    return point_dict


def check_brightness(img, config_path):
    start_time = time()
    img_return = 0
    # Load configurations
    config = config_brightness(config_path)

    # Crop background
    background_ratio = float(config['all_area']['background_ratio'])
    crop_points = crop_coord_background(img, background_ratio)
    crop_back_img = crop_img(img, crop_points)

    # Convert to gray
    crop_image_gray = cv2.cvtColor(crop_back_img, cv2.COLOR_BGR2GRAY)
    image_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Find average brightness and set brightness threshold
    brightness_val = find_brightness(crop_image_gray)
    max_thresh = 255
    denominator_min = float(config['min_brightness']['denominator'])
    min_thresh = int(255 - (255 - brightness_val) / denominator_min)

    # Apply Gaussian blur to gray image
    cropped_gray_gauss = cv2.GaussianBlur(crop_image_gray, (3, 3), cv2.BORDER_DEFAULT)
    gray_gauss = cv2.GaussianBlur(image_gray, (3, 3), cv2.BORDER_DEFAULT)

    # Get brightness mask
    _ret, thresh_crop = cv2.threshold(cropped_gray_gauss, min_thresh, max_thresh, cv2.THRESH_BINARY)
    _ret, thresh_img = cv2.threshold(gray_gauss, min_thresh, max_thresh, cv2.THRESH_BINARY)

    # Calculate glare area
    result_glare_dict = calculate_glare(thresh_crop, config)

    # Check all results and get final verdict
    final_verdict = False
    verdict_list = [bool(result_glare_dict[i].get('verdict')) for i in result_glare_dict]
    if True in verdict_list:
        final_verdict = True

    result_glare_dict['final'] = {'verdict': final_verdict}

    # Prep mask for merge with colored image
    thresh_crop = np.where(thresh_crop == 0, 1, thresh_crop)
    thresh_crop = np.where(thresh_crop == 255, 0, thresh_crop)
    thresh_img = np.where(thresh_img == 0, 1, thresh_img)
    thresh_img = np.where(thresh_img == 255, 0, thresh_img)

    # 3-channel mask
    stacked_crop_mask = np.stack((thresh_crop,) * 3, axis=2)
    stacked_mask = np.stack((thresh_img,) * 3, axis=2)

    # Blend mask with image
    final_crop_image = crop_back_img * stacked_crop_mask
    final_image = img * stacked_mask

    # Draw boxes at critical area
    height, width, channel = crop_back_img.shape
    for each_dict in config:
        if each_dict not in ['all_area', 'min_brightness']:
            height_list, width_list = extract_crit_coord(config, each_dict, height, width)
            final_crop_image = draw_box(final_crop_image, (width_list[1], height_list[1]),
                                        (width_list[0], height_list[0]))

    end_time = time() - start_time

    return final_image, result_glare_dict, final_crop_image, end_time

