import base64
import cv2
import falcon
import io
import re
import json
import os
import psutil
import numpy as np
import settings as s
import pandas as pd
from PIL import Image
from datetime import datetime
from collections import OrderedDict
import time
import tensorflow as tf
import sys

PRED_THRESHOLD = 0.5

class SegmentationHologram:
    def __init__(self, model_path: str, model_ver: str or int, input_resize: int, holo_score: float):
        self.input_resize = input_resize
        self.holo_score = holo_score
        if type(model_ver) is int:
            model_ver = str(model_ver)
        model_ver = model_ver.zfill(4)
        model_path = model_path + f'/{model_ver}'
        model_path = model_path.replace('\\', '/')
        print(model_path)
        self.model = tf.keras.models.load_model(model_path)
        print(self.model.summary())
        
    def on_post_single(self, req, resp):
        on_post_time_start = time.time()
        print('Running single frame hologram segmentation v3')

        try:
            img1 = req.get_param('image1').file.read()
        except AttributeError:
            img1 = None
            print("ERROR: Image parameter is empty. Please upload image.")

        if img1 is None:
            resp.body = json.dumps(
                {
                    "status": falcon.HTTP_400,
                    "error": "Image parameter is empty. Please upload image."
                }
            )
        else:
            # Decode image received
            np_arr = np.fromstring(img1, np.uint8)
            img_read = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

            # Store orignal image shape
            height, width, channels = img_read.shape
            # print('Original Image size:')
            # print(height, width, channels)

            # Convert image to LRGB
            image = cv2.cvtColor(img_read, cv2.COLOR_RGB2BGR)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
            image = cv2.cvtColor(image, cv2.COLOR_LAB2LRGB)

            # Resize image
            image = tf.cast(image, tf.float32) / 255.0
            image = tf.image.resize(image, (self.input_resize, self.input_resize))
            image = tf.expand_dims(image, 0)

            # Perform prediction
            prediction_mask = self.model.predict(image)
            prediction_mask = self.pred_mask_resize(prediction_mask, height, width, PRED_THRESHOLD)
            # print("PREDICTION MASK:")
            # print(type(prediction_mask))
            # print(prediction_mask.shape)
            # print(prediction_mask)
            # print(np.unique(prediction_mask))

            # Count positive mask pixel and get hologram score
            prediction_mask_flat = np.array(prediction_mask, dtype=bool).flatten()
            mask_positive = np.sum(prediction_mask_flat == True)
            # print(f"Total Positive: {mask_positive}")
            holo_overall = 0.36 * (height * width)  # It is assumed that hologram is 36% of myKad
            hologram_score = mask_positive/holo_overall

            # Get hologram verdict
            if hologram_score > self.holo_score:
                hologram_verdict = True
            else:
                hologram_verdict = False

            # Prep combination of mask with original image
            prediction_mask_normalize = np.array(prediction_mask).astype(int)
            stacked_mask = np.stack((prediction_mask_normalize,)*3, axis=2)
            stacked_mask = np.squeeze(stacked_mask, axis=-1)
            img_read_onech = cv2.cvtColor(img_read, cv2.COLOR_BGR2GRAY)
            img_gray = np.zeros_like(img_read)
            img_gray[:, :, 0] = img_read_onech
            img_gray[:, :, 1] = img_read_onech
            img_gray[:, :, 2] = img_read_onech
            mask_image = img_read * stacked_mask + img_gray * (1 - stacked_mask)

            # Prep mask image conversion for json dump
            img = mask_image.astype(np.uint8)
            img = self.np_to_jpeg(img)
            img = self.jpeg_to_base64(img)

            # Unload Model from API
            # tf.keras.backend.clear_session()

            resp.body = json.dumps(
                {
                    "status": 'OK',
                    "hologram_scores": hologram_score,
                    "hologram": hologram_verdict,
                    "mask_image": img.decode('utf-8'),
                    "time_taken": f'{time.time() - on_post_time_start:.2f}'
                }
            )

    def on_post_multi(self, req, resp):
        on_post_time_start = time.time()
        print('Running multiframe hologram segmentation v3')

        img_param_list = ['image1', 'image2', 'image3', 'image4']
        try:
            img_list = [req.get_param(i).file.read() for i in img_param_list]
        except AttributeError:
            print("ERROR: One or more image parameters were empty. Please upload four image.")
            img_list = []

        if not img_list:
            # Skip process if found error
            resp.body = json.dumps(
                {
                    "status": falcon.HTTP_400,
                    "error": "One or more image parameters were empty. Please upload four image."
                }
            )
        else:
            resp.body = json.dumps(
                {
                    "status": "OK"
                }
            )

            lrgb_list = []
            for each_img_string in img_list:
                img_lrgb = self.preprocess_image(each_img_string)
                lrgb_list.append(img_lrgb)

            height, width, channels = lrgb_list[0].shape

            img_batch = tf.stack([self.resize_img(i) for i in lrgb_list])
            print(img_batch)

            prediction_mask = self.model.predict(img_batch)
            print(prediction_mask)
            print(prediction_mask.shape)
            # # Decode image received
            # np_arr = np.fromstring(img1, np.uint8)
            # img_read = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
            #
            # # Store orignal image shape
            # height, width, channels = img_read.shape
            # # print('Original Image size:')
            # # print(height, width, channels)
            #
            # # Convert image to LRGB
            # image = cv2.cvtColor(img_read, cv2.COLOR_RGB2BGR)
            # image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
            # image = cv2.cvtColor(image, cv2.COLOR_LAB2LRGB)
            #
            # # Resize image
            # image = tf.cast(image, tf.float32) / 255.0
            # image = tf.image.resize(image, (self.input_resize, self.input_resize))
            # image = tf.expand_dims(image, 0)
            #
            # # Perform prediction
            # prediction_mask = self.model.predict(image)
            # prediction_mask = self.pred_mask_resize(prediction_mask, height, width, PRED_THRESHOLD)
            # # print("PREDICTION MASK:")
            # # print(type(prediction_mask))
            # # print(prediction_mask.shape)
            # # print(prediction_mask)
            # # print(np.unique(prediction_mask))
            #
            # # Count positive mask pixel and get hologram score
            # prediction_mask_flat = np.array(prediction_mask, dtype=bool).flatten()
            # mask_positive = np.sum(prediction_mask_flat == True)
            # # print(f"Total Positive: {mask_positive}")
            # holo_overall = 0.36 * (height * width)  # It is assumed that hologram is 36% of myKad
            # hologram_score = mask_positive/holo_overall
            #
            # # Get hologram verdict
            # if hologram_score > self.holo_score:
            #     hologram_verdict = True
            # else:
            #     hologram_verdict = False
            #
            # # Prep combination of mask with original image
            # prediction_mask_normalize = np.array(prediction_mask).astype(int)
            # stacked_mask = np.stack((prediction_mask_normalize,)*3, axis=2)
            # stacked_mask = np.squeeze(stacked_mask, axis=-1)
            # img_read_onech = cv2.cvtColor(img_read, cv2.COLOR_BGR2GRAY)
            # img_gray = np.zeros_like(img_read)
            # img_gray[:, :, 0] = img_read_onech
            # img_gray[:, :, 1] = img_read_onech
            # img_gray[:, :, 2] = img_read_onech
            # mask_image = img_read * stacked_mask + img_gray * (1 - stacked_mask)
            #
            # # Prep mask image conversion for json dump
            # img = mask_image.astype(np.uint8)
            # img = self.np_to_jpeg(img)
            # img = self.jpeg_to_base64(img)
            #
            # resp.body = json.dumps(
            #     {
            #         "status": 'OK',
            #         "hologram_scores": hologram_score,
            #         "hologram": hologram_verdict,
            #         "mask_image": img.decode('utf-8'),
            #         "time_taken": f'{time.time() - on_post_time_start:.2f}'
            #     }
            # )

    @tf.function
    def resize_img(self, to_resize_img):
        image = tf.cast(to_resize_img, tf.float32) / 255.0
        image = tf.image.resize(image, (self.input_resize, self.input_resize))
        return image

    @staticmethod
    def preprocess_image(image_str):
        # Decode image received
        np_arr = np.fromstring(image_str, np.uint8)
        img_read = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

        # Convert image to LRGB
        image = cv2.cvtColor(img_read, cv2.COLOR_RGB2BGR)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
        image = cv2.cvtColor(image, cv2.COLOR_LAB2LRGB)

        return image

    @staticmethod
    def pred_mask_resize(img, mask_height, mask_width, pred_cut):
        """
        Takes a prediction mask, resize it and return back as boolean
        """
        img = tf.image.resize(img, (mask_height, mask_width), method=tf.image.ResizeMethod.GAUSSIAN)
        img = tf.math.greater(img, pred_cut)
        img = tf.squeeze(img)
        img = tf.expand_dims(img, -1)
        return img

    def cals_difference_score_a1(self, scores):
        # approach 1 (highest - lowest)
        app1_1 = 0.0
        app1_1 = max(scores) - min(scores)
        # approach 2 (average of all elements in array scores)
        app1_2 = 0.0
        app1_2 = sum(scores) / (len(scores) - 1)
        return app1_1, app1_2

    def cals_difference_score_a2(self, scores):
        diffList2_1 = []
        diffList2_2 = []
        print('Scores by reg : {}'.format(scores))
        columns = ['R1', 'R2', 'R3', 'R4', 'R5']
        # 2.1 is by getting highest and lowest of that region
        # 2.2 is getting average of region / frame - 1
        df = pd.DataFrame(scores, columns=columns)
        print('DF\n{}'.format(df))
        for c in df:
            # print(c)
            if c == 'OB_ID':
                continue
            app21 = max(df[c]) - min(df[c])
            app22 = sum(df[c]) / (len(df) - 1)
            diffList2_1.append(app21)
            diffList2_2.append(app22)
        print('Approach 2.1 : {}'.format(diffList2_1))
        print('Approach 2.2 : {}'.format(diffList2_2))

        return diffList2_1, diffList2_2

    def cals_difference_score_a3(self, viz_arrays):
        viz_overlaps = []
        holo_percs = []
        start_app3 = datetime.now()
        for idx_v in range(len(viz_arrays)-1):
            # height, width,
            print(idx_v)
            black = 0
            white = 0
            viz_ov_byrow = []
            for idx_pixRow in range(len(viz_arrays[idx_v])):
                viz_ov_bycol = []
                for idx_pixCol in range(len(viz_arrays[idx_v][idx_pixRow])):
                    # print(idx_pixCol)
                    if viz_arrays[idx_v][idx_pixRow][idx_pixCol][0] == viz_arrays[idx_v+1][idx_pixRow][idx_pixCol][0]:
                        # print('Same')
                        viz_ov_bycol.append(0)
                        black += 1
                    else:
                        viz_ov_bycol.append(255)
                        white += 1
                    # pass
                viz_ov_byrow.append(viz_ov_bycol)

            ov = np.array(viz_ov_byrow, dtype=np.uint8)
            # n_white_pix = np.sum(ov == 255)
            # all_pix = np.sum(ov == 255) + np.sum(ov == 0)
            # holo_perc = n_white_pix / all_pix
            print('White : {}/ Black + White : {}'.format(white, black + white))
            holo_perc = white / (black + white)
            holo_percs.append(holo_perc)
            new_image = Image.fromarray(ov)
            new_image.save('debug/hologram/overlap_frame{}_{}.png'.format(idx_v, holo_perc))
        end_app3 = datetime.now()
        averages = sum(holo_percs) / (len(holo_percs))
        print('Elapsed for approach 3 : {}'.format(end_app3 - start_app3))

        return averages, holo_percs


    def threshold_config(self, holo_app, dist_app):
        thres = []
        # hologram by whole mykad
        if holo_app == 1:
            # highest - lowest
            if dist_app == 1:
                pass
            # average of all frames
            elif dist_app == 2:
                pass

        # hologram by 5 different regions
        if holo_app == 2:
            # highest - lowest for each regions
            if dist_app == 1:
                thres = [0.05, 0.05, 0.05, 0.05, 0.05]
            # average of all frames for each regions
            elif dist_app == 2:
                thres = [0.05, 0.05, 0.05, 0.05, 0.05]

        # non-overlapping holograms of frame k and k+1 on whole mykad
        if holo_app == 3:
            # get average of all non-overlapping regions
            if dist_app == 1:
                # average of whole region
                thres = [0.15]
            # get scores for each difference of frame: k1 & k2, k2 & k3, k3 & k4
            elif dist_app == 2:
                thres = [0.1, 0.1, 0.1]
        return thres

    def check_passable_hologram(self, holo_app, dist_app, thres, distance_score):
        distance_passable = []
        print('Thres {}'.format(thres))
        print('Distance score : {}'.format(distance_score))
        if holo_app == 1:
            # one index threshold only
            pass

        # if mykad is separated into multiple smaller regions
        elif holo_app == 2:
            for idx, d in enumerate(distance_score):
                if d < thres[idx]:
                    distance_passable.append(0)
                else:
                    distance_passable.append(1)

        # if distance score is overlap
        else:
            # whole region
            if dist_app == 1:
                if distance_score[0] < thres[0]:
                    distance_passable.append(0)
                else:
                    distance_passable.append(1)
            else:
                for idx, d in enumerate(distance_score):
                    if d < thres[idx]:
                        distance_passable.append(0)
                    else:
                        distance_passable.append(1)

        return distance_passable

    @staticmethod
    def np_to_jpeg(image):
        """
        :param image: Image in np array format
        :return: Image as JPEG.
        """
        return Image.fromarray(image[:, :, ::-1])

    @staticmethod
    def jpeg_to_base64(image):
        """
        :param image: Image in jpeg format.
        :return: Image in Base64 format.
        """
        buffer_ = io.BytesIO()
        image.save(buffer_, format="JPEG")
        return base64.b64encode(buffer_.getvalue())

    @staticmethod
    def upload_application_json(body):
        """
        Read request that is in json format

        :param body: json request that contains image in base64
        :return: decoded four base64 images
        """
        body = body.replace('\n', '')
        body_as_json = json.loads(body, object_pairs_hook=OrderedDict)
        images = ["image1", "image2", "image3", "image4"]
        # crop_images = ["crop_img1", "crop_img2", "crop_img3", "crop_img4"]
        imgs = []
        # crop_imgs = []
        for arg in list(body_as_json.keys()):
            image_file_as_base64 = body_as_json[arg]
            # if image_file_as_base64.strip():
            image_file_as_base64 = image_file_as_base64.split('base64,')
            image = base64.b64decode(image_file_as_base64[1])
            if arg in images:
                imgs.append(image)
            # elif arg in crop_images:
            #     crop_imgs.append(image)
        return imgs

    def recordUsage(self):
        logname = 'segment_hologramv3_memorylog_' + str(os.getpid()) + '.txt'
        pr = psutil.Process(os.getpid())
        mem = pr.memory_info().rss / 2 ** 30
        if os.path.exists(logname):
            f = open(logname, 'a')
            f.write(str(float(mem)) + ",\n")
        else:
            f = open(logname, 'w')
            f.write(str(float(mem)) + ",\n")
            f.close()
