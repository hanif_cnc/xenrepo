"""
1/10/2021
Segmentation Hologram v3:
Process:
1. Use trained neural network to segment the hologram in MyKad images
2. Based on the segmentation result, determine if the hologram exist
in Mykad by obtaining holograhic score
"""

import base64
import cv2
import falcon
import io
import re
import json
import os
import psutil
import numpy as np
import settings as s
import pandas as pd
from PIL import Image
from datetime import datetime
from collections import OrderedDict
import time
import tensorflow as tf
import sys

# Threshold for neural network pixel prediction
PRED_THRESHOLD = 0.5

class SegmentationHologram:
    def __init__(self, model_path: str, model_ver: str or int, input_resize: int, holo_score: float):
        """
        model_path: path to neural network (before the model version folder)
        model_ver: the chosen neural network version to run hologram inference
        input_resize: resize input images to fit into neural network
        holo_score: threshold of acceptable holographic score
        """
        self.input_resize = input_resize
        self.holo_score = holo_score
        if type(model_ver) is int:
            model_ver = str(model_ver)
        model_ver = model_ver.zfill(4)
        model_path = model_path + f'/{model_ver}'
        model_path = model_path.replace('\\', '/')
        print(model_path)
        self.model = tf.keras.models.load_model(model_path)
        print(self.model.summary())
        
    def on_post_single(self, req, resp):
        """
        Return in json
        If valid image input:
            status: Status of operation
            hologram_scores: image hologram score in float
            hologram: The verdict of whether hologram is present in image in Boolean
            mask_image: image of segmentation prediction (in color) by base64
            time_taken: total time taken to complete process in seconds
        """
        on_post_time_start = time.time()
        print('Running single frame hologram segmentation v3')

        try:
            img1 = req.get_param('image1').file.read()
        except AttributeError:
            img1 = None
            print("ERROR: Image parameter is empty. Please upload image.")

        if img1 is None:
            resp.body = json.dumps(
                {
                    "status": falcon.HTTP_400,
                    "error": "Image parameter is empty. Please upload image."
                }
            )
        else:
            # Decode image received
            np_arr = np.fromstring(img1, np.uint8)
            img_read = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

            # Store orignal image shape
            height, width, channels = img_read.shape

            # Convert image to LRGB
            image = cv2.cvtColor(img_read, cv2.COLOR_RGB2BGR)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
            image = cv2.cvtColor(image, cv2.COLOR_LAB2LRGB)

            # Resize image
            image = tf.cast(image, tf.float32) / 255.0
            image = tf.image.resize(image, (self.input_resize, self.input_resize))
            image = tf.expand_dims(image, 0)

            # Perform prediction
            prediction_mask = self.model.predict(image)
            prediction_mask = self.pred_mask_resize(prediction_mask, height, width, PRED_THRESHOLD)

            # Count positive mask pixel and get hologram score
            prediction_mask_flat = np.array(prediction_mask, dtype=bool).flatten()
            mask_positive = np.sum(prediction_mask_flat == True)
            holo_overall = 0.36 * (height * width)  # It is assumed that hologram is 36% of myKad
            hologram_score = mask_positive/holo_overall

            # Get hologram verdict
            if hologram_score > self.holo_score:
                hologram_verdict = True
            else:
                hologram_verdict = False

            # Prep combination of mask with original image
            prediction_mask_normalize = np.array(prediction_mask).astype(int)
            stacked_mask = np.stack((prediction_mask_normalize,)*3, axis=2)
            stacked_mask = np.squeeze(stacked_mask, axis=-1)
            img_read_onech = cv2.cvtColor(img_read, cv2.COLOR_BGR2GRAY)
            img_gray = np.zeros_like(img_read)
            img_gray[:, :, 0] = img_read_onech
            img_gray[:, :, 1] = img_read_onech
            img_gray[:, :, 2] = img_read_onech
            mask_image = img_read * stacked_mask + img_gray * (1 - stacked_mask)

            # Prep mask image conversion for json dump
            img = mask_image.astype(np.uint8)
            img = self.np_to_jpeg(img)
            img = self.jpeg_to_base64(img)

            # Unload Model from API
            # tf.keras.backend.clear_session()


            resp.body = json.dumps(
                {
                    "status": 'OK',
                    "hologram_scores": hologram_score,
                    "hologram": hologram_verdict,
                    "mask_image": img.decode('utf-8'),
                    "time_taken": f'{time.time() - on_post_time_start:.2f}'
                }
            )


    @tf.function
    def resize_img(self, to_resize_img):
        image = tf.cast(to_resize_img, tf.float32) / 255.0
        image = tf.image.resize(image, (self.input_resize, self.input_resize))
        return image

    @staticmethod
    def preprocess_image(image_str):
        # Decode image received
        np_arr = np.fromstring(image_str, np.uint8)
        img_read = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

        # Convert image to LRGB
        image = cv2.cvtColor(img_read, cv2.COLOR_RGB2BGR)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
        image = cv2.cvtColor(image, cv2.COLOR_LAB2LRGB)

        return image

    @staticmethod
    def pred_mask_resize(img, mask_height, mask_width, pred_cut):
        """
        Takes a prediction mask, resize it and return back as boolean
        """
        img = tf.image.resize(img, (mask_height, mask_width), method=tf.image.ResizeMethod.GAUSSIAN)
        img = tf.math.greater(img, pred_cut)
        img = tf.squeeze(img)
        img = tf.expand_dims(img, -1)
        return img

    @staticmethod
    def np_to_jpeg(image):
        """
        :param image: Image in np array format
        :return: Image as JPEG.
        """
        return Image.fromarray(image[:, :, ::-1])

    @staticmethod
    def jpeg_to_base64(image):
        """
        :param image: Image in jpeg format.
        :return: Image in Base64 format.
        """
        buffer_ = io.BytesIO()
        image.save(buffer_, format="JPEG")
        return base64.b64encode(buffer_.getvalue())

    def recordUsage(self):
        logname = 'segment_hologramv3_memorylog_' + str(os.getpid()) + '.txt'
        pr = psutil.Process(os.getpid())
        mem = pr.memory_info().rss / 2 ** 30
        if os.path.exists(logname):
            f = open(logname, 'a')
            f.write(str(float(mem)) + ",\n")
        else:
            f = open(logname, 'w')
            f.write(str(float(mem)) + ",\n")
            f.close()
