import numpy as np
import tensorflow as tf

def unet(input_size=(256, 256, 3)):
    inputs = tf.keras.layers.Input(input_size)
    conv1 = tf.keras.layers.Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
    conv1 = tf.keras.layers.Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
    pool1 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = tf.keras.layers.Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
    conv2 = tf.keras.layers.Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
    pool2 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = tf.keras.layers.Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
    conv3 = tf.keras.layers.Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
    pool3 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv3)
    conv4 = tf.keras.layers.Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
    conv4 = tf.keras.layers.Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
    drop4 = tf.keras.layers.Dropout(0.5)(conv4)
    pool4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5 = tf.keras.layers.Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
    conv5 = tf.keras.layers.Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
    drop5 = tf.keras.layers.Dropout(0.5)(conv5)

    up6 = tf.keras.layers.Conv2D(512, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(tf.keras.layers.UpSampling2D(size = (2,2))(drop5))
    merge6 = tf.keras.layers.concatenate([drop4,up6], axis = 3)
    conv6 = tf.keras.layers.Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
    conv6 = tf.keras.layers.Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

    up7 = tf.keras.layers.Conv2D(256, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(tf.keras.layers.UpSampling2D(size = (2,2))(conv6))
    merge7 = tf.keras.layers.concatenate([conv3,up7], axis = 3)
    conv7 = tf.keras.layers.Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
    conv7 = tf.keras.layers.Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

    up8 = tf.keras.layers.Conv2D(128, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(tf.keras.layers.UpSampling2D(size = (2,2))(conv7))
    merge8 = tf.keras.layers.concatenate([conv2,up8], axis = 3)
    conv8 = tf.keras.layers.Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
    conv8 = tf.keras.layers.Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

    up9 = tf.keras.layers.Conv2D(64, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(tf.keras.layers.UpSampling2D(size = (2,2))(conv8))
    merge9 = tf.keras.layers.concatenate([conv1,up9], axis = 3)
    conv9 = tf.keras.layers.Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
    conv9 = tf.keras.layers.Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = tf.keras.layers.Conv2D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv10 = tf.keras.layers.Conv2D(1, 1, activation = 'sigmoid')(conv9)

    model = tf.keras.Model(inputs=inputs, outputs=conv10)

    return model


def unet_v2(input_size=(256, 256, 3)):
    inputs = tf.keras.layers.Input(input_size)
    conv1 = tf.keras.layers.Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
    conv1 = tf.keras.layers.Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
    pool1 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = tf.keras.layers.Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
    conv2 = tf.keras.layers.Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
    pool2 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = tf.keras.layers.Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
    conv3 = tf.keras.layers.Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
    pool3 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv3)
    conv4 = tf.keras.layers.Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
    conv4 = tf.keras.layers.Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
    drop4 = tf.keras.layers.Dropout(0.5)(conv4)
    pool4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5 = tf.keras.layers.Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
    conv5 = tf.keras.layers.Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
    drop5 = tf.keras.layers.Dropout(0.5)(conv5)

    up6 = tf.keras.layers.Conv2D(512, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(tf.keras.layers.UpSampling2D(size = (2,2))(drop5))
    merge6 = tf.keras.layers.concatenate([drop4,up6], axis = 3)
    conv6 = tf.keras.layers.Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
    conv6 = tf.keras.layers.Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

    up7 = tf.keras.layers.Conv2D(256, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(tf.keras.layers.UpSampling2D(size = (2,2))(conv6))
    merge7 = tf.keras.layers.concatenate([conv3,up7], axis = 3)
    conv7 = tf.keras.layers.Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
    conv7 = tf.keras.layers.Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

    up8 = tf.keras.layers.Conv2D(128, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(tf.keras.layers.UpSampling2D(size = (2,2))(conv7))
    merge8 = tf.keras.layers.concatenate([conv2,up8], axis = 3)
    conv8 = tf.keras.layers.Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
    conv8 = tf.keras.layers.Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

    up9 = tf.keras.layers.Conv2D(64, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(tf.keras.layers.UpSampling2D(size = (2,2))(conv8))
    merge9 = tf.keras.layers.concatenate([conv1,up9], axis = 3)
    conv9 = tf.keras.layers.Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
    conv9 = tf.keras.layers.Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = tf.keras.layers.Conv2D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv10 = tf.keras.layers.Conv2D(1, 1, activation = 'sigmoid')(conv9)

    model = tf.keras.Model(inputs=inputs, outputs=conv10)


    return model


def unet_v3(input_size=(256, 256, 3)):
    inputs = tf.keras.layers.Input(input_size)
    s = tf.keras.layers.Lambda(lambda x: x / 255)(inputs)

    c1 = tf.keras.layers.Conv2D(16, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(s)
    c1 = tf.keras.layers.Dropout(0.1)(c1)
    c1 = tf.keras.layers.Conv2D(16, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c1)
    p1 = tf.keras.layers.MaxPooling2D((2, 2))(c1)

    c2 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(p1)
    c2 = tf.keras.layers.Dropout(0.1)(c2)
    c2 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c2)
    p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2)

    extra_c1 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                      padding='same')(p2)
    extra_c1 = tf.keras.layers.Dropout(0.1)(extra_c1)
    extra_c1 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                      padding='same')(extra_c1)
    extra_p1 = tf.keras.layers.MaxPooling2D((2, 2))(extra_c1)

    c3 = tf.keras.layers.Conv2D(64, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(extra_p1)
    c3 = tf.keras.layers.Dropout(0.2)(c3)
    c3 = tf.keras.layers.Conv2D(64, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c3)
    p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3)

    c4 = tf.keras.layers.Conv2D(128, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(p3)
    c4 = tf.keras.layers.Dropout(0.2)(c4)
    c4 = tf.keras.layers.Conv2D(128, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c4)
    p4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(c4)

    c5 = tf.keras.layers.Conv2D(256, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(p4)
    c5 = tf.keras.layers.Dropout(0.3)(c5)
    c5 = tf.keras.layers.Conv2D(256, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c5)

    u6 = tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(c5)
    u6 = tf.keras.layers.concatenate([u6, c4])
    c6 = tf.keras.layers.Conv2D(128, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(u6)
    c6 = tf.keras.layers.Dropout(0.2)(c6)
    c6 = tf.keras.layers.Conv2D(128, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c6)

    u7 = tf.keras.layers.Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(c6)
    u7 = tf.keras.layers.concatenate([u7, c3])
    c7 = tf.keras.layers.Conv2D(64, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(u7)
    c7 = tf.keras.layers.Dropout(0.2)(c7)
    c7 = tf.keras.layers.Conv2D(64, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c7)

    extra_u9 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(c7)
    extra_u9 = tf.keras.layers.concatenate([extra_u9, extra_c1])
    extra_c9 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                      padding='same')(extra_u9)
    extra_c9 = tf.keras.layers.Dropout(0.1)(extra_c9)
    extra_c9 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                      padding='same')(extra_c9)

    u8 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(extra_c9)
    u8 = tf.keras.layers.concatenate([u8, c2])
    c8 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(u8)
    c8 = tf.keras.layers.Dropout(0.1)(c8)
    c8 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c8)

    u9 = tf.keras.layers.Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same')(c8)
    u9 = tf.keras.layers.concatenate([u9, c1], axis=3)
    c9 = tf.keras.layers.Conv2D(16, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(u9)
    c9 = tf.keras.layers.Dropout(0.1)(c9)
    c9 = tf.keras.layers.Conv2D(16, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c9)

    outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(c9)

    model = tf.keras.Model(inputs=[inputs], outputs=[outputs])

    return model


def unet_v4(input_size=(256, 256, 3)):
    inputs = tf.keras.layers.Input(input_size)
    s = tf.keras.layers.Lambda(lambda x: x / 255)(inputs)

    c1 = tf.keras.layers.Conv2D(16, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(s)
    c1 = tf.keras.layers.Dropout(0.1)(c1)
    c1 = tf.keras.layers.Conv2D(16, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c1)
    p1 = tf.keras.layers.MaxPooling2D((2, 2))(c1)

    c2 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(p1)
    c2 = tf.keras.layers.Dropout(0.1)(c2)
    c2 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c2)
    p2 = tf.keras.layers.MaxPooling2D((2, 2))(c2)

    extra_c1 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                      padding='same')(p2)
    extra_c1 = tf.keras.layers.Dropout(0.1)(extra_c1)
    extra_c1 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                      padding='same')(extra_c1)
    extra_p1 = tf.keras.layers.MaxPooling2D((2, 2))(extra_c1)

    c3 = tf.keras.layers.Conv2D(64, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(extra_p1)
    c3 = tf.keras.layers.Dropout(0.2)(c3)
    c3 = tf.keras.layers.Conv2D(64, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c3)
    p3 = tf.keras.layers.MaxPooling2D((2, 2))(c3)

    c4 = tf.keras.layers.Conv2D(128, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(p3)
    c4 = tf.keras.layers.Dropout(0.2)(c4)
    c4 = tf.keras.layers.Conv2D(128, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c4)
    p4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(c4)

    c5 = tf.keras.layers.Conv2D(256, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(p4)
    c5 = tf.keras.layers.Dropout(0.3)(c5)
    c5 = tf.keras.layers.Conv2D(256, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c5)

    extra_c5 = tf.keras.layers.Conv2D(256, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                      padding='same')(c5)
    extra_c5 = tf.keras.layers.Dropout(0.3)(extra_c5)

    u6 = tf.keras.layers.Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(extra_c5)
    u6 = tf.keras.layers.concatenate([u6, c4])
    c6 = tf.keras.layers.Conv2D(128, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(u6)
    c6 = tf.keras.layers.Dropout(0.2)(c6)
    c6 = tf.keras.layers.Conv2D(128, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c6)

    u7 = tf.keras.layers.Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(c6)
    u7 = tf.keras.layers.concatenate([u7, c3])
    c7 = tf.keras.layers.Conv2D(64, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(u7)
    c7 = tf.keras.layers.Dropout(0.2)(c7)
    c7 = tf.keras.layers.Conv2D(64, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c7)

    extra_u9 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(c7)
    extra_u9 = tf.keras.layers.concatenate([extra_u9, extra_c1])
    extra_c9 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                      padding='same')(extra_u9)
    extra_c9 = tf.keras.layers.Dropout(0.1)(extra_c9)
    extra_c9 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                      padding='same')(extra_c9)

    u8 = tf.keras.layers.Conv2DTranspose(32, (2, 2), strides=(2, 2), padding='same')(extra_c9)
    u8 = tf.keras.layers.concatenate([u8, c2])
    c8 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(u8)
    c8 = tf.keras.layers.Dropout(0.1)(c8)
    c8 = tf.keras.layers.Conv2D(32, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c8)

    u9 = tf.keras.layers.Conv2DTranspose(16, (2, 2), strides=(2, 2), padding='same')(c8)
    u9 = tf.keras.layers.concatenate([u9, c1], axis=3)
    c9 = tf.keras.layers.Conv2D(16, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(u9)
    c9 = tf.keras.layers.Dropout(0.1)(c9)
    c9 = tf.keras.layers.Conv2D(16, (3, 3), activation=tf.keras.activations.elu, kernel_initializer='he_normal',
                                padding='same')(c9)

    outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(c9)

    model = tf.keras.Model(inputs=[inputs], outputs=[outputs])

    return model


def xnet(input_size=(64, 64, 3), classes=1, kernel_size=3, filter_depth=(64, 128, 256, 512, 0)):
    img_input = tf.keras.layers.Input(shape=input_size)

    # Encoder
    conv1 = tf.keras.layers.Conv2D(filter_depth[0], (kernel_size, kernel_size), padding="same")(img_input)
    batch1 = tf.keras.layers.BatchNormalization()(conv1)
    act1 = tf.keras.layers.Activation("relu")(batch1)
    pool1 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(act1)
    # 100x100

    conv2 = tf.keras.layers.Conv2D(filter_depth[1], (kernel_size, kernel_size), padding="same")(pool1)
    batch2 = tf.keras.layers.BatchNormalization()(conv2)
    act2 = tf.keras.layers.Activation("relu")(batch2)
    pool2 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(act2)
    # 50x50

    conv3 = tf.keras.layers.Conv2D(filter_depth[2], (kernel_size, kernel_size), padding="same")(pool2)
    batch3 = tf.keras.layers.BatchNormalization()(conv3)
    act3 = tf.keras.layers.Activation("relu")(batch3)
    pool3 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(act3)
    # 25x25

    # Flat
    conv4 = tf.keras.layers.Conv2D(filter_depth[3], (kernel_size, kernel_size), padding="same")(pool3)
    batch4 = tf.keras.layers.BatchNormalization()(conv4)
    act4 = tf.keras.layers.Activation("relu")(batch4)
    # 25x25

    conv5 = tf.keras.layers.Conv2D(filter_depth[3], (kernel_size, kernel_size), padding="same")(act4)
    batch5 = tf.keras.layers.BatchNormalization()(conv5)
    act5 = tf.keras.layers.Activation("relu")(batch5)
    # 25x25

    # Up
    up6 = tf.keras.layers.UpSampling2D(size=(2, 2))(act5)
    conv6 = tf.keras.layers.Conv2D(filter_depth[2], (kernel_size, kernel_size), padding="same")(up6)
    batch6 = tf.keras.layers.BatchNormalization()(conv6)
    act6 = tf.keras.layers.Activation("relu")(batch6)
    concat6 = tf.keras.layers.Concatenate()([act3, act6])
    # 50x50

    up7 = tf.keras.layers.UpSampling2D(size=(2, 2))(concat6)
    conv7 = tf.keras.layers.Conv2D(filter_depth[1], (kernel_size, kernel_size), padding="same")(up7)
    batch7 = tf.keras.layers.BatchNormalization()(conv7)
    act7 = tf.keras.layers.Activation("relu")(batch7)
    concat7 = tf.keras.layers.Concatenate()([act2, act7])
    # 100x100

    # Down
    conv8 = tf.keras.layers.Conv2D(filter_depth[1], (kernel_size, kernel_size), padding="same")(concat7)
    batch8 = tf.keras.layers.BatchNormalization()(conv8)
    act8 = tf.keras.layers.Activation("relu")(batch8)
    pool8 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(act8)
    # 50x50

    conv9 = tf.keras.layers.Conv2D(filter_depth[2], (kernel_size, kernel_size), padding="same")(pool8)
    batch9 = tf.keras.layers.BatchNormalization()(conv9)
    act9 = tf.keras.layers.Activation("relu")(batch9)
    pool9 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(act9)

    # 25x25

    # Flat
    conv10 = tf.keras.layers.Conv2D(filter_depth[3], (kernel_size, kernel_size), padding="same")(pool9)
    batch10 = tf.keras.layers.BatchNormalization()(conv10)
    act10 = tf.keras.layers.Activation("relu")(batch10)
    # 25x25

    conv11 = tf.keras.layers.Conv2D(filter_depth[3], (kernel_size, kernel_size), padding="same")(act10)
    batch11 = tf.keras.layers.BatchNormalization()(conv11)
    act11 = tf.keras.layers.Activation("relu")(batch11)
    # 25x25

    # Encoder
    up12 = tf.keras.layers.UpSampling2D(size=(2, 2))(act11)
    conv12 = tf.keras.layers.Conv2D(filter_depth[2], (kernel_size, kernel_size), padding="same")(up12)
    batch12 = tf.keras.layers.BatchNormalization()(conv12)
    act12 = tf.keras.layers.Activation("relu")(batch12)
    concat12 = tf.keras.layers.Concatenate()([act9, act12])
    # 50x50

    up13 = tf.keras.layers.UpSampling2D(size=(2, 2))(concat12)
    conv13 = tf.keras.layers.Conv2D(filter_depth[1], (kernel_size, kernel_size), padding="same")(up13)
    batch13 = tf.keras.layers.BatchNormalization()(conv13)
    act13 = tf.keras.layers.Activation("relu")(batch13)
    concat13 = tf.keras.layers.Concatenate()([act8, act13])
    # 100x100

    up14 = tf.keras.layers.UpSampling2D(size=(2, 2))(concat13)
    conv14 = tf.keras.layers.Conv2D(filter_depth[0], (kernel_size, kernel_size), padding="same")(up14)
    batch14 = tf.keras.layers.BatchNormalization()(conv14)
    act14 = tf.keras.layers.Activation("relu")(batch14)
    concat14 = tf.keras.layers.Concatenate()([act1, act14])
    # 200x200

    outputs = tf.keras.layers.Conv2D(1, (1, 1), activation='sigmoid')(concat14)

    #     conv15 = Conv2D(classes, (1,1), padding="valid")(concat14)
    #     reshape15 = Reshape((input_shape[0]*input_shape[1],classes))(conv15)
    #     act15 = Activation("softmax")(conv15)

    model = tf.keras.models.Model(img_input, outputs)

    return model


def bcdu_d3_float16(input_size=(256, 256, 1)):
    n_shape = input_size[0]
    inputs = tf.keras.Input(input_size)
    # inputs = input_size
    print(inputs)
    conv1 = tf.keras.layers.Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(inputs)
    conv1 = tf.keras.layers.Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv1)

    pool1 = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(conv1)
    conv2 = tf.keras.layers.Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool1)
    conv2 = tf.keras.layers.Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv2)
    pool2 = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(conv2)
    conv3 = tf.keras.layers.Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool2)
    conv3 = tf.keras.layers.Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv3)
    drop3 = tf.keras.layers.Dropout(0.5)(conv3)
    pool3 = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(conv3)
    # D1
    conv4 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool3)
    conv4_1 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4)
    drop4_1 = tf.keras.layers.Dropout(0.5)(conv4_1)
    # D2
    conv4_2 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(drop4_1)
    conv4_2 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4_2)
    conv4_2 = tf.keras.layers.Dropout(0.5)(conv4_2)
    # D3
    merge_dense = tf.keras.layers.concatenate([conv4_2, drop4_1], axis=3)
    conv4_3 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same',
                                     kernel_initializer='he_normal')(merge_dense)
    conv4_3 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same',
                                     kernel_initializer='he_normal')(conv4_3)
    drop4_3 = tf.keras.layers.Dropout(0.5)(conv4_3)

    up6 = tf.keras.layers.Conv2DTranspose(256, kernel_size=2, strides=2, padding='same',
                                          kernel_initializer='he_normal')(drop4_3)
    up6 = tf.keras.layers.BatchNormalization(axis=3)(up6)
    up6 = tf.keras.layers.Activation('relu')(up6)

    x1 = tf.keras.layers.Reshape(target_shape=(1, np.int32(n_shape / 4), np.int32(n_shape / 4), 256))(drop3)
    x2 = tf.keras.layers.Reshape(target_shape=(1, np.int32(n_shape / 4), np.int32(n_shape / 4), 256))(up6)
    merge6 = tf.keras.layers.concatenate([x1, x2], axis=1)
    merge6 = tf.keras.layers.ConvLSTM2D(filters=128, kernel_size=(3, 3), padding='same', return_sequences=False,
                                        go_backwards=True, kernel_initializer='he_normal')(merge6)

    conv6 = tf.keras.layers.Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge6)
    conv6 = tf.keras.layers.Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv6)

    up7 = tf.keras.layers.Conv2DTranspose(128, kernel_size=2, strides=2, padding='same',
                                          kernel_initializer='he_normal')(conv6)
    up7 = tf.keras.layers.BatchNormalization(axis=3)(up7)
    up7 = tf.keras.layers.Activation('relu')(up7)

    x1 = tf.keras.layers.Reshape(target_shape=(1, np.int32(n_shape / 2), np.int32(n_shape / 2), 128))(conv2)
    x2 = tf.keras.layers.Reshape(target_shape=(1, np.int32(n_shape / 2), np.int32(n_shape / 2), 128))(up7)
    merge7 = tf.keras.layers.concatenate([x1, x2], axis=1)
    merge7 = tf.keras.layers.ConvLSTM2D(filters=64, kernel_size=(3, 3), padding='same', return_sequences=False,
                                        go_backwards=True, kernel_initializer='he_normal')(merge7)

    conv7 = tf.keras.layers.Conv2D(128, 3, activation='relu',
                                   padding='same', kernel_initializer='he_normal')(merge7)
    conv7 = tf.keras.layers.Conv2D(128, 3, activation='relu',
                                   padding='same', kernel_initializer='he_normal')(conv7)

    up8 = tf.keras.layers.Conv2DTranspose(64, kernel_size=2, strides=2,
                                          padding='same', kernel_initializer='he_normal')(conv7)
    up8 = tf.keras.layers.BatchNormalization(axis=3)(up8)
    up8 = tf.keras.layers.Activation('relu')(up8)

    x1 = tf.keras.layers.Reshape(target_shape=(1, n_shape, n_shape, 64))(conv1)
    x2 = tf.keras.layers.Reshape(target_shape=(1, n_shape, n_shape, 64))(up8)
    merge8 = tf.keras.layers.concatenate([x1, x2], axis=1)
    merge8 = tf.keras.layers.ConvLSTM2D(filters=32, kernel_size=(3, 3), padding='same',
                                        return_sequences=False, go_backwards=True,
                                        kernel_initializer='he_normal')(merge8)

    conv8 = tf.keras.layers.Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge8)
    conv8 = tf.keras.layers.Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)
    conv8 = tf.keras.layers.Conv2D(2, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)
    conv9 = tf.keras.layers.Conv2D(1, 1, activation='sigmoid')(conv8)

    # adam = tf.keras.optimizers.Adam
    model = tf.keras.Model(inputs=inputs, outputs=conv9)

    return model


def bcdu_d3(input_size=(256, 256, 1)):
    n_shape = input_size[0]
    inputs = tf.keras.Input(input_size)
    # inputs = input_size
    print(inputs)
    conv1 = tf.keras.layers.Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(inputs)
    conv1 = tf.keras.layers.Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv1)

    pool1 = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(conv1)
    conv2 = tf.keras.layers.Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool1)
    conv2 = tf.keras.layers.Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv2)
    pool2 = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(conv2)
    conv3 = tf.keras.layers.Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool2)
    conv3 = tf.keras.layers.Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv3)
    drop3 = tf.keras.layers.Dropout(0.5)(conv3)
    pool3 = tf.keras.layers.MaxPool2D(pool_size=(2, 2))(conv3)
    # D1
    conv4 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool3)
    conv4_1 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4)
    drop4_1 = tf.keras.layers.Dropout(0.5)(conv4_1)
    # D2
    conv4_2 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(drop4_1)
    conv4_2 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4_2)
    conv4_2 = tf.keras.layers.Dropout(0.5)(conv4_2)
    # D3
    merge_dense = tf.keras.layers.concatenate([conv4_2, drop4_1], axis=3)
    conv4_3 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same',
                                     kernel_initializer='he_normal')(merge_dense)
    conv4_3 = tf.keras.layers.Conv2D(512, 3, activation='relu', padding='same',
                                     kernel_initializer='he_normal')(conv4_3)
    drop4_3 = tf.keras.layers.Dropout(0.5)(conv4_3)

    up6 = tf.keras.layers.Conv2DTranspose(256, kernel_size=2, strides=2, padding='same',
                                          kernel_initializer='he_normal')(drop4_3)
    up6 = tf.keras.layers.BatchNormalization(axis=3)(up6)
    up6 = tf.keras.layers.Activation('relu')(up6)

    x1 = tf.keras.layers.Reshape(target_shape=(1, np.int32(n_shape / 4), np.int32(n_shape / 4), 256))(drop3)
    x2 = tf.keras.layers.Reshape(target_shape=(1, np.int32(n_shape / 4), np.int32(n_shape / 4), 256))(up6)
    merge6 = tf.keras.layers.concatenate([x1, x2], axis=1)
    merge6 = tf.keras.layers.ConvLSTM2D(filters=128, kernel_size=(3, 3), padding='same', return_sequences=False,
                                        go_backwards=True, kernel_initializer='he_normal')(merge6)

    conv6 = tf.keras.layers.Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge6)
    conv6 = tf.keras.layers.Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv6)

    up7 = tf.keras.layers.Conv2DTranspose(128, kernel_size=2, strides=2, padding='same',
                                          kernel_initializer='he_normal')(conv6)
    up7 = tf.keras.layers.BatchNormalization(axis=3)(up7)
    up7 = tf.keras.layers.Activation('relu')(up7)

    x1 = tf.keras.layers.Reshape(target_shape=(1, np.int32(n_shape / 2), np.int32(n_shape / 2), 128))(conv2)
    x2 = tf.keras.layers.Reshape(target_shape=(1, np.int32(n_shape / 2), np.int32(n_shape / 2), 128))(up7)
    merge7 = tf.keras.layers.concatenate([x1, x2], axis=1)
    merge7 = tf.keras.layers.ConvLSTM2D(filters=64, kernel_size=(3, 3), padding='same', return_sequences=False,
                                        go_backwards=True, kernel_initializer='he_normal')(merge7)

    conv7 = tf.keras.layers.Conv2D(128, 3, activation='relu',
                                   padding='same', kernel_initializer='he_normal')(merge7)
    conv7 = tf.keras.layers.Conv2D(128, 3, activation='relu',
                                   padding='same', kernel_initializer='he_normal')(conv7)

    up8 = tf.keras.layers.Conv2DTranspose(64, kernel_size=2, strides=2,
                                          padding='same', kernel_initializer='he_normal')(conv7)
    up8 = tf.keras.layers.BatchNormalization(axis=3)(up8)
    up8 = tf.keras.layers.Activation('relu')(up8)

    x1 = tf.keras.layers.Reshape(target_shape=(1, n_shape, n_shape, 64))(conv1)
    x2 = tf.keras.layers.Reshape(target_shape=(1, n_shape, n_shape, 64))(up8)
    merge8 = tf.keras.layers.concatenate([x1, x2], axis=1)
    merge8 = tf.keras.layers.ConvLSTM2D(filters=32, kernel_size=(3, 3), padding='same',
                                        return_sequences=False, go_backwards=True,
                                        kernel_initializer='he_normal')(merge8)

    conv8 = tf.keras.layers.Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge8)
    conv8 = tf.keras.layers.Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)
    conv8 = tf.keras.layers.Conv2D(2, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)
    conv9 = tf.keras.layers.Conv2D(1, 1, activation='sigmoid')(conv8)

    # adam = tf.keras.optimizers.Adam
    model = tf.keras.Model(inputs=inputs, outputs=conv9)

    return model