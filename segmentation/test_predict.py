import os
import cv2
import statistics
import numpy as np
from tqdm import tqdm
import tensorflow as tf
import matplotlib.pyplot as plt

MODEL_FOLDER = '/data/hanif/hologram_segmentation_model/trained_models/210903'
SELECTED_MODEL_VER = 3
IMG_FOLDER = 'dataset/test_img'
MASK_FOLDER = 'dataset/test_mask'
OUTPUT_FOLD = 'output_img'
# IMG_ORI_HEIGHT = 530
# IMG_ORI_WIDTH = 840
IMG_RESIZE_HEIGHT = 416
IMG_RESIZE_WIDTH = 416


def show_images(img, mk, title: str):
    """
    Show image together with mask.
    """
    img = tf.squeeze(img)
    mk = tf.squeeze(mk)

    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle(title)
    ax1.imshow(img)
    ax2.imshow(mk)
    ax1.axis('off')
    ax2.axis('off')
    plt.show()


@tf.function
def pred_mask_resize(img, mask_height, mask_width, pred_cut):
    """
    Takes a prediction mask, resize it and return back as boolean
    """
    img = tf.image.resize(img, (mask_height, mask_width), method=tf.image.ResizeMethod.GAUSSIAN)
    img = tf.math.greater(img, pred_cut)
    img = tf.squeeze(img)
    img = tf.expand_dims(img, -1)
    return img


@tf.function
def iou_prep(img):
    """
    Expand the dimension as part of preparation for metric calculation
    """
    return tf.expand_dims(img, 0)


def recolor_mask(arr1, red=0, green=255, blue=85):
    """
    To recolor image mask.
    """
    # Set the color value here from 0 to 255
    red_value = red
    green_value = green
    blue_value = blue

    # Create array based on RGB
    red_array = np.copy(arr1)
    green_array = np.copy(arr1)
    blue_array = np.copy(arr1)

    # Change the type from boolean to uint8 for image color
    red_array = red_array.astype(np.uint8)
    green_array = green_array.astype(np.uint8)
    blue_array = blue_array.astype(np.uint8)

    # From color value selection, change the color value
    for index, value in np.ndenumerate(red_array):
        if value == 1:
            red_array[index] = red_value

    for index, value in np.ndenumerate(green_array):
        if value == 1:
            green_array[index] = green_value

    for index, value in np.ndenumerate(blue_array):
        if value == 1:
            blue_array[index] = blue_value

    # Reshape to fit the into a rgb value to a new image
    red_array = np.squeeze(red_array)
    green_array = np.squeeze(green_array)
    blue_array = np.squeeze(blue_array)

    # Create the new image
    result_image = np.stack((red_array, green_array, blue_array))
    result_image = np.moveaxis(result_image, 0, -1)

    return result_image


# Evaluation metric: Dice
def compute_dice(im1, im2, empty_score=1.0):
    """
    Computes the Dice coefficient, a measure of set similarity.
    Parameters
    ----------
    im1 : array-like, bool
        Any array of arbitrary size. If not boolean, will be converted.
    im2 : array-like, bool
        Any other array of identical size. If not boolean, will be converted.
    empty_score: 1.0
    Returns
    -------
    dice : float
        Dice coefficient as a float on range [0,1].
        Maximum similarity = 1
        No similarity = 0
        Both are empty (sum eq to zero) = empty_score
    Notes
    -----
    The order of inputs for `dice` is irrelevant. The result will be
    identical if `im1` and `im2` are switched.
    """
    im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

    im_sum = im1.sum() + im2.sum()
    if im_sum == 0:
        return empty_score

    # Compute Dice coefficient
    intersection = np.logical_and(im1, im2)

    return 2. * intersection.sum() / im_sum


# Evaluation metric: IOU
def compute_iou(img1, img2):
    """
    Computes the IOU from two images.
    Recommend the type to be Boolean for both

    """
    img1 = np.array(img1)
    img2 = np.array(img2)

    if img1.shape[0] != img2.shape[0]:
        raise ValueError("Shape mismatch: the number of images mismatch.")
    IoU = np.zeros((img1.shape[0],), dtype=np.float32)
    for i in range(img1.shape[0]):
        im1 = np.squeeze(img1[i] > 0.5)
        im2 = np.squeeze(img2[i] > 0.5)

        if im1.shape != im2.shape:
            raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

        # Compute Dice coefficient
        intersection = np.logical_and(im1, im2)

        if im1.sum() + im2.sum() == 0:
            IoU[i] = 100
        else:
            IoU[i] = 2. * intersection.sum() * 100.0 / (im1.sum() + im2.sum())
        # database.display_image_mask_pairs(im1, im2)

    return IoU


def combine_masks_and_img(image, mask1, mask2):
    """
    Combine 2 masks and image together.
    Return is ready to plot
    """
    image = np.asarray(image)
    mask1 = np.asarray(mask1)
    mask2 = np.asarray(mask2)

    dst = cv2.addWeighted(image, 1, mask1, 0.9, 0)
    dst2 = cv2.addWeighted(dst, 1, mask2, 0.9, 0)

    return dst2


def run_pred(model_dir, img_dir, mask_dir, output_dir, img_resized_height, img_resized_width):
    # Load model
    model = tf.keras.models.load_model(model_dir)

    # Prep folder output
    model_basename = model_dir.split('/')[-2]
    model_ver = model_dir.split('/')[-1]
    output_folder = f'{output_dir}/{model_basename}/{model_ver}'
    os.makedirs(output_folder, exist_ok=True)

    img_list = [i.split('.')[0] for i in os.listdir(img_dir) if i.endswith(('.png', '.jpg'))]

    iou_score_list = []
    for each_img in tqdm(img_list):
        # Prepare 2 images, one to predict and another to be used together with mask
        img_path = f'{img_dir}/{each_img}.png'

        if not os.path.exists(img_path):
            img_path = f'{img_dir}/{each_img}.jpg'

        # print(img_path)
        image_read = tf.io.read_file(img_path)
        img_for_mask = tf.image.decode_png(image_read, channels=3)
        img_height, img_width = img_for_mask.shape[0], img_for_mask.shape[1]
        image = tf.image.convert_image_dtype(img_for_mask, tf.uint8)
        image = tf.cast(image, tf.float32) / 255.0
        image = tf.image.resize(image, (img_resized_height, img_resized_width))
        image = tf.expand_dims(image, 0)

        # Load original mask labelled by user
        mask_path = f'{mask_dir}/{each_img}.png'
        # print(mask_path)
        mask = tf.io.read_file(mask_path)
        mask = tf.image.decode_png(mask, channels=1)

        # this mask variable holds the original mask size
        mask_baseline = tf.cast(mask, tf.bool)

        # Perform prediction
        prediction_mask = model.predict(image)
        import sys
        # np.set_printoptions(threshold=sys.maxsize)
        # Compute iou
        prediction_mask = pred_mask_resize(prediction_mask, img_height, img_width, pred_cut=0.5)
        mask_baseline = iou_prep(mask_baseline)
        prediction_mask = iou_prep(prediction_mask)
        # print(mask_baseline.shape)
        # print(np.unique(prediction_mask))
        # print(prediction_mask.shape)
        # print(prediction_mask)
        # input()
        iou_score = compute_iou(prediction_mask, mask_baseline)[0]
        iou_score_formatted = f'{iou_score:.2f}'

        pred_mask_rgb = recolor_mask(mask_baseline)
        baseline_mask_rgb = recolor_mask(prediction_mask, red=255, green=0, blue=0)
        image_and_mask = combine_masks_and_img(img_for_mask, pred_mask_rgb, baseline_mask_rgb)

        # # Put IOU score
        # font = cv2.FONT_HERSHEY_SIMPLEX
        # org = (50, 50)
        # font_scale = 1
        # color = (0, 0, 0)
        # thickness = 2
        # image_and_mask = cv2.putText(image_and_mask, iou_score_formatted, org, font,
        #                              font_scale, color, thickness, cv2.LINE_AA)

        image_and_mask = cv2.cvtColor(image_and_mask, cv2.COLOR_BGR2RGB)

        outpath_img = f'{output_folder}/{each_img}_{iou_score_formatted}.png'
        cv2.imwrite(outpath_img, image_and_mask)

        iou_score_list.append(iou_score)

    return iou_score_list


def list_average(lst):
    return sum(lst) / len(lst)


def main():
    model_list = [i for i in os.listdir(MODEL_FOLDER)]
    model_ver = f'{SELECTED_MODEL_VER}'.zfill(4)

    report_file = 'iou_score_overall.txt'
    with open(report_file, 'w') as f:
        f.write(f'----IOU REPORT----\n')

    for each_id, each_model in enumerate(model_list, start=1):
        print('')
        print('================')
        print(f'Model {each_id} out of {len(model_list)}')
        print(f'Processing for {each_model}')
        print('================')
        print('')
        model_full_path = f'{MODEL_FOLDER}/{each_model}/{model_ver}'
        iou_score_list = run_pred(model_dir=model_full_path,
                                  img_dir=IMG_FOLDER,
                                  mask_dir=MASK_FOLDER,
                                  output_dir=OUTPUT_FOLD,
                                  img_resized_height=IMG_RESIZE_HEIGHT,
                                  img_resized_width=IMG_RESIZE_WIDTH)
        arr_score_list = np.array(iou_score_list)
        average_iou = np.mean(arr_score_list)
        std_dev = np.std(iou_score_list)

        with open(report_file, 'a') as f:
            f.write(f'{each_model} IOU score: {average_iou:.2f}, Std dev.: {std_dev:.2f}\n')

        tf.keras.backend.clear_session()


if __name__ == '__main__':
    main()
