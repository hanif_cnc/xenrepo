import os
import cv2
import math
import numpy as np
from tqdm import tqdm
import tensorflow as tf
import matplotlib.pyplot as plt

MODEL_FOLDER = '/data/hanif/hologram_segmentation_model2/trained_models/210914'
IMG_FOLDER = '/data/hanif/hologram_segmentation_model/dataset/test_img'
MASK_FOLDER = '/data/hanif/hologram_segmentation_model/dataset/test_mask'
IOU_REPORT = 'iou_score_test.csv'
# IMG_ORI_HEIGHT = 530
# IMG_ORI_WIDTH = 840
IMG_RESIZE = 416
PSP_RESIZE = 432
# IMG_RESIZE_HEIGHT = 432
# IMG_RESIZE_WIDTH = 432
GPU_DEV_USAGE = 0

gpus = tf.config.list_physical_devices('GPU')
if gpus:
  try:
    tf.config.set_visible_devices(gpus[GPU_DEV_USAGE], 'GPU')
    logical_gpus = tf.config.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)


def show_images(img, mk, title: str):
    """
    Show image together with mask.
    """
    img = tf.squeeze(img)
    mk = tf.squeeze(mk)

    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle(title)
    ax1.imshow(img)
    ax2.imshow(mk)
    ax1.axis('off')
    ax2.axis('off')
    plt.show()


def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])


@tf.function
def pred_mask_resize(img, mask_height, mask_width, pred_cut):
    """
    Takes a prediction mask, resize it and return back as boolean
    """
    img = tf.image.resize(img, (mask_height, mask_width), method=tf.image.ResizeMethod.GAUSSIAN)
    img = tf.math.greater(img, pred_cut)
    img = tf.squeeze(img)
    img = tf.expand_dims(img, -1)
    return img


@tf.function
def iou_prep(img):
    """
    Expand the dimension as part of preparation for metric calculation
    """
    return tf.expand_dims(img, 0)


def recolor_mask(arr1, red=0, green=255, blue=85):
    """
    To recolor image mask.
    """
    # Set the color value here from 0 to 255
    red_value = red
    green_value = green
    blue_value = blue

    # Create array based on RGB
    red_array = np.copy(arr1)
    green_array = np.copy(arr1)
    blue_array = np.copy(arr1)

    # Change the type from boolean to uint8 for image color
    red_array = red_array.astype(np.uint8)
    green_array = green_array.astype(np.uint8)
    blue_array = blue_array.astype(np.uint8)

    # From color value selection, change the color value
    for index, value in np.ndenumerate(red_array):
        if value == 1:
            red_array[index] = red_value

    for index, value in np.ndenumerate(green_array):
        if value == 1:
            green_array[index] = green_value

    for index, value in np.ndenumerate(blue_array):
        if value == 1:
            blue_array[index] = blue_value

    # Reshape to fit the into a rgb value to a new image
    red_array = np.squeeze(red_array)
    green_array = np.squeeze(green_array)
    blue_array = np.squeeze(blue_array)

    # Create the new image
    result_image = np.stack((red_array, green_array, blue_array))
    result_image = np.moveaxis(result_image, 0, -1)

    return result_image


# Evaluation metric: Dice
def compute_dice(im1, im2, empty_score=1.0):
    """
    Computes the Dice coefficient, a measure of set similarity.
    Parameters
    ----------
    im1 : array-like, bool
        Any array of arbitrary size. If not boolean, will be converted.
    im2 : array-like, bool
        Any other array of identical size. If not boolean, will be converted.
    empty_score: 1.0
    Returns
    -------
    dice : float
        Dice coefficient as a float on range [0,1].
        Maximum similarity = 1
        No similarity = 0
        Both are empty (sum eq to zero) = empty_score
    Notes
    -----
    The order of inputs for `dice` is irrelevant. The result will be
    identical if `im1` and `im2` are switched.
    """
    im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

    im_sum = im1.sum() + im2.sum()
    if im_sum == 0:
        return empty_score

    # Compute Dice coefficient
    intersection = np.logical_and(im1, im2)

    return 2. * intersection.sum() / im_sum


# Evaluation metric: IOU
def compute_iou(img1, img2):
    """
    Computes the IOU from two images.
    Recommend the type to be Boolean for both

    """
    img1 = np.array(img1)
    img2 = np.array(img2)

    if img1.shape[0] != img2.shape[0]:
        raise ValueError("Shape mismatch: the number of images mismatch.")
    IoU = np.zeros((img1.shape[0],), dtype=np.float32)
    for i in range(img1.shape[0]):
        im1 = np.squeeze(img1[i] > 0.5)
        im2 = np.squeeze(img2[i] > 0.5)

        if im1.shape != im2.shape:
            raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

        # Compute Dice coefficient
        intersection = np.logical_and(im1, im2)

        if im1.sum() + im2.sum() == 0:
            IoU[i] = 100
        else:
            IoU[i] = 2. * intersection.sum() * 100.0 / (im1.sum() + im2.sum())
        # database.display_image_mask_pairs(im1, im2)

    return IoU


def combine_masks_and_img(image, mask1, mask2):
    """
    Combine 2 masks and image together.
    Return is ready to plot
    """
    image = np.asarray(image)
    mask1 = np.asarray(mask1)
    mask2 = np.asarray(mask2)

    dst = cv2.addWeighted(image, 1, mask1, 0.9, 0)
    dst2 = cv2.addWeighted(dst, 1, mask2, 0.9, 0)

    return dst2


def run_pred(model_dir, img_dir, mask_dir, img_resized_height, img_resized_width):
    # Load model
    model = tf.keras.models.load_model(model_dir)

    img_list = [i.split('.')[0] for i in os.listdir(img_dir) if i.endswith(('.png', '.jpg'))]

    iou_score_list = []
    for each_img in tqdm(img_list):
        # Prepare 2 images, one to predict and another to be used together with mask
        img_path = f'{img_dir}/{each_img}.png'

        if not os.path.exists(img_path):
            img_path = f'{img_dir}/{each_img}.jpg'

        image_read = tf.io.read_file(img_path)
        img_for_mask = tf.image.decode_png(image_read, channels=3)
        img_height, img_width = img_for_mask.shape[0], img_for_mask.shape[1]
        image = tf.image.convert_image_dtype(img_for_mask, tf.uint8)
        image = tf.cast(image, tf.float32) / 255.0
        image = tf.image.resize(image, (img_resized_height, img_resized_width))
        image = tf.expand_dims(image, 0)

        # Load original mask labelled by user
        mask_path = f'{mask_dir}/{each_img}.png'
        mask = tf.io.read_file(mask_path)
        mask = tf.image.decode_png(mask, channels=1)

        # this mask variable holds the original mask size
        mask_baseline = tf.cast(mask, tf.bool)

        # Perform prediction
        prediction_mask = model.predict(image)

        prediction_mask = pred_mask_resize(prediction_mask, img_height, img_width, pred_cut=0.5)
        mask_baseline = iou_prep(mask_baseline)
        prediction_mask = iou_prep(prediction_mask)

        iou_score = compute_iou(prediction_mask, mask_baseline)[0]

        iou_score_list.append(iou_score)

    return iou_score_list


def list_average(lst):
    return sum(lst) / len(lst)


def main():
    with open(IOU_REPORT, 'w') as f:
        f.write(f'model_name, model_architecture, model_backbone, model_ver, iou_score, iou_std, iou_diff, estd._size, '
                f'size_comparison\n')

    proc_list = []
    for each_model in os.listdir(MODEL_FOLDER):
        model_path = f'{MODEL_FOLDER}/{each_model}'
        model_path = model_path.replace('\\', '/')

        split_name = each_model.split('_')

        if len(split_name) == 3:
            model_arch = split_name[1]
            model_backb = split_name[2]
        elif len(split_name) == 2:
            model_arch = split_name[1]
            model_backb = ''
        else:
            print(f'WARNING: the model {each_model} name is invalid. Skipping')
            continue

        for each_ver in os.listdir(model_path):
            model_version = int(each_ver)

            model_fullpath = f'{model_path}/{each_ver}'
            model_fullpath = model_fullpath.replace('\\', '/')

            # Get model items path
            list_model_filepath = []
            for each_item in os.listdir(model_fullpath):
                if '.pb' in each_item:
                    pb_path = f'{model_fullpath}/{each_item}'
                    list_model_filepath.append(pb_path)
                elif each_item == 'variables':
                    var_path = f'{model_fullpath}/{each_item}'
                    for each_var in os.listdir(var_path):
                        model_item_path = f'{var_path}/{each_var}'
                        list_model_filepath.append(model_item_path)

            # Determine filesizes for each model item
            list_model_size = [os.stat(i).st_size for i in list_model_filepath]
            model_size_byte = sum(list_model_size)

            proc_list.append([model_fullpath, each_model, model_arch, model_backb, model_version, model_size_byte])

    # Get the order of model size
    list_size_only = [i[-1] for i in proc_list]
    list_size_only.sort()
    total_model = len(proc_list)

    # Embed info of model size order
    proc_list2 = []
    for each_proc in proc_list:
        model_size = each_proc[-1]
        model_size_order = list_size_only.index(model_size)
        formatted_size = convert_size(model_size)
        each_proc.extend([formatted_size, f'{model_size_order + 1} out of {total_model}'])
        proc_list2.append(each_proc)

    # Get iou score
    proc_list3 = []
    for each_id, each_proc in enumerate(proc_list2, start=1):
        print('')
        print('================')
        print(f'Model {each_id} out of {len(proc_list2)}')
        print(f'Processing for {each_proc[1]}')
        print('================')
        print('')
        model_full_path = each_proc[0]

        # Change resize if pspnet
        if each_proc[2] == 'pspnet':
            resize_img = PSP_RESIZE
        else:
            resize_img = IMG_RESIZE

        iou_score_list = run_pred(model_dir=model_full_path,
                                  img_dir=IMG_FOLDER,
                                  mask_dir=MASK_FOLDER,
                                  img_resized_height=resize_img,
                                  img_resized_width=resize_img)
        arr_score_list = np.array(iou_score_list)
        average_iou = np.mean(arr_score_list)
        std_dev = np.std(iou_score_list)
        print("")
        print(f"IOU score: {average_iou:.2f}, Std dev.: {std_dev:.2f}, Estimate size: {each_proc[6]}")
        each_proc.extend([average_iou, std_dev])
        proc_list3.append(each_proc)

        tf.keras.backend.clear_session()

    proc_list3.sort(key=lambda x: x[-2], reverse=True)

    # Calculate score difference between iou score
    highest_iou = proc_list3[0][-2]
    proc_list4 = []
    for each_proc in proc_list3:
        iou_diff = each_proc[-2] - highest_iou
        each_proc.append(iou_diff)
        proc_list4.append(each_proc)

    with open(IOU_REPORT, 'a') as f:
        for each_proc in proc_list4:

            f.write(f'{each_proc[1]}, {each_proc[2]}, {each_proc[3]}, '
                    f'{each_proc[4]}, {each_proc[-3]:.2f}, {each_proc[-2]:.2f}, '
                    f'{each_proc[-1]:.2f}, {each_proc[6]}, {each_proc[7]}\n')


if __name__ == '__main__':
    main()
