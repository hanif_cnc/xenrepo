import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import cv2

MODEL_PATH = '/data/hanif/hologram_segmentation_model/trained_models/210830/m_xnet/0001'
IMG_PATH = 'dataset/test_img/0000051.png'
MASK_PATH = 'dataset/test_mask/0000051.png'
OUTPUT_DIR = 'output_img/0000051.png'
IMG_ORI_HEIGHT = 530
IMG_ORI_WIDTH = 840
IMG_RESIZE = 320


def show_images(img, mk, title: str):
    """
    Show image together with mask.
    """
    img = tf.squeeze(img)
    mk = tf.squeeze(mk)

    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle(title)
    ax1.imshow(img)
    ax2.imshow(mk)
    ax1.axis('off')
    ax2.axis('off')
    plt.show()


@tf.function
def pred_mask_resize(img, mask_height, mask_width, pred_cut):
    """
    Takes a prediction mask, resize it and return back as boolean
    """
    img = tf.image.resize(img, (mask_height, mask_width), method=tf.image.ResizeMethod.GAUSSIAN)
    img = tf.math.greater(img, pred_cut)
    img = tf.squeeze(img)
    img = tf.expand_dims(img, -1)
    return img


@tf.function
def iou_prep(img):
    """
    Expand the dimension as part of preparation for metric calculation
    """
    return tf.expand_dims(img, 0)


def recolor_mask(arr1, red=0, green=255, blue=85):
    """
    To recolor image mask.
    """
    # Set the color value here from 0 to 255
    red_value = red
    green_value = green
    blue_value = blue

    # Create array based on RGB
    red_array = np.copy(arr1)
    green_array = np.copy(arr1)
    blue_array = np.copy(arr1)

    # Change the type from boolean to uint8 for image color
    red_array = red_array.astype(np.uint8)
    green_array = green_array.astype(np.uint8)
    blue_array = blue_array.astype(np.uint8)

    # From color value selection, change the color value
    for index, value in np.ndenumerate(red_array):
        if value == 1:
            red_array[index] = red_value

    for index, value in np.ndenumerate(green_array):
        if value == 1:
            green_array[index] = green_value

    for index, value in np.ndenumerate(blue_array):
        if value == 1:
            blue_array[index] = blue_value

    # Reshape to fit the into a rgb value to a new image
    red_array = np.squeeze(red_array)
    green_array = np.squeeze(green_array)
    blue_array = np.squeeze(blue_array)

    # Create the new image
    result_image = np.stack((red_array, green_array, blue_array))
    result_image = np.moveaxis(result_image, 0, -1)

    return result_image


# Evaluation metric: Dice
def compute_dice(im1, im2, empty_score=1.0):
    """
    Computes the Dice coefficient, a measure of set similarity.
    Parameters
    ----------
    im1 : array-like, bool
        Any array of arbitrary size. If not boolean, will be converted.
    im2 : array-like, bool
        Any other array of identical size. If not boolean, will be converted.
    Returns
    -------
    dice : float
        Dice coefficient as a float on range [0,1].
        Maximum similarity = 1
        No similarity = 0
        Both are empty (sum eq to zero) = empty_score
    Notes
    -----
    The order of inputs for `dice` is irrelevant. The result will be
    identical if `im1` and `im2` are switched.
    """
    im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

    im_sum = im1.sum() + im2.sum()
    if im_sum == 0:
        return empty_score

    # Compute Dice coefficient
    intersection = np.logical_and(im1, im2)

    return 2. * intersection.sum() / im_sum


# Evaluation metric: IOU
def compute_iou(img1, img2):
    """
    Computes the IOU from two images.
    Recommend the type to be Boolean for both

    """
    img1 = np.array(img1)
    img2 = np.array(img2)

    if img1.shape[0] != img2.shape[0]:
        raise ValueError("Shape mismatch: the number of images mismatch.")
    IoU = np.zeros((img1.shape[0],), dtype=np.float32)
    for i in range(img1.shape[0]):
        im1 = np.squeeze(img1[i] > 0.5)
        im2 = np.squeeze(img2[i] > 0.5)

        if im1.shape != im2.shape:
            raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

        # Compute Dice coefficient
        intersection = np.logical_and(im1, im2)

        if im1.sum() + im2.sum() == 0:
            IoU[i] = 100
        else:
            IoU[i] = 2. * intersection.sum() * 100.0 / (im1.sum() + im2.sum())
        # database.display_image_mask_pairs(im1, im2)

    return IoU


def combine_masks_and_img(image, mask1, mask2):
    """
    Combine 2 masks and image together.
    Return is ready to plot
    """
    image = np.asarray(image)
    mask1 = np.asarray(mask1)
    mask2 = np.asarray(mask2)

    dst = cv2.addWeighted(image, 1, mask1, 0.9, 0)
    dst2 = cv2.addWeighted(dst, 1, mask2, 0.9, 0)

    return dst2


def main():
    # Load model
    model = tf.keras.models.load_model(MODEL_PATH)
    model.summary()

    # Prepare 2 images, one to predict and another to be used together with mask
    image_read = tf.io.read_file(IMG_PATH)
    img_for_mask = tf.image.decode_png(image_read, channels=3)

    image = tf.image.convert_image_dtype(img_for_mask, tf.uint8)
    image = tf.cast(image, tf.float32) / 255.0
    image = tf.image.resize(image, (IMG_RESIZE, IMG_RESIZE))
    image = tf.expand_dims(image, 0)

    # Load original mask labelled by user
    mask = tf.io.read_file(MASK_PATH)
    mask = tf.image.decode_png(mask, channels=1)

    # this mask variable holds the original mask size
    mask_baseline = tf.cast(mask, tf.bool)

    # Perform prediction
    prediction_mask = model.predict(image)

    # Compute iou
    prediction_mask = pred_mask_resize(prediction_mask, IMG_ORI_HEIGHT, IMG_ORI_WIDTH, pred_cut=0.5)
    mask_baseline = iou_prep(mask_baseline)
    prediction_mask = iou_prep(prediction_mask)
    iou_score = compute_iou(prediction_mask, mask_baseline)[0]
    print(f'IOU SCORE: {iou_score}')

    pred_mask_rgb = recolor_mask(mask_baseline)
    # pred_mask_rgb = np.resize(prediction_mask, img_for_mask.shape)
    baseline_mask_rgb = recolor_mask(prediction_mask, red=255, green=0, blue=0)
    # baseline_mask_rgb = np.resize(mask_baseline, img_for_mask.shape)
    print(pred_mask_rgb.shape)
    print(baseline_mask_rgb.shape)
    image_and_mask = combine_masks_and_img(img_for_mask, pred_mask_rgb, baseline_mask_rgb)
    image_and_mask = cv2.cvtColor(image_and_mask, cv2.COLOR_BGR2RGB)
    cv2.imwrite(OUTPUT_DIR, image_and_mask)


if __name__ == '__main__':
    main()
