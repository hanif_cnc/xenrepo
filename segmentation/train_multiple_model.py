import os
from tensorflow import keras
import tensorflow as tf
import numpy as np
import segmentation_models as sm
from model import model
from model import DCUNet, MultiResUnet, ESPNet, DeepLab

AUTOTUNE = tf.data.experimental.AUTOTUNE
TRAIN_IMG_DIR = '/data/hanif/hologram_dataset/210924_train_img'
TEST_IMG_DIR = '/data/hanif/hologram_dataset/210924_test_img'
# TRAIN_IMG_DIR = 'F:/Xendity/LabelMe/dataset/train_img'
# TEST_IMG_DIR = 'F:/Xendity/LabelMe/dataset/test_img'

# Models to train
MODEL_LIST = ['unet', 'fpn', 'linknet']  # str: 'unet', 'fpn', 'linknet'
# Refer https://github.com/qubvel/segmentation_models
BACKBONE_LIST = ['vgg16', 'vgg19', 'resnet18', 'resnet34', 'resnet50', 'resnet101',
                 'seresnet18', 'seresnet50', 'seresnet101',
                 'resnext50', 'seresnext50', 'seresnext101', 'densenet121',
                 'inceptionresnetv2', 'mobilenetv2', 'efficientnetb0',
                 'efficientnetb1', 'efficientnetb2', 'efficientnetb3',
                 'efficientnetb4']
MODEL_DATE = '210924'
MODEL_VERSION = '0007'

# Training parameters
EPOCHS = 1_000
STEPS_PER_EPOCH = 2_000
BATCH_SIZE = 1
IMG_HEIGH = 530
IMG_WIDTH = 840
RESIZE_HEIGHT = 416
RESIZE_WIDTH = 416
PSP_RESIZE = 432
EARLY_STOP_PATIENCE = 15
LEARNING_PATIENCE = 4
GPU_DEV_USAGE = 0
OPT = 'adam'
MODEL_ACTIVATION = 'sigmoid'
TRAIN_LOSS = 'binary_crossentropy'
METRIC_LIST = ['accuracy']


gpus = tf.config.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.set_visible_devices(gpus[GPU_DEV_USAGE], 'GPU')
    logical_gpus = tf.config.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
    # print('GPU SELECTED:')
    # print(gpus[0])
    # tf.config.experimental.set_virtual_device_configuration(gpus[0], [
    #     tf.config.experimental.VirtualDeviceConfiguration(memory_limit=9000)])
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)

#################################################################################
# Function to obtain the mask images
@tf.function
def parse_image(img_path: str) -> dict:
    image = tf.io.read_file(img_path)
    image = tf.image.decode_png(image, channels=3)
    image = tf.image.convert_image_dtype(image, tf.uint8)

    mask_path = tf.strings.regex_replace(img_path, 'img', 'mask')
    mask = tf.io.read_file(mask_path)

    mask = tf.image.decode_png(mask, channels=1)
    # mask = tf.where(mask == 255, np.dtype('uint8').type(0), mask)

    return {'image': image, 'segmentation_mask': mask}


# Function to normalize images
@tf.function
def normalize(input_image, input_mask):
    input_image = tf.cast(input_image, tf.float32) / 255.0
    input_mask = tf.cast(input_mask, tf.bool)
    # input_mask = tf.cast(input_mask, tf.uint8)
    # input_mask = tf.cast(input_mask, tf.float32) / 255.0
    return input_image, input_mask


# Function to prep data for training
@tf.function
def load_image(datapoint):
    input_image = tf.image.resize(datapoint['image'], (RESIZE_HEIGHT, RESIZE_WIDTH))
    input_mask = tf.image.resize(datapoint['segmentation_mask'], (RESIZE_HEIGHT, RESIZE_WIDTH))

    input_image, input_mask = normalize(input_image, input_mask)

    return input_image, input_mask


# Function to prep data for training
@tf.function
def load_image_psp(datapoint):
    input_image = tf.image.resize(datapoint['image'], (PSP_RESIZE, PSP_RESIZE))
    input_mask = tf.image.resize(datapoint['segmentation_mask'], (PSP_RESIZE, PSP_RESIZE))

    input_image, input_mask = normalize(input_image, input_mask)

    return input_image, input_mask

#################################################################################
# Dataset pipeline
TRAIN_IMG_DIR = TRAIN_IMG_DIR.replace('\\', '/')
TEST_IMG_DIR = TEST_IMG_DIR.replace('\\', '/')
train_img_directory = f'{TRAIN_IMG_DIR}/*.png'
test_img_directory = f'{TEST_IMG_DIR}/*.png'

train_data = tf.data.Dataset.list_files(train_img_directory)
test_data = tf.data.Dataset.list_files(test_img_directory)
train_data_psp = tf.data.Dataset.list_files(train_img_directory)
test_data_psp = tf.data.Dataset.list_files(test_img_directory)

# Get the mask images that correlates to the image names
train_parse = train_data.map(parse_image, num_parallel_calls=AUTOTUNE)
test_parse = test_data.map(parse_image, num_parallel_calls=AUTOTUNE)
train_parse_psp = train_data_psp.map(parse_image, num_parallel_calls=AUTOTUNE)
test_parse_psp = test_data_psp.map(parse_image, num_parallel_calls=AUTOTUNE)

# Prep image and mask for training
train = train_parse.map(load_image, num_parallel_calls=AUTOTUNE)
test = test_parse.map(load_image, num_parallel_calls=AUTOTUNE)
train_psp = train_parse_psp.map(load_image_psp, num_parallel_calls=AUTOTUNE)
test_psp = test_parse_psp.map(load_image_psp, num_parallel_calls=AUTOTUNE)
train_batch = train.batch(BATCH_SIZE).repeat()
test_batch = test.batch(BATCH_SIZE).repeat()
train_batch_psp = train_psp.batch(BATCH_SIZE).repeat()
test_batch_psp = test_psp.batch(BATCH_SIZE).repeat()

# Get test image amount
test_size = len([i for i in os.listdir(TEST_IMG_DIR) if i.endswith('.png')])
##########################################################################################
# Prep model training list
model_proc_list = []
for each_model in MODEL_LIST:
    for each_backbone in BACKBONE_LIST:
        model_proc_list.append([each_model, each_backbone])

psp_proc_list = [['pspnet', i] for i in BACKBONE_LIST]

additional_model = 9
total_proc = len(model_proc_list) + additional_model + len(psp_proc_list)

#################################################################################
# Train UNET
model_save_path = f'trained_models/{MODEL_DATE}/m_unet/{MODEL_VERSION}'
tensorboard_log_path = model_save_path + '/logs'

print("")
print("========================================")
print("TRAINING FOR:")
print(f'unet_input{RESIZE_HEIGHT}')
print(f'Training process 1 out of {total_proc}')
print(f'Model save path: {model_save_path}')
print("========================================")
print("")

# model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)

model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path,
                                              save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                                      patience=LEARNING_PATIENCE, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()

#################################################################################
# Train DeepLab Xception
model_save_path = f'trained_models/{MODEL_DATE}/m_deeplab_xception/{MODEL_VERSION}'
tensorboard_log_path = model_save_path + '/logs'

print("")
print("========================================")
print("TRAINING FOR:")
print(f'deeplab_xception_input{RESIZE_HEIGHT}')
print(f'Training process 2 out of {total_proc}')
print(f'Model save path: {model_save_path}')
print("========================================")
print("")

# model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train = DeepLab.deeplabv3_plus_xception(input_shape=(RESIZE_HEIGHT, RESIZE_WIDTH, 3), num_classes=1)
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)

model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path,
                                              save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                                      patience=LEARNING_PATIENCE, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()

#################################################################################
# Train DeepLab Mobilenet
model_save_path = f'trained_models/{MODEL_DATE}/m_deeplab_mobilenet/{MODEL_VERSION}'
tensorboard_log_path = model_save_path + '/logs'

print("")
print("========================================")
print("TRAINING FOR:")
print(f'deeplab_mobilenet_input{RESIZE_HEIGHT}')
print(f'Training process 3 out of {total_proc}')
print(f'Model save path: {model_save_path}')
print("========================================")
print("")

model_train = DeepLab.deeplabv3_plus_mobile(input_shape=(RESIZE_HEIGHT, RESIZE_WIDTH, 3), num_classes=1)
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)

model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path,
                                              save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                                      patience=LEARNING_PATIENCE, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()

#################################################################################
# Train XNET model
model_save_path = f'trained_models/{MODEL_DATE}/m_xnet/{MODEL_VERSION}'
tensorboard_log_path = model_save_path + '/logs'

print("")
print("========================================")
print("TRAINING FOR:")
print(f'xnet_input{RESIZE_HEIGHT}')
print(f'Training process 4 out of {total_proc}')
print(f'Model save path: {model_save_path}')
print("========================================")
print("")

model_train = model.xnet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path, save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                                      patience=LEARNING_PATIENCE, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()

#################################################################################
# Train BDCUNet Float 16 model
model_save_path = f'trained_models/{MODEL_DATE}/m_bdcunetf16/{MODEL_VERSION}'
tensorboard_log_path = model_save_path + '/logs'

print("")
print("========================================")
print("TRAINING FOR:")
print(f'bdcunetf16_input{RESIZE_HEIGHT}')
print(f'Training process 5 out of {total_proc}')
print(f'Model save path: {model_save_path}')
print("========================================")
print("")

model_train = model.bcdu_d3_float16(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path, save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=LEARNING_PATIENCE, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()

#################################################################################
# Train BDCUNet model
model_save_path = f'trained_models/{MODEL_DATE}/m_bdcunet/{MODEL_VERSION}'
tensorboard_log_path = model_save_path + '/logs'

print("")
print("========================================")
print("TRAINING FOR:")
print(f'bdcunet_input{RESIZE_HEIGHT}')
print(f'Training process 6 out of {total_proc}')
print(f'Model save path: {model_save_path}')
print("========================================")
print("")

model_train = model.bcdu_d3(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path, save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                                      patience=LEARNING_PATIENCE, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()


#################################################################################
# Train ESPNet
model_save_path = f'trained_models/{MODEL_DATE}/m_espnet/{MODEL_VERSION}'
tensorboard_log_path = model_save_path + '/logs'

print("")
print("========================================")
print("TRAINING FOR:")
print(f'espnet_input{RESIZE_HEIGHT}')
print(f'Training process 7 out of {total_proc}')
print(f'Model save path: {model_save_path}')
print("========================================")
print("")


model_train = ESPNet.ESPNet(RESIZE_HEIGHT, RESIZE_WIDTH, 3)
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path,
                                              save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=LEARNING_PATIENCE, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()

#################################################################################
# Train MultiResNet
model_save_path = f'trained_models/{MODEL_DATE}/m_multiresnet/{MODEL_VERSION}'
tensorboard_log_path = model_save_path + '/logs'

print("")
print("========================================")
print("TRAINING FOR:")
print(f'multiresnet_input{RESIZE_HEIGHT}')
print(f'Training process 8 out of {total_proc}')
print(f'Model save path: {model_save_path}')
print("========================================")
print("")

# model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train = MultiResUnet.MultiResUnet(RESIZE_HEIGHT, RESIZE_WIDTH, 3)
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path,
                                              save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                                      patience=LEARNING_PATIENCE, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()

#################################################################################
# Train DCUNet
model_save_path = f'trained_models/{MODEL_DATE}/m_dcunet/{MODEL_VERSION}'
tensorboard_log_path = model_save_path + '/logs'

print("")
print("========================================")
print("TRAINING FOR:")
print(f'dcunet_input{RESIZE_HEIGHT}')
print(f'Training process 9 out of {total_proc}')
print(f'Model save path: {model_save_path}')
print("========================================")
print("")

# model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train = DCUNet.DCUNet(RESIZE_HEIGHT, RESIZE_WIDTH, 3)
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path,
                                              save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=LEARNING_PATIENCE, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()
###################################################################################################
# Train pspnet with various backbone
for idx, each_proc in enumerate(psp_proc_list, start=10):
    model_type, backbone_type = each_proc

    model = sm.PSPNet(backbone_type, classes=1, activation=MODEL_ACTIVATION, input_shape=(PSP_RESIZE, PSP_RESIZE, 3),
                      encoder_weights='imagenet')

    model_save_path = f'trained_models/{MODEL_DATE}/m_{model_type}_{backbone_type}/{MODEL_VERSION}'
    tensorboard_log_path = model_save_path + '/logs'

    print("")
    print("========================================")
    print("TRAINING FOR:")
    print(f'{model_type}_{backbone_type}_input{PSP_RESIZE}')
    print(f'Training process {idx} out of {total_proc}')
    print(f'Model save path: {model_save_path}')
    print("========================================")
    print("")

    model.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
    model.summary()
    mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path, save_best_only=True,
                                                  save_weights_only=False, monitor='val_loss', verbose=1)
    reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                                          patience=LEARNING_PATIENCE, verbose=1)
    early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
    tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

    history = model.fit(train_batch_psp, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                        validation_data=test_batch_psp, validation_steps=test_size,
                        callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
    tf.keras.backend.clear_session()
###################################################################################################
# Train unet, fpn, linknet with various backbone
start_idx = 9 + len(psp_proc_list) + 1
for idx, each_proc in enumerate(model_proc_list, start=start_idx):
    model_type, backbone_type = each_proc

    if model_type == 'unet':
        model = sm.Unet(backbone_type, classes=1, activation=MODEL_ACTIVATION,
                        input_shape=(RESIZE_HEIGHT, RESIZE_WIDTH, 3), encoder_weights='imagenet')
    elif model_type == 'fpn':
        model = sm.FPN(backbone_type, classes=1, activation=MODEL_ACTIVATION,
                       input_shape=(RESIZE_HEIGHT, RESIZE_WIDTH, 3), encoder_weights='imagenet')
    elif model_type == 'linknet':
        model = sm.Linknet(backbone_type, classes=1, activation=MODEL_ACTIVATION,
                           input_shape=(RESIZE_HEIGHT, RESIZE_WIDTH, 3), encoder_weights='imagenet')
    else:
        print(f"WARNING: Model type {model_type} is not valid. Skipped")
        continue

    model_save_path = f'trained_models/{MODEL_DATE}/m_{model_type}_{backbone_type}/{MODEL_VERSION}'
    tensorboard_log_path = model_save_path + '/logs'

    print("")
    print("========================================")
    print("TRAINING FOR:")
    print(f'{model_type}_{backbone_type}_input{RESIZE_HEIGHT}')
    print(f'Training process {idx} out of {total_proc}')
    print(f'Model save path: {model_save_path}')
    print("========================================")
    print("")

    model.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
    model.summary()
    mcp_save = tf.keras.callbacks.ModelCheckpoint(model_save_path, save_best_only=True,
                                                  save_weights_only=False, monitor='val_loss', verbose=1)
    reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                                          patience=LEARNING_PATIENCE, verbose=1)
    early_stop = tf.keras.callbacks.EarlyStopping(patience=EARLY_STOP_PATIENCE, monitor='val_loss', verbose=1)
    tboard = tf.keras.callbacks.TensorBoard(log_dir=tensorboard_log_path)

    history = model.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                        validation_data=test_batch, validation_steps=test_size,
                        callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
    tf.keras.backend.clear_session()

