import os
import tensorflow as tf
from model import model
from model import DCUNet, MultiResUnet, ESPNet, DeepLab
import numpy as np
from tensorflow.keras.optimizers import Adam

AUTOTUNE = tf.data.experimental.AUTOTUNE

gpus = tf.config.list_physical_devices('GPU')
if gpus:
  # Restrict TensorFlow to only use the first GPU
  try:
    tf.config.set_visible_devices(gpus[1], 'GPU')
    logical_gpus = tf.config.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPU")
    # print('GPU SELECTED:')
    # print(gpus[0])
    # tf.config.experimental.set_virtual_device_configuration(gpus[0], [
    #     tf.config.experimental.VirtualDeviceConfiguration(memory_limit=9000)])
  except RuntimeError as e:
    # Visible devices must be set before GPUs have been initialized
    print(e)

TRAIN_IMG_DIR = '/data/hanif/hologram_segmentation_model/dataset/train_img'
TEST_IMG_DIR = '/data/hanif/hologram_segmentation_model/dataset/test_img'
# TRAIN_IMG_DIR = 'F:/Xendity/LabelMe/dataset/final_train_img'
# TEST_IMG_DIR = 'F:/Xendity/LabelMe/dataset/final_test_img'
EPOCHS = 1_000
STEPS_PER_EPOCH = 1_500
BATCH_SIZE = 1
IMG_HEIGH = 530
IMG_WIDTH = 840
# RESIZE_HEIGHT = 320
# RESIZE_WIDTH = 320
RESIZE_HEIGHT = 416
RESIZE_WIDTH = 416
MODEL_DATE = '210903'
MODEL_VERSION = '0003'
# Training parameters
OPT = 'adam'
TRAIN_LOSS = 'binary_crossentropy'
METRIC_LIST = ['accuracy']
#################################################################################
# Function to obtain the mask images
@tf.function
def parse_image(img_path: str) -> dict:
    image = tf.io.read_file(img_path)
    image = tf.image.decode_png(image, channels=3)
    image = tf.image.convert_image_dtype(image, tf.uint8)

    mask_path = tf.strings.regex_replace(img_path, 'img', 'mask')
    mask = tf.io.read_file(mask_path)

    mask = tf.image.decode_png(mask, channels=1)
    # mask = tf.where(mask == 255, np.dtype('uint8').type(0), mask)

    return {'image': image, 'segmentation_mask': mask}


# Function to normalize images
@tf.function
def normalize(input_image, input_mask):
    input_image = tf.cast(input_image, tf.float32) / 255.0
    input_mask = tf.cast(input_mask, tf.bool)
    # input_mask = tf.cast(input_mask, tf.uint8)
    # input_mask = tf.cast(input_mask, tf.float32) / 255.0
    return input_image, input_mask


# Function to prep data for training
@tf.function
def load_image(datapoint):
    input_image = tf.image.resize(datapoint['image'], (RESIZE_HEIGHT, RESIZE_WIDTH))
    input_mask = tf.image.resize(datapoint['segmentation_mask'], (RESIZE_HEIGHT, RESIZE_WIDTH))

    input_image, input_mask = normalize(input_image, input_mask)

    return input_image, input_mask
#################################################################################

# Dataset pipeline
TRAIN_IMG_DIR = TRAIN_IMG_DIR.replace('\\', '/')
TEST_IMG_DIR = TEST_IMG_DIR.replace('\\', '/')
train_img_directory = f'{TRAIN_IMG_DIR}/*.png'
test_img_directory = f'{TEST_IMG_DIR}/*.png'

train_data = tf.data.Dataset.list_files(train_img_directory)
test_data = tf.data.Dataset.list_files(test_img_directory)

# Get the mask images that correlates to the image names
train_parse = train_data.map(parse_image, num_parallel_calls=AUTOTUNE)
test_parse = test_data.map(parse_image, num_parallel_calls=AUTOTUNE)

# Prep image and mask for training
train = train_parse.map(load_image, num_parallel_calls=AUTOTUNE)
test = test_parse.map(load_image, num_parallel_calls=AUTOTUNE)

# train_batch = train.batch(BATCH_SIZE).cache().repeat().prefetch(buffer_size=AUTOTUNE)
# test_batch = test.batch(BATCH_SIZE).cache().repeat().prefetch(buffer_size=AUTOTUNE)
train_batch = train.batch(BATCH_SIZE).repeat()
test_batch = test.batch(BATCH_SIZE).repeat()

# Get test image size
test_size = len([i for i in os.listdir(TEST_IMG_DIR) if i.endswith('.png')])

image_train_batch, mask_train_batch = next(iter(test))

# #################################################################################
# # Train DeepLab Xception
#
# # model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train = DeepLab.deeplabv3_plus_xception(input_shape=(RESIZE_HEIGHT, RESIZE_WIDTH, 3), num_classes=1)
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
#
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_deeplab_xcep/{MODEL_VERSION}',
#                                               save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_deeplab_xcep/{MODEL_VERSION}/logs')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()

#################################################################################
# Train DeepLab Mobilenet

# model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train = DeepLab.deeplabv3_plus_mobile(input_shape=(RESIZE_HEIGHT, RESIZE_WIDTH, 3), num_classes=1)
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)

model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_deeplab_mobile/{MODEL_VERSION}',
                                              save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_deeplab_mobile/{MODEL_VERSION}/logs')

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()
#################################################################################
# Train UNET

# model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)

model_train.summary()

mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_unet/{MODEL_VERSION}',
                                              save_best_only=True, save_weights_only=False,
                                              monitor='val_loss', verbose=1)
reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_unet/{MODEL_VERSION}/logs')

history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
                          validation_data=test_batch, validation_steps=test_size,
                          callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)

tf.keras.backend.clear_session()
# #################################################################################
# # Train XNET model
#
# model_train = model.xnet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_xnet/{MODEL_VERSION}', save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_xnet/0002/{MODEL_VERSION}')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()
#
# #################################################################################
# # Train BDCUNet Float 16 model
#
# model_train = model.bcdu_d3_float16(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_bcdu_d3_float16/{MODEL_VERSION}', save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_bcdu_d3_float16/{MODEL_VERSION}/logs')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()
#
# #################################################################################
# # Train BDCUNet model
#
# model_train = model.bcdu_d3(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_bcdu_d3/{MODEL_VERSION}', save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_bcdu_d3/{MODEL_VERSION}/logs')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()
#
#
# #################################################################################
# # Train ESPNet
#
# # model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train = ESPNet.ESPNet(RESIZE_HEIGHT, RESIZE_WIDTH, 3)
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_ESPNet/{MODEL_VERSION}',
#                                               save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_ESPNet/{MODEL_VERSION}/logs')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()
#
# #################################################################################
# # Train MultiResNet
#
# # model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train = MultiResUnet.MultiResUnet(RESIZE_HEIGHT, RESIZE_WIDTH, 3)
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_MultiResNet/{MODEL_VERSION}',
#                                               save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_MultiResNet/{MODEL_VERSION}/logs')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()
#
# #################################################################################
# # Train DCUNet
#
# # model_train = model.unet(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train = DCUNet.DCUNet(RESIZE_HEIGHT, RESIZE_WIDTH, 3)
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_DCU/{MODEL_VERSION}',
#                                               save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_DCU/{MODEL_VERSION}/logs')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()
#
# #################################################################################
# # Train UNET_V2
#
# model_train = model.unet_v2(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_unetv2/{MODEL_VERSION}',
#                                               save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_unetv2/{MODEL_VERSION}/logs')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()
#
# #################################################################################
# # Train DCUNet
#
# model_train = model.unet_v3(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_unetv3/{MODEL_VERSION}',
#                                               save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_unetv3/{MODEL_VERSION}/logs')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()
#
# #################################################################################
# # Train DCUNet
#
# model_train = model.unet_v4(input_size=(RESIZE_HEIGHT, RESIZE_WIDTH, 3))
# model_train.compile(optimizer=OPT, loss=TRAIN_LOSS, metrics=METRIC_LIST)
# model_train.summary()
#
# mcp_save = tf.keras.callbacks.ModelCheckpoint(f'trained_models/{MODEL_DATE}/m_unetv4/{MODEL_VERSION}',
#                                               save_best_only=True, save_weights_only=False,
#                                               monitor='val_loss', verbose=1)
# reduce_lr_loss = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1)
# early_stop = tf.keras.callbacks.EarlyStopping(patience=10, monitor='val_loss', verbose=1)
# tboard = tf.keras.callbacks.TensorBoard(log_dir=f'trained_models/{MODEL_DATE}/m_unetv4/{MODEL_VERSION}/logs')
#
# history = model_train.fit(train_batch, epochs=EPOCHS, steps_per_epoch=STEPS_PER_EPOCH, verbose=1,
#                           validation_data=test_batch, validation_steps=test_size,
#                           callbacks=[mcp_save, reduce_lr_loss, tboard, early_stop],)
#
# tf.keras.backend.clear_session()