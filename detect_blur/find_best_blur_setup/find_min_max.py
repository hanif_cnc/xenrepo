"""
Find minimum and maximum values of FFT and laplacian from dataset
"""
import os
import pandas as pd

IN_LAP_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/laplacian_resized'
IN_FFT_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/FFT/img_ratio_2'


def obtain_dataframe(input_dir):
    # Get all folders and csv
    folder_list = [f'{input_dir}/{i}' for i in os.listdir(input_dir)]
    csv_list = []
    for each_folder in folder_list:
        for each_files in os.listdir(each_folder):
            if each_files.endswith('.csv'):
                csv_list.append(f'{each_folder}/{each_files}')

    # Concatenate csv results
    li = []
    folder_type_list = []
    for each_csv in csv_list:
        if 'Non_categorical' not in each_csv:
            df_tempt = pd.read_csv(each_csv)
            # Find current folder
            current_folder = each_csv.split('/')[-1].split('_')[-2]
            df_tempt['folder'] = current_folder
            li.append(df_tempt)
            folder_type_list.append(current_folder)

    df_all = pd.concat(li, axis=0, ignore_index=True)

    return df_all


def find_min_max(lap_dir, fft_dir):
    lap_dir = lap_dir.replace('\\', '/')
    fft_dir = fft_dir.replace('\\', '/')

    # Read CSV and create dataframe
    df_fft = obtain_dataframe(fft_dir)
    df_lap = obtain_dataframe(lap_dir)

    lap_max = df_lap['blur_val'].max()
    lap_min = df_lap['blur_val'].min()
    fft_max = df_fft['blur_val'].max()
    fft_min = df_fft['blur_val'].min()

    print("----LAPLACIAN----")
    print(f"Laplacian Max value: {lap_max:,}")
    print(f"Laplacian Min value: {lap_min:,}")
    print("----FFT----")
    print(f"FFT Max value: {fft_max:,}")
    print(f"FFT Min value: {fft_min:,}")


def main():
    find_min_max(IN_LAP_DIR, IN_FFT_DIR)


if __name__ == '__main__':
    main()
