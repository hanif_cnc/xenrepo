Model: trained_svm/svm_lap_fft_ver1.joblib

True Positive: 69
True Negative: 139
False Positive: 11
False Negative: 22

Precision: 0.86
Recall: 0.76
F1 score: 0.81

