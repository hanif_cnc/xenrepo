"""
Get accuracy metric for laplacian and FFT based on threshold settings only

Blur will be treated as a Positive detection (1). Sharp will be a negative detection (0)
Therefore, if a blur image is detected as Blur - it is considered as true positive
if a sharp image is detected as Blur - it is considered as false positive
"""

import os
import csv
import requests
import json
import base64
import pandas as pd
import numpy as np
from tqdm import tqdm
from sklearn.metrics import confusion_matrix
from concurrent.futures import ThreadPoolExecutor

IN_LAP_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/laplacian_resized'
IN_FFT_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/FFT/img_ratio_2'
SAVE_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/lap_fft_threshold_result'
LAP_THRESHOLD_LIST = [80 + (i * 2) for i in range(0, 70)]
FFT_THRESHOLD_LIST = [5 + (i * 1) for i in range(0, 70)]
LAP_IMAGE_OUT = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/lap_fft_threshold_result/laplacian/result'
FFT_IMAGE_OUT = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/lap_fft_threshold_result/fft/result'
DIR_LIST = ['F:/Xendity/Project_QC_Blur/Blur_Dataset_v1/clear',
            'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1/blur/MCblur']


def obtain_dataframe(input_dir):
    # Get all folders and csv
    folder_list = [[f'{input_dir}/{i}', i] for i in os.listdir(input_dir)]
    csv_list = []
    folder_selection = ['clear', 'MCblur']
    for each_folder in folder_list:
        if each_folder[1] in folder_selection:
            for each_files in os.listdir(each_folder[0]):
                if each_files.endswith('.csv'):
                    csv_list.append(f'{each_folder[0]}/{each_files}')

    # Concatenate csv results
    li = []
    folder_type_list = []
    for each_csv in csv_list:
        df_tempt = pd.read_csv(each_csv)
        # Find current folder
        current_folder = each_csv.split('/')[-1].split('_')[-2]
        df_tempt['folder'] = current_folder
        li.append(df_tempt)
        folder_type_list.append(current_folder)

    df_all = pd.concat(li, axis=0, ignore_index=True)

    return df_all


def lap_fft_result(lap_dir, fft_dir, outdir, lap_list, fft_list):
    print("LAPLACIAN AND FFT THRESHOLD PERFORMANCE MEASURMENT STARTS...")
    lap_dir = lap_dir.replace('\\', '/')
    fft_dir = fft_dir.replace('\\', '/')
    outdir = outdir.replace('\\', '/')
    os.makedirs(outdir, exist_ok=True)

    # Read CSV and create dataframe
    print("Reading dataset")
    df_fft = obtain_dataframe(fft_dir)
    del df_fft['img_ratio']
    df_lap = obtain_dataframe(lap_dir)

    laplacian_raw_output = f'{outdir}/laplacian/raw_data'
    laplacian_final_output = f'{outdir}/laplacian/result'
    os.makedirs(laplacian_raw_output, exist_ok=True)
    os.makedirs(laplacian_final_output, exist_ok=True)
    fft_raw_output = f'{outdir}/fft/raw_data'
    fft_final_output = f'{outdir}/fft/result'
    os.makedirs(fft_raw_output, exist_ok=True)
    os.makedirs(fft_final_output, exist_ok=True)

    # Iterate each laplacian
    print("Processing for laplacian")
    laplacian_csv = f'{laplacian_final_output}/laplacian_overall.csv'
    with open(laplacian_csv, 'w', newline='') as csv_file:
        csv_reader = csv.writer(csv_file, delimiter=',')
        csv_reader.writerow(['setup_type', 'true_positive', 'true_negative',
                             'false_positive', 'false_negative', 'precision', 'recall', 'f1_score', 'f2_score'])

    for each_thresh in tqdm(lap_list):

        df_compare = df_lap.copy()

        # Classify dataset category True or negative
        condition = [df_compare['folder'] == 'clear']
        choice = [0]
        df_compare['y_true'] = np.select(condition, choice, default=1)

        def perform_comparison(row):
            out_result = 0
            if row['blur_val'] < each_thresh:
                out_result = 1
            return out_result

        # Check if blur value is indicating sharp or blur depending on threshold set
        df_compare['y_pred'] = df_compare.apply(perform_comparison, axis=1)

        # Output raw result
        csv_raw_basename = f'laplacian_thresh{each_thresh}.csv'
        outpath1 = f'{laplacian_raw_output}/{csv_raw_basename}'
        df_compare.to_csv(outpath1)

        # Calculate performance metric and append result
        y_true = df_compare['y_true'].tolist()
        y_pred = df_compare['y_pred'].tolist()
        cm = confusion_matrix(y_true, y_pred)
        tn = cm[0][0]
        fn = cm[1][0]
        tp = cm[1][1]
        fp = cm[0][1]
        precision = tp / (tp + fp)
        recall = tp / (tp + fn)
        f1_score = 2 * (precision * recall) / (precision + recall)
        f2_score = ((1 + pow(2, 2)) * precision * recall) / (pow(2, 2) * precision + recall)

        with open(laplacian_csv, 'a', newline='') as csv_file:
            csv_reader = csv.writer(csv_file, delimiter=',')
            setup_name = f'laplacian_thresh{each_thresh}'
            csv_reader.writerow([setup_name, tp, tn, fp, fn, precision, recall, f1_score, f2_score])

    ######################################################################################################
    # Iterate each FFT
    print("Processing for FFT")
    fft_csv = f'{fft_final_output}/FFT_overall.csv'
    with open(fft_csv, 'w', newline='') as csv_file:
        csv_reader = csv.writer(csv_file, delimiter=',')
        csv_reader.writerow(['setup_type', 'true_positive', 'true_negative',
                             'false_positive', 'false_negative', 'precision', 'recall', 'f1_score', 'f2_score'])

    for each_thresh in tqdm(fft_list):
        df_compare = df_lap.copy()
        # Classify dataset category True or negative
        condition = [df_compare['folder'] == 'clear']
        choice = [0]
        df_compare['y_true'] = np.select(condition, choice, default=1)

        def perform_comparison(row):
            out_result = 0
            if row['blur_val'] < each_thresh:
                out_result = 1
            return out_result

        # Check if blur value is indicating sharp or blur depending on threshold set
        df_compare['y_pred'] = df_compare.apply(perform_comparison, axis=1)

        # Output raw result
        csv_raw_basename = f'fft_thresh{each_thresh}.csv'
        outpath1 = f'{fft_raw_output}/{csv_raw_basename}'
        df_compare.to_csv(outpath1)

        # Calculate performance metric and append result
        y_true = df_compare['y_true'].tolist()
        y_pred = df_compare['y_pred'].tolist()
        cm = confusion_matrix(y_true, y_pred)
        tn = cm[0][0]
        fn = cm[1][0]
        tp = cm[1][1]
        fp = cm[0][1]
        precision = tp / (tp + fp)
        recall = tp / (tp + fn)
        f1_score = 2 * (precision * recall) / (precision + recall)
        f2_score = ((1 + pow(2, 2)) * precision * recall) / (pow(2, 2) * precision + recall)

        with open(fft_csv, 'a', newline='') as csv_file:
            csv_reader = csv.writer(csv_file, delimiter=',')
            setup_name = f'fft_thresh{each_thresh}'
            csv_reader.writerow([setup_name, tp, tn, fp, fn, precision, recall, f1_score, f2_score])


def request_api(each_proc):
    # Determine which type to call
    if each_proc[-1] == 1:
        img_path, url, img_out_path, threshold, img_ratio_setting, __ = each_proc
        files = {'image1': open(img_path, 'rb'), 'img_ratio': (None, f'{img_ratio_setting}'),
                 'output_image': (None, 'True')}
    else:
        img_path, url, img_out_path, threshold, __ = each_proc
        files = {'image1': open(img_path, 'rb'), 'output_image': (None, 'True')}

    # Get image id
    img_basename = img_path.split('/')[-1]

    response = requests.post(url, files=files, timeout=5000)

    if response.status_code == 200:
        body = json.loads(response.text)

        blur_value = body['blur_score']

        img_out = body['output_image']
        imgdata = base64.b64decode(img_out)

        # Determine which category it belongs to
        current_folder = img_path.replace(f'/{img_basename}', '').split('/')[-1]

        if current_folder == 'clear':
            if blur_value > threshold:
                filename = f'{img_out_path}/true_negative/{img_basename}'
            else:
                filename = f'{img_out_path}/false_positive/{img_basename}'
        else:
            if blur_value < threshold:
                filename = f'{img_out_path}/true_positive/{img_basename}'
            else:
                filename = f'{img_out_path}/false_negative/{img_basename}'

        with open(filename, 'wb') as f:
            f.write(imgdata)


def draw_image(call_type, threshold, indir_list, outdir):

    if call_type == 'fft':
        print("Outputting image for FFT")
        current_type = 1
        url = 'http://localhost:8001/blur_fft_v1'
    else:
        print("Outputting image for Laplacian")
        current_type = 0
        url = 'http://localhost:8001/blur_laplacian_v1'

    # Prep output directory
    tp_folder = f'{outdir}/image_thresh{threshold}/true_positive'
    tn_folder = f'{outdir}/image_thresh{threshold}/true_negative'
    fp_folder = f'{outdir}/image_thresh{threshold}/false_positive'
    fn_folder = f'{outdir}/image_thresh{threshold}/false_negative'
    os.makedirs(tp_folder, exist_ok=True)
    os.makedirs(tn_folder, exist_ok=True)
    os.makedirs(fp_folder, exist_ok=True)
    os.makedirs(fn_folder, exist_ok=True)

    # Process for all input directory
    for each_input in indir_list:
        # Check current folder
        current_folder = each_input.split('/')[-1]
        print(f"Current folder: {current_folder}")

        # image input list
        if current_type == 1:
            img_proc_list = [[f"{each_input}/{i}", url, f'{outdir}/image_thresh{threshold}', threshold, 2, current_type]
                             for i in os.listdir(each_input)]
        else:
            img_proc_list = [[f"{each_input}/{i}", url, f'{outdir}/image_thresh{threshold}', threshold, current_type]
                             for i in os.listdir(each_input)]

        with ThreadPoolExecutor(max_workers=4) as executor:
            list(tqdm(executor.map(request_api, img_proc_list), total=len(img_proc_list)))




def main():
    # lap_fft_result(IN_LAP_DIR, IN_FFT_DIR, SAVE_DIR, LAP_THRESHOLD_LIST, FFT_THRESHOLD_LIST)

    # Process for laplacian
    draw_image('laplacian', 122, DIR_LIST, LAP_IMAGE_OUT)

    # Process for FFT
    draw_image('fft', 74, DIR_LIST, FFT_IMAGE_OUT)


if __name__ == '__main__':
    main()

