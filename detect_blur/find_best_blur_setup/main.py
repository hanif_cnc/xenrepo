"""
Main script to run svm training and performance metric
"""

from svm import train_svm, test_accuracy

IN_LAP_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/laplacian_resized'
IN_FFT_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/FFT/img_ratio_2'
SAVE_DIR = 'trained_svm'
MODEL_NAME = 'svm_lap_fft_ver1.joblib'
LAP_RANGE = {"max": 800, "min": 10}
FFT_RANGE = {"max": 60, "min": 25}
OUTPUT_CSV = 'raw_prediction_ver1.csv'
RESULT_OUTPUT = 'result_ver1.txt'


def main():
    train_svm(IN_LAP_DIR, IN_FFT_DIR, LAP_RANGE, FFT_RANGE, SAVE_DIR, MODEL_NAME)

    model_output = f'{SAVE_DIR}/{MODEL_NAME}'
    test_accuracy(IN_LAP_DIR, IN_FFT_DIR, LAP_RANGE, FFT_RANGE, model_output, OUTPUT_CSV, RESULT_OUTPUT)


if __name__ == '__main__':
    main()
