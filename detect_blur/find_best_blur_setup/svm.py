"""
Perform SVM using both Laplacian and FFT normalized values

For SVM input:
index 0: normalized laplacian value
index 1: normalized FFT value

Blur will be treated as a Positive detection (1). Sharp will be a negative detection (0)
Therefore, if a blur image is detected as Blur - it is considered as true positive
if a sharp image is detected as Blur - it is considered as false positive
"""

import os
import csv
import json
import shutil
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from joblib import dump, load
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report, confusion_matrix


IN_LAP_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/laplacian_resized'
IN_FFT_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/FFT/img_ratio_2'
IMAGE_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1'
SAVE_DIR = 'trained_svm'
OUTPUT_RESULT_SVM = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/svm'
MODEL_NAME = 'svm_lap_fft_ver1.joblib'
LAP_RANGE = {"max": 800, "min": 10}
FFT_RANGE = {"max": 60, "min": 25}
PARAM_GRID = {'C': [0.01, 0.1, 1, 10, 100],
              'gamma': [1, 0.1, 0.01, 0.001],
              'kernel': ['rbf', 'poly', 'sigmoid', 'linear']}
BEST_SVM_MODEL = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/svm/saved_best_model/best_svm.joblib'
BEST_SVM_OUTPUT = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/svm/best_model_report'


def obtain_dataframe(input_dir):
    # Get all folders and csv
    folder_list = [[f'{input_dir}/{i}', i] for i in os.listdir(input_dir)]
    csv_list = []
    folder_selection = ['clear', 'MCblur']
    for each_folder in folder_list:
        if each_folder[1] in folder_selection:
            for each_files in os.listdir(each_folder[0]):
                if each_files.endswith('.csv'):
                    csv_list.append(f'{each_folder[0]}/{each_files}')

    # Concatenate csv results
    li = []
    folder_type_list = []
    for each_csv in csv_list:
        df_tempt = pd.read_csv(each_csv)
        # Find current folder
        current_folder = each_csv.split('/')[-1].split('_')[-2]
        df_tempt['folder'] = current_folder
        li.append(df_tempt)
        folder_type_list.append(current_folder)

    df_all = pd.concat(li, axis=0, ignore_index=True)

    return df_all


def train_svm(lap_dir, fft_dir, lap_setting, fft_setting, outdir, model_out):
    print("TRAINING SVM START...")
    lap_dir = lap_dir.replace('\\', '/')
    fft_dir = fft_dir.replace('\\', '/')
    outdir = outdir.replace('\\', '/')
    os.makedirs(outdir, exist_ok=True)

    # Read CSV and create dataframe
    print("Reading dataset")
    df_fft = obtain_dataframe(fft_dir)
    del df_fft['img_ratio']
    df_lap = obtain_dataframe(lap_dir)

    # Normalize values
    # Laplacian
    df_lap['lap_val_norm'] = (df_lap['blur_val'] - lap_setting["min"]) / (
                lap_setting["max"] - lap_setting["min"])
    # FFT
    df_fft['fft_val_norm'] = (df_fft['blur_val'] - fft_setting["min"]) / (
                fft_setting["max"] - fft_setting["min"])

    # Combine dataset
    df = pd.merge(df_lap, df_fft, on=["image_id", "folder"])

    # Change MCBlur to 0 and clear to 1
    condition = [df['folder'] == 'clear']
    choice = [0]
    df['img_category'] = np.select(condition, choice, default=1)

    # Prepare data for svm
    print("Preparing dataset for svm")
    x = []
    lap_list = df['lap_val_norm'].tolist()
    fft_list = df['fft_val_norm'].tolist()
    for each_lap, each_fft in zip(lap_list, fft_list):
        x.append([each_lap, each_fft])

    y = df['img_category'].tolist()

    # Fitting SVM
    print("Fitting SVC")
    clf = SVC()
    clf.fit(x, y)
    print("Fitting completed!")

    output_path = f'{outdir}/{model_out}'
    dump(clf, output_path)

    print(f"Saved model to {output_path}")


def test_accuracy(lap_dir, fft_dir, img_dir, lap_setting, fft_setting, model_svm, output_path):
    print("EVALUATING SVM START...")
    print(f"model: {model_svm}")
    lap_dir = lap_dir.replace('\\', '/')
    fft_dir = fft_dir.replace('\\', '/')
    output_path = output_path.replace('\\', '/')
    os.makedirs(output_path, exist_ok=True)
    raw_csv = f'{output_path}/raw_classification.csv'
    result_csv = f'{output_path}/confusion_mat.csv'
    img_dir = img_dir.replace('\\', '/')

    img_out = f'{output_path}/img_output'
    tp_folder = f'{img_out}/true_positive'
    tn_folder = f'{img_out}/true_negative'
    fp_folder = f'{img_out}/false_positive'
    fn_folder = f'{img_out}/false_negative'
    os.makedirs(tp_folder, exist_ok=True)
    os.makedirs(tn_folder, exist_ok=True)
    os.makedirs(fp_folder, exist_ok=True)
    os.makedirs(fn_folder, exist_ok=True)

    # Read CSV and create dataframe
    print("Reading dataset")
    df_fft = obtain_dataframe(fft_dir)
    del df_fft['img_ratio']
    df_lap = obtain_dataframe(lap_dir)

    # Normalize values
    # Laplacian
    df_lap['lap_val_norm'] = (df_lap['blur_val'] - lap_setting["min"]) / (
                lap_setting["max"] - lap_setting["min"])
    # FFT
    df_fft['fft_val_norm'] = (df_fft['blur_val'] - fft_setting["min"]) / (
                fft_setting["max"] - fft_setting["min"])

    # Combine dataset
    df = pd.merge(df_lap, df_fft, on=["image_id", "folder"])

    # Change MCBlur to 0 and clear to 1
    condition = [df['folder'] == 'clear']
    choice = [0]
    df['img_category'] = np.select(condition, choice, default=1)

    # Load model
    print("Loading model")
    clf = load(model_svm)
    print("Model loaded")

    # Iterate through each dataset and get predictions
    print("Performing prediction")

    def predict(row):
        lap_val = row['lap_val_norm']
        fft_val = row['fft_val_norm']

        input_array = np.array([lap_val, fft_val]).reshape(1, -1)

        return clf.predict(input_array)[0]

    df['svm_prediction'] = df.apply(predict, axis=1)

    df.to_csv(raw_csv, index=False)

    # Tabulate results
    print("Tabulating results")
    y_true = df['img_category'].tolist()
    y_pred = df['svm_prediction'].tolist()
    cm = confusion_matrix(y_true, y_pred)
    tn = cm[0][0]
    fn = cm[1][0]
    tp = cm[1][1]
    fp = cm[0][1]
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1_score = 2 * (precision * recall) / (precision + recall)
    f2_score = ((1 + pow(2, 2)) * precision * recall) / (pow(2, 2) * precision + recall)

    with open(result_csv, 'w', newline='') as csv_file:
        csv_reader = csv.writer(csv_file, delimiter=',')
        setup_name = f'svm_best_model'
        csv_reader.writerow(['setup_type', 'true_positive', 'true_negative',
                             'false_positive', 'false_negative', 'precision', 'recall', 'f1_score', 'f2_score'])
        csv_reader.writerow([setup_name, tp, tn, fp, fn, precision, recall, f1_score, f2_score])

    # Output images
    print("Outputting sharp images")
    # For clear images
    clear_dir = f'{img_dir}/clear'
    df_clear = df.loc[df['img_category'] == 0]

    def clear_out(row):
        image_basename = row['image_id']
        pred_result = row['svm_prediction']

        if pred_result == 0:
            img_outpath = f'{tn_folder}/{image_basename}'
        else:
            img_outpath = f'{fp_folder}/{image_basename}'

        input_img_path = f'{clear_dir}/{image_basename}'
        shutil.copy2(input_img_path, img_outpath)

    df_clear.apply(clear_out, axis=1)

    # For MCBlur images
    print("Outputting blurred images")
    blur_dir = f'{img_dir}/blur/MCblur'
    df_blur = df.loc[df['img_category'] == 1]

    def blur_out(row):
        image_basename = row['image_id']
        pred_result = row['svm_prediction']

        if pred_result == 1:
            img_outpath = f'{tp_folder}/{image_basename}'
        else:
            img_outpath = f'{fn_folder}/{image_basename}'

        input_img_path = f'{blur_dir}/{image_basename}'
        shutil.copy2(input_img_path, img_outpath)

    df_blur.apply(blur_out, axis=1)


    print("Finished evaluation!")


def find_best_svm(lap_dir, fft_dir, lap_setting, fft_setting, param_setting, outdir_path):
    print("FINDING BEST SVM START...")
    lap_dir = lap_dir.replace('\\', '/')
    fft_dir = fft_dir.replace('\\', '/')
    outdir_path = outdir_path.replace('\\', '/')
    os.makedirs(outdir_path, exist_ok=True)
    outdir_report = f'{outdir_path}/report'
    os.makedirs(outdir_report, exist_ok=True)
    outdir_best_svm = f'{outdir_path}/saved_best_model'
    os.makedirs(outdir_best_svm, exist_ok=True)

    # Read CSV and create dataframe
    print("Reading dataset")
    df_fft = obtain_dataframe(fft_dir)
    del df_fft['img_ratio']
    df_lap = obtain_dataframe(lap_dir)

    # Normalize values
    # Laplacian
    df_lap['lap_val_norm'] = (df_lap['blur_val'] - lap_setting["min"]) / (
                lap_setting["max"] - lap_setting["min"])
    # FFT
    df_fft['fft_val_norm'] = (df_fft['blur_val'] - fft_setting["min"]) / (
                fft_setting["max"] - fft_setting["min"])

    # Combine dataset
    df = pd.merge(df_lap, df_fft, on=["image_id", "folder"])

    # Change MCBlur to 0 and clear to 1
    condition = [df['folder'] == 'clear']
    choice = [0]
    df['img_category'] = np.select(condition, choice, default=1)

    # Prepare data for svm
    print("Preparing dataset for svm")
    x = []
    lap_list = df['lap_val_norm'].tolist()
    fft_list = df['fft_val_norm'].tolist()
    for each_lap, each_fft in zip(lap_list, fft_list):
        x.append([each_lap, each_fft])

    y = df['img_category'].tolist()

    # Finding the best svm
    print("Finding best SVM...")
    grid = GridSearchCV(SVC(), param_setting, refit=True, verbose=2)
    grid.fit(x, y)
    print("")
    print("Found best SVM: ")
    print(grid.best_estimator_)

    print("Outputting report")
    df_result = pd.DataFrame(grid.cv_results_)
    output_path = f'{outdir_report}/svm_result.csv'
    df_result.to_csv(output_path, index=False)

    best_svm_path = f'{outdir_report}/svm_best_setting.txt'
    with open(best_svm_path, 'w') as f:
        string = json.dumps(grid.best_estimator_.get_params())
        f.write(string)

    y_pred = grid.predict(x)
    report = classification_report(y, y_pred, output_dict=True)
    df_confusion = pd.DataFrame(report).transpose()
    output2_path = f'{outdir_report}/confusion_matrix.csv'
    df_confusion.to_csv(output2_path)

    # Saving best model
    output3_path = f'{outdir_best_svm}/best_svm.joblib'
    print(f"Saving best model to: {output3_path}")
    dump(grid, output3_path)

    print("Finished!")


def main():
    train_svm(IN_LAP_DIR, IN_FFT_DIR, LAP_RANGE, FFT_RANGE, SAVE_DIR, MODEL_NAME)

    find_best_svm(IN_LAP_DIR, IN_FFT_DIR, LAP_RANGE, FFT_RANGE, PARAM_GRID, OUTPUT_RESULT_SVM)

    test_accuracy(IN_LAP_DIR, IN_FFT_DIR, IMAGE_DIR, LAP_RANGE, FFT_RANGE, BEST_SVM_MODEL, BEST_SVM_OUTPUT)


if __name__ == '__main__':
    main()
