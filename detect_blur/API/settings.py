from dotenv import load_dotenv
import os

load_dotenv()


def get_bool(key):
    return str2bool(os.getenv(key))


def get_int(key):
    return int(os.getenv(key))


def get_float(key):
    return float(os.getenv(key))


def get_str(key):
    return os.getenv(key)


def str2bool(s):
    return s.lower() in ["yes", "true", "t", "1"]
