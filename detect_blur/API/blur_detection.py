"""
BLUR DETECTION
Date: 21/6/2022
Version: 1.0
Author: Muhd Hanif

Main Objective:
To classify a MyKad image as sharp or blur. The classification
is based on the blur value obtained from various methods
of measuring sharpness of an image

Refer to blur_settings/blur.ini for script settings

In this script, there are three types of classes:
1) BlurLaplacian
- Get blur value using Laplacian Method
- input image is resized to ensure consistent result
2) BlurFft
- Get blur value using FFT Method
- Input image is not resized. Image ratio has to be
defined as part of FFT calculation settings
3) SvmBlur
- Combines both blur value using FFT and Laplacian methods.
- Based on blur values, a trained SVM classify if the image
is sharp or blur

Standard request JSON format:
1)image1: cropped Mykad image to be processed
2)blur_thresh(optional): Float. Set the blur threshold
5)draw_output(optional): output image result. Can be string of
the following: TRUE, true, 1, yes, y (Capital letters are lowered). Wrong or
no input will by default not draw any output image.

Refer to on_post function for JSON response
"""

import os
import re
import io
import cv2
import json
import time
import falcon
import base64
import numpy as np
import settings as s
from PIL import Image
import configparser
from joblib import dump, load


class BlurLaplacian:
    def __init__(self, config_path='blur_settings/blur.ini'):
        # Load configurations
        self.config = configparser.ConfigParser()
        self.config_path = config_path
        self.config_dict = self.get_config()

        # Initializer
        self.blur_threshold = self.config_dict['def_blur_threshold']
        self.draw_output = self.config_dict['def_draw_output']
        self.resize_height = self.config_dict['def_resize_height']
        self.resize_width = self.config_dict['def_resize_width']
        self.blur_threshold_cond = False

    def on_post(self, req, resp):
        """
        Check image blur by Laplacian (version 1). Resized image to default size before performing
        Laplacian operation.

        --Request--
        1)image1: cropped Mykad image to be processed
        2)blur_thresh(optional): Float. Set the blur threshold
        5)draw_output(optional): output image result. Can be string of
        the following: TRUE, true, 1, yes, y (Capital letters are lowered). Wrong or
        no input will by default not draw any output image.

        --Return--
        1) status: status of return
        2) blur_score: Value of blur based on FFT
        3) blur_verdict: Verdict based on blur threshold
        4) output_image: Resized image output if draw_output is set.
        5) blur_thresh: Indicate the blur threshold that was set
        6) draw_output: Indicate if outputting image was set
        7) img_size: Original image input size
        8) time_taken: Time taken to complete request process
        """
        on_post_time_start = time.time()
        print('Running blur detection Laplacian v1')

        try:
            body = req.stream.read().decode('utf-8')
            img1 = req.get_param('image1')

            if body != '':
                img1 = self.upload_application_json(body)
            elif img1 is not None:
                img1 = img1.file.read()
            else:
                resp.status = falcon.HTTP_400

        except AttributeError:
            img1 = None
            print("ERROR: Image parameter is empty. Please upload image.")

        # Check for additional parameters
        self.new_param_single(req)

        # Process image if no issues with request
        if img1 is None:
            status = re.search('\\d+\\s(.*)', resp.status,
                               re.IGNORECASE).group(1)
            resp.body = json.dumps(
                {
                    "status": status,
                    "error": "Image parameter is empty. Please upload image."
                }
            )
        elif not self.blur_threshold_cond:
            # Skip process if error in pixel threshold request
            resp.body = json.dumps(
                {
                    "status": falcon.HTTP_400,
                    "error": "blur_thresh parameter is not a valid float."
                }
            )

        else:
            print("Blur detection single frame request OK. Processing image")
            # Decode image received
            np_arr = np.fromstring(img1, np.uint8)
            img_read = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
            height, width, __ = img_read.shape

            # Resize image to default value
            if height == self.resize_height and width == self.resize_width:
                resize_img = img_read
            else:
                dim = (self.resize_width, self.resize_height)
                resize_img = cv2.resize(img_read, dim, interpolation=cv2.INTER_AREA)

            # Detect blur by laplacian method
            blur_val, blur_verdict, blur_img = self.blur_laplacian(resize_img)

            if self.draw_output:
                # Convert mask image to base64
                img = self.np_to_jpeg(blur_img)
                img = self.jpeg_to_base64(img)
                print(f"Blur score: {blur_val}, Blur verdict: {blur_verdict}")

                resp.body = json.dumps(
                    {
                        "status": 'OK',
                        "blur_score": blur_val,
                        "blur_verdict": blur_verdict,
                        "output_image": img.decode('utf-8'),
                        "blur_thresh": self.blur_threshold,
                        "draw_output": self.draw_output,
                        "img_size": {'height': height,
                                     'width': width},
                        "general_version": s.get_str("VER_GENERAL"),
                        "api_version": s.get_str("VER_SEGM_HOLO_V2_2"),
                        "time_taken": f'{t+ime.time() - on_post_time_start:.2f}'
                    }
                )

            else:
                # Convert mask image to base64
                print(f"Blur score: {blur_val}, Blur verdict: {blur_verdict}")

                resp.body = json.dumps(
                    {
                        "status": 'OK',
                        "blur_score": blur_val,
                        "blur_verdict": blur_verdict,
                        "blur_thresh": self.blur_threshold,
                        "draw_output": self.draw_output,
                        "img_size": {'height': height,
                                     'width': width},
                        "general_version": s.get_str("VER_GENERAL"),
                        "api_version": s.get_str("VER_SEGM_HOLO_V2_2"),
                        "time_taken": f'{time.time() - on_post_time_start:.2f}'
                    }
                )

    def blur_laplacian(self, img):

        # Get blur of laplacian value
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blur_val = cv2.Laplacian(gray, cv2.CV_64F).var()

        # Get blur verdict
        if blur_val > self.blur_threshold:
            blur_verdict = True
        else:
            blur_verdict = False

        # Put blur value text on image
        font = cv2.FONT_HERSHEY_SIMPLEX
        text_coord = (40, 40)
        scale = 1
        color = (0, 0, 255)
        thickness = 2
        line = 1

        blur_img = cv2.putText(img, f'Laplacian Blur: {blur_val:.1f}', text_coord,
                               font, scale, color, thickness, line)

        return blur_val, blur_verdict, blur_img

    def get_config(self):
        """
        Read and prepare configuration settings
        :return: dict configuration settings
        """
        self.config.read(self.config_path)
        config_dict = dict()

        config_dict['def_blur_threshold'] = float(self.config['LAPLACIAN']['def_blur_threshold'])
        config_dict['def_draw_output'] = bool(self.config['LAPLACIAN']['def_draw_output'])
        config_dict['def_resize_height'] = int(self.config['LAPLACIAN']['def_resize_height'])
        config_dict['def_resize_width'] = int(self.config['LAPLACIAN']['def_resize_width'])

        return config_dict

    def new_param_single(self, req):
        # check if parameter pixel_thresh exist
        if 'blur_thresh' in req.params.keys():
            self.blur_threshold = req.get_param('blur_thresh')
        else:
            self.blur_threshold = self.config_dict['def_blur_threshold']

        try:
            self.blur_threshold = float(self.blur_threshold)
            print(f"Using new blur threshold: {self.blur_threshold}")
            self.blur_threshold_cond = True
        except ValueError:
            print("ERROR: blur_thresh parameter is not a valid float.")
            self.blur_threshold_cond = False

        # check if parameter draw_output exist
        if 'draw_output' in req.params.keys():
            self.draw_output = req.get_param('draw_output').lower() in ['true', '1', 't', 'y', 'yes']
        else:
            self.draw_output = self.config_dict['def_draw_output']

    @staticmethod
    def upload_application_json(body):
        """
        Read request that is in json format

        :param body: json request that contains image in base64
        :return: decoded based64 image
        """
        body = body.replace('\n', '')
        body_as_json = json.loads(body)
        image_file_as_base64 = body_as_json['image1']

        image_file_as_base64 = image_file_as_base64.split('base64,')
        image = base64.b64decode(image_file_as_base64[1])

        return image

    @staticmethod
    def np_to_jpeg(image):
        """
        :param image: Image in np array format
        :return: Image as JPEG.
        """
        return Image.fromarray(image[:, :, ::-1])

    @staticmethod
    def jpeg_to_base64(image):
        """
        :param image: Image in jpeg format.
        :return: Image in Base64 format.
        """
        buffer_ = io.BytesIO()
        image.save(buffer_, format="JPEG")
        return base64.b64encode(buffer_.getvalue())


class BlurFft:
    def __init__(self, config_path='blur_settings/blur.ini'):
        # Load configurations
        self.config = configparser.ConfigParser()
        self.config_path = config_path
        self.config_dict = self.get_config()

        # Initializer
        self.blur_threshold = self.config_dict['def_blur_threshold']
        self.img_ratio = self.config_dict['def_radius_img_size_percent']
        self.draw_output = self.config_dict['def_draw_output']
        self.blur_threshold_cond = False
        self.img_ratio_cond = False
        self.rad_size_x, self.rad_size_y = 0, 0

    def on_post(self, req, resp):
        """
        Check image blur by FFT (version 1)

        --Request--
        1)image1: cropped Mykad image to be processed
        2)blur_thresh(optional): Float. Set the blur threshold
        3)img_ratio(optional): Integer. Set the image percentage ratio for setting the radian size
        5)draw_output(optional): output image result. Can be string of
        the following: TRUE, true, 1, yes, y (Capital letters are lowered). Wrong or
        no input will by default not draw any output image.

        --Return--
        1) status: status of return
        2) blur_score: Value of blur based on FFT
        3) blur_verdict: Verdict based on blur threshold
        4) output_image: Image output if draw_output is set
        5) blur_thresh: Indicate the blur threshold that was set
        6) img_ratio: Indicate the image ratio that was set
        7) draw_output: Indicate if outputting image was set
        8) img_size: Original image input size
        9) time_taken: Time taken to complete request process
        """
        on_post_time_start = time.time()
        print('Running blur detection FFT v1')

        try:
            body = req.stream.read().decode('utf-8')
            img1 = req.get_param('image1')

            if body != '':
                img1 = self.upload_application_json(body)
            elif img1 is not None:
                img1 = img1.file.read()
            else:
                resp.status = falcon.HTTP_400

        except AttributeError:
            img1 = None
            print("ERROR: Image parameter is empty. Please upload image.")

        # Check for additional parameters for single image process
        self.check_param_single(req)

        # Process image if no issues with request
        if img1 is None:
            status = re.search('\\d+\\s(.*)', resp.status,
                               re.IGNORECASE).group(1)
            resp.body = json.dumps(
                {
                    "status": status,
                    "error": "Image parameter is empty. Please upload image."
                }
            )
        elif not self.blur_threshold_cond:
            # Skip process if error in pixel threshold request
            resp.body = json.dumps(
                {
                    "status": falcon.HTTP_400,
                    "error": "blur_thresh parameter is not a valid float."
                }
            )

        elif not self.img_ratio_cond:
            # Skip process if error in pixel threshold request
            resp.body = json.dumps(
                {
                    "status": falcon.HTTP_400,
                    "error": "img_ratio parameter is not a valid integer."
                }
            )

        else:
            print("Blur detection single frame request OK. Processing image")
            # Decode image received
            np_arr = np.fromstring(img1, np.uint8)
            img_read = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

            # Find height and width of image
            height, width, __ = img_read.shape

            # Obtain radian size to use for fft by using the specified image size ratio
            self.rad_size_x = int(width * (self.img_ratio/100))
            self.rad_size_y = int(height * (self.img_ratio / 100))

            # Detect blur by laplacian method
            blur_val, blur_verdict, blur_img = self.blur_fft(img_read, self.rad_size_x, self.rad_size_y)

            if self.draw_output:
                # Convert mask image to base64
                img = self.np_to_jpeg(blur_img)
                img = self.jpeg_to_base64(img)
                print(f"Blur score: {blur_val}, Blur verdict: {blur_verdict}, Draw output")

                resp.body = json.dumps(
                    {
                        "status": 'OK',
                        "blur_score": blur_val,
                        "blur_verdict": blur_verdict,
                        "output_image": img.decode('utf-8'),
                        "blur_thresh": self.blur_threshold,
                        "img_ratio": self.img_ratio,
                        "draw_output": self.draw_output,
                        "img_size": {'height': height,
                                     'width': width},
                        "general_version": s.get_str("VER_GENERAL"),
                        "api_version": s.get_str("VER_SEGM_HOLO_V2_2"),
                        "time_taken": f'{time.time() - on_post_time_start:.2f}'
                    }
                )

            else:
                # Convert mask image to base64
                print(f"Blur score: {blur_val}, Blur verdict: {blur_verdict}")

                resp.body = json.dumps(
                    {
                        "status": 'OK',
                        "blur_score": blur_val,
                        "blur_verdict": blur_verdict,
                        "blur_thresh": self.blur_threshold,
                        "img_ratio": self.img_ratio,
                        "draw_output": self.draw_output,
                        "img_size": {'height': height,
                                     'width': width},
                        "general_version": s.get_str("VER_GENERAL"),
                        "api_version": s.get_str("VER_SEGM_HOLO_V2_2"),
                        "time_taken": f'{time.time() - on_post_time_start:.2f}'
                    }
                )

    def blur_fft(self, img, size_x, size_y):

        # Get blur value by FFT method
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        (h, w) = gray.shape
        (cX, cY) = (int(w / 2.0), int(h / 2.0))
        fft = np.fft.fft2(gray)
        fft_shift = np.fft.fftshift(fft)
        fft_shift[cY - size_y:cY + size_y, cX - size_x:cX + size_x] = 0
        fft_shift = np.fft.ifftshift(fft_shift)
        recon = np.fft.ifft2(fft_shift)
        magnitude = 20 * np.log(np.abs(recon) + 1)
        blur_val = np.mean(magnitude)

        # Get blur verdict
        if blur_val > self.blur_threshold:
            blur_verdict = True
        else:
            blur_verdict = False

        # Output image if required
        if self.draw_output:
            # Put blur value text on image
            font = cv2.FONT_HERSHEY_SIMPLEX
            text_coord = (40, 40)
            scale = 1
            color = (0, 0, 255)
            thickness = 2
            line = 1

            blur_img = cv2.putText(img, f'FFT Blur: {blur_val:.1f}', text_coord, font, scale, color, thickness, line)

            return blur_val, blur_verdict, blur_img
        else:
            return blur_val, blur_verdict, 0

    def get_config(self):
        """
        Read and prepare configuration settings
        :return: dict configuration settings
        """
        self.config.read(self.config_path)
        config_dict = dict()

        config_dict['def_blur_threshold'] = float(self.config['FFT']['def_blur_threshold'])
        config_dict['def_radius_img_size_percent'] = int(self.config['FFT']['def_radius_img_size_percent'])
        config_dict['def_draw_output'] = bool(self.config['FFT']['def_draw_output'])

        return config_dict

    def check_param_single(self, req):
        """
        Check if additional request parameters are valid
        """
        # check if parameter blur_thresh exist
        if 'blur_thresh' in req.params.keys():
            self.blur_threshold = req.get_param('blur_thresh')
        else:
            self.blur_threshold = self.config_dict['def_blur_threshold']

        try:
            self.blur_threshold = float(self.blur_threshold)
            print(f"Using new blur threshold: {self.blur_threshold}")
            self.blur_threshold_cond = True
        except ValueError:
            print("ERROR: blur_thresh parameter is not a valid float.")
            self.blur_threshold_cond = False

        # check if parameter img_ratio exist
        if 'img_ratio' in req.params.keys():
            self.img_ratio = req.get_param('img_ratio')
        else:
            self.img_ratio = self.config_dict['def_radius_img_size_percent']

        try:
            self.img_ratio = int(self.img_ratio)
            print(f"Using new FFT image ratio percentage: {self.img_ratio}")
            self.img_ratio_cond = True
        except ValueError:
            print("ERROR: img_ratio parameter is not a valid int.")
            self.img_ratio_cond = False

        # check if parameter draw_output exist
        if 'draw_output' in req.params.keys():
            self.draw_output = req.get_param('draw_output').lower() in ['true', '1', 't', 'y', 'yes']
        else:
            self.draw_output = self.config_dict['def_draw_output']

    @staticmethod
    def upload_application_json(body):
        """
        Read request that is in json format

        :param body: json request that contains image in base64
        :return: decoded based64 image
        """
        body = body.replace('\n', '')
        body_as_json = json.loads(body)
        image_file_as_base64 = body_as_json['image1']

        image_file_as_base64 = image_file_as_base64.split('base64,')
        image = base64.b64decode(image_file_as_base64[1])

        return image

    @staticmethod
    def np_to_jpeg(image):
        """
        :param image: Image in np array format
        :return: Image as JPEG.
        """
        return Image.fromarray(image[:, :, ::-1])

    @staticmethod
    def jpeg_to_base64(image):
        """
        :param image: Image in jpeg format.
        :return: Image in Base64 format.
        """
        buffer_ = io.BytesIO()
        image.save(buffer_, format="JPEG")
        return base64.b64encode(buffer_.getvalue())


class SvmBlur:

    def __init__(self, config_path='blur_settings/blur.ini'):
        # Load configurations
        self.config = configparser.ConfigParser()
        self.config_path = config_path
        self.config_dict = self.get_config()

        # Initializer
        self.img_ratio = self.config_dict['def_fft_radius_img_size_percent']
        self.draw_output = self.config_dict['def_draw_output']
        self.resize_height = self.config_dict['def_lap_resize_height']
        self.resize_width = self.config_dict['def_lap_resize_width']
        self.img_ratio_cond = False

        # Load SVM
        self.svm_clf = load(self.config_dict['def_svm_model_path'])

    def on_post(self, req, resp):
        """
        Check image blur by SVM of Laplacian (resized image) and FFT. Both Laplacian and FFT values will be
        normalized in a range of 0 to 1.

        --Request--
        1)image1: cropped Mykad image to be processed
        5)draw_output(optional): output image result. Can be string of
        the following: TRUE, true, 1, yes, y (Capital letters are lowered). Wrong or
        no input will by default not draw any output image.

        --Return--
        1) status: status of return
        2) blur_score: Value of blur based on FFT
        3) blur_verdict: Verdict based on blur threshold
        4) output_image: Resized image output if draw_output is set.
        6) draw_output: Indicate if outputting image was set
        7) img_size: Original image input size
        8) time_taken: Time taken to complete request process
        """
        on_post_time_start = time.time()
        print('Running blur detection SVM ver1')

        try:
            body = req.stream.read().decode('utf-8')
            img1 = req.get_param('image1')

            if body != '':
                img1 = self.upload_application_json(body)
            elif img1 is not None:
                img1 = img1.file.read()
            else:
                resp.status = falcon.HTTP_400

        except AttributeError:
            img1 = None
            print("ERROR: Image parameter is empty. Please upload image.")

        # Check for additional parameters
        self.new_param_single(req)

        # Process image if no issues with request
        if img1 is None:
            status = re.search('\\d+\\s(.*)', resp.status,
                               re.IGNORECASE).group(1)
            resp.body = json.dumps(
                {
                    "status": status,
                    "error": "Image parameter is empty. Please upload image."
                }
            )

        else:
            print("Blur detection single frame request OK. Processing image")
            # Decode image received
            np_arr = np.fromstring(img1, np.uint8)
            img_read = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
            height, width, __ = img_read.shape

            # Detect blur by laplacian method
            output_result = self.blur_svm(img_read, height, width)
            blur_val_lap, blur_val_fft, val_norm_lap, val_norm_fft, blur_verdict, blur_img = output_result

            if self.draw_output:
                # Convert mask image to base64
                img = self.np_to_jpeg(blur_img)
                img = self.jpeg_to_base64(img)
                print(f"Blur score Laplacian: {blur_val_lap}, Blur score FFT: {blur_val_fft}, "
                      f"Blur verdict: {blur_verdict}")

                resp.body = json.dumps(
                    {
                        "status": 'OK',
                        "blur_score_lap": blur_val_lap,
                        "blur_score_fft": blur_val_fft,
                        "blur_norm_lap": val_norm_lap,
                        "blur_norm_fft": val_norm_fft,
                        "blur_verdict": blur_verdict,
                        "output_image": img.decode('utf-8'),
                        "draw_output": self.draw_output,
                        "img_size_original": {'height': height, 'width': width},
                        "general_version": s.get_str("VER_GENERAL"),
                        "api_version": s.get_str("VER_SEGM_HOLO_V2_2"),
                        "time_taken": f'{time.time() - on_post_time_start:.2f}'
                    }
                )

            else:
                # Convert mask image to base64
                print(f"Blur score Laplacian: {blur_val_lap}, Blur score FFT: {blur_val_fft}, "
                      f"Blur verdict: {blur_verdict}")

                resp.body = json.dumps(
                    {
                        "status": 'OK',
                        "blur_score_lap": blur_val_lap,
                        "blur_score_fft": blur_val_fft,
                        "blur_norm_lap": val_norm_lap,
                        "blur_norm_fft": val_norm_fft,
                        "blur_verdict": blur_verdict,
                        "draw_output": self.draw_output,
                        "img_size_original": {'height': height, 'width': width},
                        "general_version": s.get_str("VER_GENERAL"),
                        "api_version": s.get_str("VER_SEGM_HOLO_V2_2"),
                        "time_taken": f'{time.time() - on_post_time_start:.2f}'
                    }
                )

    def blur_svm(self, img, height, width):

        # Resize image to default value
        if height == self.resize_height and width == self.resize_width:
            resize_img = img
        else:
            dim = (self.resize_width, self.resize_height)
            resize_img = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

        # Get blur of laplacian value from resized image
        gray = cv2.cvtColor(resize_img, cv2.COLOR_BGR2GRAY)
        blur_val_lap = cv2.Laplacian(gray, cv2.CV_64F).var()

        # Get blur value by FFT method from original image
        # Obtain radian size to use for fft by using the specified image size ratio
        size_x = int(width * (self.img_ratio / 100))
        size_y = int(height * (self.img_ratio / 100))

        gray2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        (h, w) = gray2.shape
        (cX, cY) = (int(w / 2.0), int(h / 2.0))
        fft = np.fft.fft2(gray2)
        fft_shift = np.fft.fftshift(fft)
        fft_shift[cY - size_y:cY + size_y, cX - size_x:cX + size_x] = 0
        fft_shift = np.fft.ifftshift(fft_shift)
        recon = np.fft.ifft2(fft_shift)
        magnitude = 20 * np.log(np.abs(recon) + 1)
        blur_val_fft = np.mean(magnitude)

        # Normalize laplacian and FFT values
        val_norm_lap = (blur_val_lap - self.config_dict['def_lap_min_norm']) / (
                self.config_dict['def_lap_max_norm'] - self.config_dict['def_lap_min_norm'])
        # FFT
        val_norm_fft = (blur_val_fft - self.config_dict['def_fft_min_norm']) / (
                self.config_dict['def_fft_max_norm'] - self.config_dict['def_fft_min_norm'])

        if val_norm_lap > 1.0:
            val_norm_lap = 1.0
        elif val_norm_lap < 0.0:
            val_norm_lap = 0.0

        if val_norm_fft > 1.0:
            val_norm_fft = 1.0
        elif val_norm_fft < 0.0:
            val_norm_fft = 0.0

        # Get blur verdict from svm
        input_array = np.array([val_norm_lap, val_norm_fft]).reshape(1, -1)
        blur_verdict = bool(self.svm_clf.predict(input_array)[0])
        print(blur_verdict)

        # Draw image if required
        if self.draw_output:
            # Put blur value text on image
            font = cv2.FONT_HERSHEY_SIMPLEX
            text_coord = (40, 40)
            scale = 1
            color = (0, 0, 255)
            thickness = 2
            line = 1

            blur_img = cv2.putText(resize_img, f'Laplacian Blur: {blur_val_lap:.1f}, FFT Blur: {blur_val_fft:.1f}',
                                   text_coord, font, scale, color, thickness, line)

            return blur_val_lap, blur_val_fft, val_norm_lap, val_norm_fft, blur_verdict, blur_img
        else:
            return blur_val_lap, blur_val_fft, val_norm_lap, val_norm_fft, blur_verdict, 0

    def get_config(self):
        """
        Read and prepare configuration settings
        :return: dict configuration settings
        """
        self.config.read(self.config_path)
        config_dict = dict()

        config_dict['def_lap_resize_height'] = int(self.config['SVM']['def_lap_resize_height'])
        config_dict['def_lap_resize_width'] = int(self.config['SVM']['def_lap_resize_width'])
        config_dict['def_lap_max_norm'] = int(self.config['SVM']['def_lap_max_norm'])
        config_dict['def_lap_min_norm'] = int(self.config['SVM']['def_lap_min_norm'])
        config_dict['def_fft_radius_img_size_percent'] = int(self.config['SVM']['def_fft_radius_img_size_percent'])
        config_dict['def_fft_max_norm'] = int(self.config['SVM']['def_fft_max_norm'])
        config_dict['def_fft_min_norm'] = int(self.config['SVM']['def_fft_min_norm'])
        config_dict['def_draw_output'] = bool(self.config['SVM']['def_draw_output'])
        config_dict['def_svm_model_path'] = self.config['SVM']['def_svm_model_path']

        return config_dict

    def new_param_single(self, req):
        # check if parameter draw_output exist
        if 'draw_output' in req.params.keys():
            self.draw_output = req.get_param('draw_output').lower() in ['true', '1', 't', 'y', 'yes']
        else:
            self.draw_output = self.config_dict['def_draw_output']

    @staticmethod
    def upload_application_json(body):
        """
        Read request that is in json format

        :param body: json request that contains image in base64
        :return: decoded based64 image
        """
        body = body.replace('\n', '')
        body_as_json = json.loads(body)
        image_file_as_base64 = body_as_json['image1']

        image_file_as_base64 = image_file_as_base64.split('base64,')
        image = base64.b64decode(image_file_as_base64[1])

        return image

    @staticmethod
    def np_to_jpeg(image):
        """
        :param image: Image in np array format
        :return: Image as JPEG.
        """
        return Image.fromarray(image[:, :, ::-1])

    @staticmethod
    def jpeg_to_base64(image):
        """
        :param image: Image in jpeg format.
        :return: Image in Base64 format.
        """
        buffer_ = io.BytesIO()
        image.save(buffer_, format="JPEG")
        return base64.b64encode(buffer_.getvalue())