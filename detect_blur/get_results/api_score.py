"""
Obtain API results and store in CSV for later processing
"""
import os
import csv
import json
import requests
from concurrent.futures import ThreadPoolExecutor
import base64
from tqdm import tqdm


DIR_LIST = ['F:/Xendity/Project_QC_Blur/Blur_Dataset_v1/clear',
            'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1/blur/MCblur',
            'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1/blur/Noisy',
            'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1/blur/Non_categorical',
            'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1/blur/OCRblur']

DIR_OUTPUT = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output'

# URL_LAPLACIAN = 'http://172.16.2.156:8001/blur_laplacian_v1'
# URL_FFT = 'http://172.16.2.156:8001/blur_fft_v1'
URL_LAPLACIAN = 'http://localhost:8001/blur_laplacian_v1'
URL_FFT = 'http://localhost:8001/blur_fft_v1'

# For localhost
# os.environ['NO_PROXY'] = '0.0.0.0'


def request_api_laplacian(each_proc):
    img_path, img_out = each_proc
    # Get image id
    img_basename = img_path.split('/')[-1]

    files = {'image1': open(img_path, 'rb'), 'output_image': (None, 'True')}

    response = requests.post(URL_LAPLACIAN, files=files, timeout=5000)

    if response.status_code == 200:
        body = json.loads(response.text)

        blur_value = body['blur_score']
        filename = f'{img_out}/{img_basename}'

        img_out = body['output_image']
        imgdata = base64.b64decode(img_out)
        with open(filename, 'wb') as f:
            f.write(imgdata)

        return [img_basename, blur_value]
    else:
        return []


def request_api_fft(proc_list):
    img_path, img_ratio_setting, img_out = proc_list
    # Get image id
    img_basename = img_path.split('/')[-1]

    files = {'image1': open(img_path, 'rb'), 'img_ratio': (None, f'{img_ratio_setting}'),
             'output_image': (None, 'True')}

    response = requests.post(URL_FFT, files=files, timeout=5000)

    if response.status_code == 200:
        body = json.loads(response.text)

        blur_value = body['blur_score']
        filename = f'{img_out}/{img_basename}'

        img_out = body['output_image']
        imgdata = base64.b64decode(img_out)
        with open(filename, 'wb') as f:
            f.write(imgdata)

        return [img_basename, img_ratio_setting, blur_value]
    else:
        return []


###################################################################################
# Process for Laplacian method
lap_result_dir = f'{DIR_OUTPUT}/laplacian_resized'
os.makedirs(lap_result_dir, exist_ok=True)

with ThreadPoolExecutor(max_workers=4) as executor:
    for idx_folder, each_input_dir in enumerate(DIR_LIST, start=1):
        current_folder = each_input_dir.split('/')[-1]
        print("---Processing for Laplacian method----")
        print(f"Folder {idx_folder} out of {len(DIR_LIST)}")
        print(f"Current folder: {current_folder}")

        # prep report file
        output_folder = f'{lap_result_dir}/{current_folder}'
        os.makedirs(output_folder, exist_ok=True)
        csv_result = f'{output_folder}/laplacian_{current_folder}_result.csv'

        with open(csv_result, 'w', newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=',')
            writer.writerow(["image_id", "blur_val"])

        # image folder
        img_out_folder = f'{output_folder}/img_out'
        os.makedirs(img_out_folder, exist_ok=True)

        # image input list
        img_proc_list = [[f"{each_input_dir}/{i}", img_out_folder] for i in os.listdir(each_input_dir)]

        lap_sharp_result = list(tqdm(executor.map(request_api_laplacian, img_proc_list), total=len(img_proc_list)))

        with open(csv_result, 'a', newline='') as csv_file:
            writer1 = csv.writer(csv_file, delimiter=',')

            for each_res in lap_sharp_result:
                img_id, blur_val = each_res
                writer1.writerow([img_id, round(float(blur_val), 2)])
###################################################################################
# Process for FFT method
lap_result_dir = f'{DIR_OUTPUT}/FFT'
os.makedirs(lap_result_dir, exist_ok=True)

# Make list of possible rad_size
img_ratio = [2 + i*2 for i in range(15)]

with ThreadPoolExecutor(max_workers=4) as executor:
    for idx_ratio, each_ratio in enumerate(img_ratio, start=1):
        print("---Processing for FFT method----")
        print(f"Image Ratio {idx_ratio} out of {len(img_ratio)}")
        print(f"Current ratio: {each_ratio}")

        for idx_folder, each_input_dir in enumerate(DIR_LIST, start=1):
            current_folder = each_input_dir.split('/')[-1]

            print(f"Folder {idx_folder} out of {len(DIR_LIST)}")
            print(f"Current folder: {current_folder}")

            # prep report file
            output_folder = f'{lap_result_dir}/img_ratio_{each_ratio}/{current_folder}'
            os.makedirs(output_folder, exist_ok=True)
            csv_result = f'{output_folder}/fft_ratio{each_ratio}_{current_folder}_result.csv'

            with open(csv_result, 'w', newline='') as csv_file:
                writer = csv.writer(csv_file, delimiter=',')
                writer.writerow(["image_id", "img_ratio", "blur_val"])

            # image folder
            img_out_folder = f'{output_folder}/img_out'
            os.makedirs(img_out_folder, exist_ok=True)

            # image input list
            img_proc_list = [[f"{each_input_dir}/{i}", each_ratio, img_out_folder] for i in os.listdir(each_input_dir)]

            lap_sharp_result = list(tqdm(executor.map(request_api_fft, img_proc_list), total=len(img_proc_list)))

            with open(csv_result, 'a', newline='') as csv_file:
                writer1 = csv.writer(csv_file, delimiter=',')

                for each_res in lap_sharp_result:
                    img_id, img_ratio_value, blur_val = each_res
                    writer1.writerow([img_id, img_ratio_value, round(float(blur_val), 2)])

print("ALL FINISHED!!")
###################################################################################

