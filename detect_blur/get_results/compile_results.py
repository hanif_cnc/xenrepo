import pandas as pd
import os
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.stats import ttest_ind
import csv
from tqdm import tqdm
import numpy as np

OUT_DIR = 'results'
IN_LAP_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/laplacian_resized'
IN_FFT_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/FFT'
SHARP_FOLDER = 'clear'


def laplacianstat(input_folder, output_folder, sharp_data):
    print("Processing for laplacian")
    # Ensure correct format
    input_folder = input_folder.replace('\\', '/')
    output_folder = output_folder.replace('\\', '/') + '/laplacian_resized'
    os.makedirs(output_folder, exist_ok=True)

    # Get all folders and csv
    folder_list = [f'{input_folder}/{i}' for i in os.listdir(input_folder)]
    csv_list = []
    for each_folder in folder_list:
        for each_files in os.listdir(each_folder):
            if each_files.endswith('.csv'):
                csv_list.append(f'{each_folder}/{each_files}')

    # Concatenate csv results
    li = []
    folder_type_list = []
    for each_csv in csv_list:
        if 'Non_categorical' not in each_csv:
            df_tempt = pd.read_csv(each_csv)
            # Find current folder
            current_folder = each_csv.split('/')[-1].split('_')[1]
            df_tempt['folder'] = current_folder
            li.append(df_tempt)
            folder_type_list.append(current_folder)

    df_all = pd.concat(li, axis=0, ignore_index=True)

    # Draw boxplot
    sns.set(style="whitegrid")
    sns.boxplot(x="folder", y="blur_val", data=df_all, palette="PRGn",
                showmeans=True,
                meanprops={"marker": "o",
                           "markerfacecolor": "white",
                           "markeredgecolor": "black",
                           "markersize": "5"})
    sns.swarmplot(x="folder", y="blur_val", data=df_all, color=".25", alpha=.5)
    plot_savepath = f'{output_folder}/laplacian_resized_boxplot.png'
    plt.savefig(plot_savepath)
    plt.clf()

    csv_result = f'{output_folder}/laplacian_resized_report.csv'
    with open(csv_result, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["folder_type", "median", "mean", "Std_dev", "Student_t_test", "p < 0.05", "p < 0.01",
                         "Q3 quartile", "Q1 quartile"])

    # Calculate median, mode, t-test significance and save result to csv
    df_sharp = df_all.query(f'folder=="{sharp_data}"')["blur_val"]
    with open(csv_result, 'a', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        data = df_all.query(f'folder=="clear"')["blur_val"].tolist()
        q3, q1 = np.percentile(data, [75, 25])
        writer.writerow([sharp_data, df_sharp.median(), df_sharp.mean(), df_sharp.std(), "Nil", "Nil", "Nil", q3, q1])

    for each_type in folder_type_list:
        if each_type != sharp_data:
            df_selection = df_all.query(f'folder=="{each_type}"')["blur_val"]
            q3, q1 = np.percentile(df_selection.tolist(), [75, 25])
            p_val = ttest_ind(df_sharp, df_selection)[-1]

            if p_val < 0.01:
                p01 = True
            else:
                p01 = False

            if p_val < 0.05:
                p05 = True
            else:
                p05 = False

            with open(csv_result, 'a', newline='') as csv_file:
                writer = csv.writer(csv_file, delimiter=',')
                writer.writerow([each_type, df_selection.median(),
                                 df_selection.mean(),
                                 df_selection.std(), p_val, p05, p01, q3, q1])


def get_stat(input_folder, output_folder, sharp_data):
    # Get all folders and csv
    folder_list = [f'{input_folder}/{i}' for i in os.listdir(input_folder)]
    csv_list = []
    for each_folder in folder_list:
        for each_files in os.listdir(each_folder):
            if each_files.endswith('.csv'):
                csv_list.append(f'{each_folder}/{each_files}')

    # Concatenate csv results
    li = []
    folder_type_list = []
    for each_csv in csv_list:
        if 'Non_categorical' not in each_csv:
            df_tempt = pd.read_csv(each_csv)
            # Find current folder
            current_folder = each_csv.split('/')[-1].split('_')[2]
            df_tempt['folder'] = current_folder
            li.append(df_tempt)
            folder_type_list.append(current_folder)

    df_all = pd.concat(li, axis=0, ignore_index=True)
    current_fft = df_all.iat[1, 1]

    # Draw boxplot
    sns.set(style="whitegrid")
    sns.boxplot(x="folder", y="blur_val", data=df_all, palette="PRGn",
                showmeans=True,
                meanprops={"marker": "o",
                           "markerfacecolor": "white",
                           "markeredgecolor": "black",
                           "markersize": "5"})
    sns.swarmplot(x="folder", y="blur_val", data=df_all, color=".25", alpha=.5)
    plot_savepath = f'{output_folder}/fft_{current_fft}_boxplot.png'
    plt.savefig(plot_savepath)
    plt.clf()

    # Calculate q1 and q3
    data = df_all["blur_val"].tolist()
    q3, q1 = np.percentile(data, [75, 25])

    csv_result = f'{output_folder}/fft_{current_fft}_report.csv'
    with open(csv_result, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["folder_type", "median", "mean", "Std_dev", "Student_t_test", "p < 0.05", "p < 0.01",
                         "Q3 quartile", "Q1 quartile"])

    # Calculate median, mode, t-test significance and save result to csv
    df_sharp = df_all.query(f'folder=="{sharp_data}"')["blur_val"]
    with open(csv_result, 'a', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        # Calculate q1 and q3
        data = df_all.query(f'folder=="clear"')["blur_val"].tolist()
        q3, q1 = np.percentile(data, [75, 25])
        writer.writerow([sharp_data, df_sharp.median(), df_sharp.mean(), df_sharp.std(), "Nil", "Nil", "Nil", q3, q1])

    p01_total = 0
    p05_total = 0
    for each_type in folder_type_list:
        if each_type != sharp_data:
            df_selection = df_all.query(f'folder=="{each_type}"')["blur_val"]
            q3, q1 = np.percentile(df_selection.tolist(), [75, 25])
            p_val = ttest_ind(df_sharp, df_selection)[-1]

            if p_val < 0.01:
                p01 = True
                p01_total += 1
            else:
                p01 = False

            if p_val < 0.05:
                p05 = True
                p05_total += 1
            else:
                p05 = False

            with open(csv_result, 'a', newline='') as csv_file:
                writer = csv.writer(csv_file, delimiter=',')
                writer.writerow([each_type, df_selection.median(),
                                 df_selection.mean(),
                                 df_selection.std(), p_val, p05, p01, q3, q1])

    return [int(current_fft), p05_total, p01_total]


def fftstat(fft_folder, output_folder, sharp_data):
    print("Processing for FFT")
    # Ensure correct format
    fft_folder = fft_folder.replace('\\', '/')
    output_folder = output_folder.replace('\\', '/') + '/fft'
    os.makedirs(output_folder, exist_ok=True)

    # Iterate for each FFT folder
    ratio_overall_list = []
    for each_fft in tqdm(os.listdir(fft_folder)):
        input_fft = f'{fft_folder}/{each_fft}'
        ratio_result = get_stat(input_fft, output_folder, sharp_data)
        ratio_overall_list.append(ratio_result)

    df_ratio_result = pd.DataFrame(ratio_overall_list, columns=['ratio_val', 'Total p < 0.05', 'Total p < 0.01'])
    df_ratio_result.sort_values(by='ratio_val')
    outpath = f'{output_folder}/ratio_significant_results.csv'
    df_ratio_result.to_csv(outpath, index=False)


def main():
    laplacianstat(IN_LAP_DIR, OUT_DIR, SHARP_FOLDER)
    fftstat(IN_FFT_DIR, OUT_DIR, SHARP_FOLDER)


if __name__ == '__main__':
    main()
