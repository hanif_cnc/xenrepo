import pandas as pd
import os
from matplotlib import pyplot as plt
import seaborn as sns
from scipy.stats import ttest_ind
import csv
from tqdm import tqdm

CSV_LIST = ['F:/Xendity/Project_QC_Blur/Scorecard_data/Inhouse',
            'F:/Xendity/Project_QC_Blur/Scorecard_data/CTOS']
OUTPUT_DIR = 'results_scorecard'


def scorecard_plot(dir_list, outdir):
    outdir = outdir.replace('\\', '/')

    for each_dir in dir_list:

        # read csv and prep out directory
        each_dir = each_dir.replace('\\', '/')
        cat_type = each_dir.split('/')[-1]
        outdir_cat = outdir + f'/{cat_type}'
        os.makedirs(outdir_cat, exist_ok=True)

        print("Reading csv...")
        df_positive = pd.read_csv(each_dir + '/positive_result.csv')
        df_positive['type'] = 'positive'
        if 'Onboarding ID' in df_positive:
            df_positive.rename(columns={'Onboarding ID': 'Filename'}, inplace=True)
        df_negative = pd.read_csv(each_dir + '/negative_result.csv')

        df_negative['type'] = 'negative'
        if 'Onboarding ID' in df_negative:
            df_negative.rename(columns={'Onboarding ID': 'Filename'}, inplace=True)

        all_csv = [df_positive, df_negative]
        print("CSV read")

        df = pd.concat(all_csv, axis=0, ignore_index=True)

        for col in df.columns:
            print(col)

        # Draw boxplot for laplacian
        print("Plotting laplacian...")
        sns.set(style="whitegrid")
        sns.boxplot(x="type", y="QC_Laplace", data=df, palette="PRGn",
                    showmeans=True,
                    meanprops={"marker": "o",
                               "markerfacecolor": "white",
                               "markeredgecolor": "black",
                               "markersize": "5"})
        plot_savepath = f'{outdir_cat}/boxplot_laplacian_{cat_type}.png'
        plt.savefig(plot_savepath, dpi=300)
        plt.clf()
        print("Laplacian plotted")

        # Draw boxplot for FFT
        print("Plotting FFT...")
        sns.set(style="whitegrid")
        sns.boxplot(x="type", y="QC_FFT", data=df, palette="PRGn",
                    showmeans=True,
                    meanprops={"marker": "o",
                               "markerfacecolor": "white",
                               "markeredgecolor": "black",
                               "markersize": "5"})
        plot_savepath = f'{outdir_cat}/boxplot_FFT_{cat_type}.png'
        plt.savefig(plot_savepath, dpi=300)
        plt.clf()
        print("FFT plotted")

        # Normalize values
        # Laplacian
        df['QC_Laplace_norm'] = (df['QC_Laplace'] - df['QC_Laplace'].min()) / (
                df['QC_Laplace'].max() - df['QC_Laplace'].min())
        # FFT
        df['QC_FFT_norm'] = (df['QC_FFT'] - df['QC_FFT'].min()) / (
                df['QC_FFT'].max() - df['QC_FFT'].min())

        sns.catplot('category', 'blur value normalize', data=df, hue='Blur technique', alpha=.5)
        plot_savepath = f'{output_dir}/laplacian_fftratio2_by_category.png'
        plt.savefig(plot_savepath, dpi=300)
        plt.clf()

        print("Finished")


def main():
    scorecard_plot(CSV_LIST, OUTPUT_DIR)


if __name__ == '__main__':
    main()
