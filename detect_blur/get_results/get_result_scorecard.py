import os
import csv
import json
import base64
import requests
import pandas as pd
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor

URL_LAPLACIAN = 'http://localhost:8001/blur_laplacian_v1'
URL_FFT = 'http://localhost:8001/blur_fft_v1'
# CSV_LIST = ['F:/Xendity/Project_QC_Blur/Scorecard_data/CTOS/positive.csv',
#             'F:/Xendity/Project_QC_Blur/Scorecard_data/CTOS/negative.csv',
#             'F:/Xendity/Project_QC_Blur/Scorecard_data/Inhouse/positive.csv',
#             'F:/Xendity/Project_QC_Blur/Scorecard_data/Inhouse/negative.csv']
# CSV_LIST = ['F:/Xendity/Project_QC_Blur/Scorecard_data/CTOS/negative.csv',
#             'F:/Xendity/Project_QC_Blur/Scorecard_data/Inhouse/positive.csv',
#             'F:/Xendity/Project_QC_Blur/Scorecard_data/Inhouse/negative.csv']
CSV_LIST = ['F:/Xendity/Project_QC_Blur/Scorecard_data/CTOS/positive.csv']
FFT_RATIO = 2


def request_api_laplacian(img_path):
    # Get image id
    img_basename = img_path.split('/')[-1]

    files = {'image1': open(img_path, 'rb'), 'output_image': (None, 'False')}

    response = requests.post(URL_LAPLACIAN, files=files, timeout=5000)

    if response.status_code == 200:
        body = json.loads(response.text)

        blur_value = body['blur_score']

        return blur_value
    else:
        return []


def request_api_fft(proc_list):
    img_path, img_ratio_setting = proc_list
    # Get image id
    img_basename = img_path.split('/')[-1]

    files = {'image1': open(img_path, 'rb'), 'img_ratio': (None, f'{img_ratio_setting}'),
             'output_image': (None, 'False')}

    response = requests.post(URL_FFT, files=files, timeout=5000)

    if response.status_code == 200:
        body = json.loads(response.text)

        blur_value = body['blur_score']

        return blur_value

    else:
        return []


def scorecard_result(csv_list: list, img_ratio_fft: int):
    for idx_csv, each_csv in enumerate(csv_list, start=1):
        print(f"Processing CSV {idx_csv} out of {len(csv_list)}")
        print(f"Current CSV: {each_csv}")

        each_csv = each_csv.replace('\\', '/')
        img_dir = each_csv.replace('.csv', '')

        # Read csv
        df = pd.read_csv(each_csv, dtype=object)
        first_column = df.iloc[:, 0].tolist()
        first_column = [i.replace(' ', '') for i in first_column]

        # Ensure image can be retrived
        img_list = []
        for each_img in first_column:
            img_path = f'{img_dir}/{each_img}'

            if not os.path.exists(img_path):
                path_test = [f'{img_path}.png',
                             f'{img_path}.jpg',
                             f'{img_path}.jpeg']
                try:
                    valid_path = [i for i in path_test if os.path.exists(i)][0]
                except IndexError:
                    print(f'Cannot find {img_path}')
                    valid_path = 0
                img_list.append(valid_path)
            else:
                img_list.append(img_path)

        with ThreadPoolExecutor(max_workers=4) as executor:
            print("current test: laplacian (1 out of 2)")
            lap_result = list(tqdm(executor.map(request_api_laplacian, img_list), total=len(img_list)))

            img_proc_list = [[i, img_ratio_fft] for i in img_list]
            print(f"current test: fft with image ratio set to {img_ratio_fft} (2 out of 2)")
            fft_result = list(tqdm(executor.map(request_api_fft, img_proc_list), total=len(img_proc_list)))

        df['QC_Laplace'] = lap_result
        df['QC_FFT'] = fft_result

        output_csv = each_csv.replace('.csv', '_result.csv')
        df.to_csv(output_csv, index=False)


def main():
    scorecard_result(CSV_LIST, FFT_RATIO)


if __name__ == '__main__':
    main()

