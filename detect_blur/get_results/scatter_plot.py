import cv2
import csv
import pandas as pd
from matplotlib import pyplot as plt
import os
import seaborn as sns


IN_LAP_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/laplacian_resized'
IN_FFT_DIR = 'F:/Xendity/Project_QC_Blur/Blur_Dataset_v1_output/FFT/img_ratio_2'
OUT_DIR = 'results/scatter_laplacian_fftratio2'


def obtain_dataframe(input_dir):
    # Get all folders and csv
    folder_list = [f'{input_dir}/{i}' for i in os.listdir(input_dir)]
    csv_list = []
    for each_folder in folder_list:
        for each_files in os.listdir(each_folder):
            if each_files.endswith('.csv'):
                csv_list.append(f'{each_folder}/{each_files}')

    # Concatenate csv results
    li = []
    folder_type_list = []
    for each_csv in csv_list:
        if 'Non_categorical' not in each_csv:
            df_tempt = pd.read_csv(each_csv)
            # Find current folder
            current_folder = each_csv.split('/')[-1].split('_')[-2]
            df_tempt['folder'] = current_folder
            li.append(df_tempt)
            folder_type_list.append(current_folder)

    df_all = pd.concat(li, axis=0, ignore_index=True)

    return df_all


def draw_scatter_plot(lap_dir, fft_dir, output_dir):
    lap_dir = lap_dir.replace('\\', '/')
    fft_dir = fft_dir.replace('\\', '/')
    output_dir = output_dir.replace('\\', '/')
    os.makedirs(output_dir, exist_ok=True)

    # Read CSV and create dataframe
    df_fft = obtain_dataframe(fft_dir)
    del df_fft['img_ratio']
    df_fft['blur_type'] = 'FFT'
    df_lap = obtain_dataframe(lap_dir)
    df_lap['blur_type'] = 'LAP'

    # Normalize values
    # Laplacian
    df_lap['blur_val_norm'] = (df_lap['blur_val'] - df_lap['blur_val'].min()) / (
                df_lap['blur_val'].max() - df_lap['blur_val'].min())
    # FFT
    df_fft['blur_val_norm'] = (df_fft['blur_val'] - df_fft['blur_val'].min()) / (
                df_fft['blur_val'].max() - df_fft['blur_val'].min())

    df_list = [df_fft, df_lap]
    df = pd.concat(df_list)

    # Plot by category
    df = df.rename(columns={'folder': 'category', 'blur_val_norm': 'blur value normalize',
                            'blur_type': 'Blur technique'})

    sns.catplot('category', 'blur value normalize', data=df, hue='Blur technique', alpha=.5)
    plot_savepath = f'{output_dir}/laplacian_fftratio2_by_category.png'
    plt.savefig(plot_savepath, dpi=300)
    plt.clf()

    # Plot by axes
    df_value_lap = df.loc[df['Blur technique'] == 'LAP'].copy()
    df_value_lap = df_value_lap.rename(columns={'blur value normalize': 'blur_laplacian'})
    df_value_fft = df.loc[df['Blur technique'] == 'FFT'].copy()
    df_value_fft = df_value_fft.rename(columns={'blur value normalize': 'blur_fft'})
    new_df = pd.merge(df_value_lap, df_value_fft,  how='left',
                      left_on=['image_id', 'category'],
                      right_on=['image_id', 'category'])

    sns.scatterplot(data=new_df, x="blur_laplacian", y="blur_fft", hue='category', alpha=.5)
    plot_savepath = f'{output_dir}/laplacian_fftratio2_by_axes_all.png'
    plt.savefig(plot_savepath, dpi=300)
    plt.clf()

    selected_data = ['clear', 'MCblur']
    filtered_df = new_df[new_df['category'].isin(selected_data)]
    sns.scatterplot(data=filtered_df, x="blur_laplacian", y="blur_fft", hue='category', alpha=.5)
    plot_savepath = f'{output_dir}/laplacian_fftratio2_by_axes_clear&MC.png'
    plt.savefig(plot_savepath, dpi=300)
    plt.clf()




def main():
    draw_scatter_plot(IN_LAP_DIR, IN_FFT_DIR, OUT_DIR)


if __name__ == '__main__':
    main()
