"""
Separate the the JSON by class. Prevent copy json if 'no_holo' class.
"""

import os
import json
import shutil
from tqdm import tqdm
from collections import Counter

IMG_JSON_DIR = 'F:/Xendity/LabelMe/hologram_dataset'
OUTPUT_DIR = 'F:/Xendity/LabelMe/hologram_dataset_split'
PRIORITY_CLASS = ['unsure', 'no_holo', 'hologram']  # Leftmost have highest priority


def sep_by_class(file_dir_input, file_dir_output, priority_list, report_file='dataset_report.txt'):
    print("Separating files...")
    os.makedirs(file_dir_output, exist_ok=True)
    # Process all JSON and its related images
    label_list = []
    for roots, folders, files in os.walk(file_dir_input):
        for file in tqdm(files):
            if file.endswith('.json'):
                json_path = roots + f'/{file}'
                json_path = json_path.replace('\\', '/')

                with open(json_path, 'r') as f:
                    loaded_json = json.load(f)
                try:
                    shape_json = loaded_json['shapes']
                except IndexError:
                    print(f'Index error {file}')
                    continue

                each_json_label = []
                for each_item in shape_json:
                    for key, val in each_item.items():
                        if key == 'label':
                            each_json_label.append(val)

                # Get unique label
                # Prevent json copy if category is no_holo
                each_json_label = list(set(each_json_label))
                selected_class = 'no_label'
                copy_json = True
                for each_class in priority_list:
                    matching_class = [i for i in each_json_label if i == each_class]
                    if len(matching_class) > 0:
                        selected_class = each_class
                        if selected_class == 'no_holo':
                            copy_json = False
                        break

                output_folder_path = f'{file_dir_output}/{selected_class}'
                output_folder_path = output_folder_path.replace('\\', '/')

                os.makedirs(output_folder_path, exist_ok=True)

                json_output_path = f'{output_folder_path}/{file}'

                # image path
                img_basename = file.replace('.json', '.png')
                img_input_path = f'{roots}/{img_basename}'
                img_input_path = img_input_path.replace('\\', '/')

                if not os.path.exists(img_input_path):
                    img_basename = file.replace('.json', '.jpg')
                    img_input_path = f'{roots}/{img_basename}'
                    img_input_path = img_input_path.replace('\\', '/')

                img_output_path = f'{output_folder_path}/{img_basename}'

                if copy_json:
                    shutil.copy(json_path, json_output_path)
                shutil.copy(img_input_path, img_output_path)
                label_list.append(selected_class)

    with open(report_file, 'w') as f:
        f.write('------------------------\n')
        f.write('Labelling Dataset Report\n')
        f.write(f'Total json: {len(label_list):,}\n')
        f.write("\n")
        for key, val in zip(Counter(label_list).keys(), Counter(label_list).values()):
            f.write(f'Total {key} class: {val:,}\n')
        f.write('------------------------\n')

    print('------------------------\n')
    print('Labelling Dataset Report\n')
    print(f'Total json: {len(label_list):,}\n')
    print("\n")
    for key, val in zip(Counter(label_list).keys(), Counter(label_list).values()):
        print(f'Total "{key}" class: {val:,}\n')
    print('------------------------\n')


def main():
    sep_by_class(IMG_JSON_DIR, OUTPUT_DIR, PRIORITY_CLASS)


if __name__ == '__main__':
    main()
