import os
import cv2
from tqdm import tqdm
from concurrent import futures


def get_highest_file_index(dir_list: list):
    dir_list = [i.replace('\\', '/') for i in dir_list]

    # Get all files
    all_files = []
    for each_dir in dir_list:
        file_list = [i.split('.')[0] for i in os.listdir(each_dir)]
        all_files.extend(file_list)

    all_files = list(set(all_files))
    all_files = [int(i) for i in all_files]

    return max(all_files)


def convert_jpg_to_png(input_dir: str):
    input_dir = input_dir.replace('\\', '/')

    jpg_list = [f'{input_dir}/{i}' for i in os.listdir(input_dir) if i.endswith('.jpg')]

    def proc_convert(img_path):
        img = cv2.imread(img_path)
        file_name = img_path.split('/')[-1]
        basename = file_name.split('.')[0]
        output_path = f'{input_dir}/{basename}.png'
        cv2.imwrite(output_path, img)

        os.remove(img_path)

    with futures.ThreadPoolExecutor() as executor:
        print('Converting to png...')
        list(tqdm(executor.map(proc_convert, jpg_list), total=len(jpg_list)))

    print('Finished')


def augment_img_mask(img_dir, mask_dir, img_out, mask_out, current_basename: int, zero_fill: int):
    img_dir = img_dir.replace('\\', '/')
    mask_dir = mask_dir.replace('\\', '/')
    img_out = img_out.replace('\\', '/')
    mask_out = mask_out.replace('\\', '/')

    os.makedirs(img_out, exist_ok=True)
    os.makedirs(mask_out, exist_ok=True)

    # Get all filename in image dir
    file_list = [i.split('.')[0] for i in os.listdir(img_dir)]

    print('Augmenting dataset...')
    for basename in tqdm(file_list):
        img_path = f'{img_dir}/{basename}.png'
        mask_path = f'{mask_dir}/{basename}.png'

        img_read = cv2.imread(img_path)
        mask_read = cv2.imread(mask_path)

        # Horizontal flip
        aug_img1 = cv2.flip(img_read, 0)
        aug_mask1 = cv2.flip(mask_read, 0)

        # Vertical flip
        aug_img2 = cv2.flip(img_read, 1)
        aug_mask2 = cv2.flip(mask_read, 1)

        # Horizontal and vertical flip
        aug_img3 = cv2.flip(img_read, -1)
        aug_mask3 = cv2.flip(mask_read, -1)

        # Prep naming
        basename1 = f'{current_basename + 1}'.zfill(zero_fill) + '.png'
        basename2 = f'{current_basename + 2}'.zfill(zero_fill) + '.png'
        basename3 = f'{current_basename + 3}'.zfill(zero_fill) + '.png'

        # Prep outpaths
        img_path_out1 = f'{img_out}/{basename1}'
        img_path_out2 = f'{img_out}/{basename2}'
        img_path_out3 = f'{img_out}/{basename3}'
        mask_path_out1 = f'{mask_out}/{basename1}'
        mask_path_out2 = f'{mask_out}/{basename2}'
        mask_path_out3 = f'{mask_out}/{basename3}'

        # Save images
        cv2.imwrite(img_path_out1, aug_img1)
        cv2.imwrite(img_path_out2, aug_img2)
        cv2.imwrite(img_path_out3, aug_img3)
        cv2.imwrite(mask_path_out1, aug_mask1)
        cv2.imwrite(mask_path_out2, aug_mask2)
        cv2.imwrite(mask_path_out3, aug_mask3)

        # update current basename
        current_basename += 3

    print('Augmentation finished.')
