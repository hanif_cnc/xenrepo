"""
Functions to prepare dataset for segmentation neural network

Function summary list
find_prob_json: Find JSON that cannot be loaded using conventional method
mask_json_format1: To create mask where JSON cannot be loaded using conventional means
mask_json_format2: Creates masks for JSON loaded using conventional means.
mask_no_json: creates empty mask for unlabelled images intended to be a 'background'
copy_files: Uses multithread to copy files based on file extension
move_files: Uses multithread to move files based on file extension
"""
import re
import os
import cv2
import json
import shutil
import numpy as np
from tqdm import tqdm
from concurrent import futures


def polygons_to_mask_array(polygons: list, width: int, height: int):
    """
    Returns mask 3 channel based on polygon data.
    Object represented by polygon will be in full white (255,255,255)
    Background will be black (0,0,0)

    Example of polygon:
    [[x1, y1], [x2, y2] ...]
    Elements are in integer
    """
    mask = np.zeros([height, width, 3], dtype=np.uint8)
    for each_polygon in polygons:
        polygon_array = np.array(each_polygon, dtype=np.int32)
        polygon_array = polygon_array.reshape((-1, 1, 2))
        color = (255, 255, 255)
        mask = cv2.fillPoly(mask, [polygon_array], color)

    return mask


def find_prob_json(input_dir: str, output_dir: str):
    """
    Find JSON that cannot be loaded using conventional method
    and move it to output_dir
    """
    input_dir = input_dir.replace('\\', '/')
    output_dir = output_dir.replace('\\', '/')

    json_list = [each_json for each_json in os.listdir(input_dir) if each_json.endswith('.json')]
    print('Processing JSON...')
    for each_json in tqdm(json_list):

        input_path = f'{input_dir}/{each_json}'

        try:
            with open(input_path, 'r') as f:
                read_json = json.load(f)
        except json.decoder.JSONDecodeError:
            os.makedirs(output_dir, exist_ok=True)
            output_path = f'{output_dir}/{each_json}'
            shutil.move(input_path, output_path)


def mask_json_format1(input_dir: str, output_dir: str):
    """
    To create mask where JSON cannot be loaded using conventional means.
    This function reads the JSON as text and extract 'points' info from it
    to create the mask.
    :param input_dir: JSON directory
    :param output_dir: Mask directory
    """
    input_dir = input_dir.replace('\\', '/')
    output_dir = output_dir.replace('\\', '/')

    os.makedirs(output_dir, exist_ok=True)

    json_list = [each_json for each_json in os.listdir(input_dir) if each_json.endswith('.json')]
    print('Processing JSON...')
    for each_json in tqdm(json_list):
        input_path = f'{input_dir}/{each_json}'

        with open(input_path, 'r') as f:
            lines = f.readlines()

        # Remove unnessary lines
        lines = lines[1:]

        point_list = []
        group_id_list = []
        img_width = 0
        img_height = 0
        for idx, each_line in enumerate(lines):
            if "points" in each_line:
                point_list.append(idx)
            elif "group_id" in each_line:
                group_id_list.append(idx)
            elif "imageWidth" in each_line:
                each_line = re.sub("[^0-9]", "", each_line)
                img_width = int(each_line)
            elif "imageHeight" in each_line:
                each_line = re.sub("[^0-9]", "", each_line)
                img_height = int(each_line)

        all_hologram = []
        for point_id, group_id in zip(point_list, group_id_list):
            each_hologram = []
            selected_line = lines[point_id: group_id]
            for each_line in selected_line:
                each_line = re.sub("[^\d\.]", "", each_line)
                try:
                    extracted_float = float(each_line)
                    each_hologram.append(extracted_float)
                except ValueError:
                    pass

            idx_range = len(each_hologram)
            grouped_hologram = [[int(each_hologram[x]), int(each_hologram[x+1])] for x in range(0, idx_range, 2)]

            all_hologram.append(grouped_hologram)

        mask = polygons_to_mask_array(all_hologram, width=img_width, height=img_height)

        file_basename = each_json.split('.')[0]
        img_savepath = f'{output_dir}/{file_basename}.png'
        cv2.imwrite(img_savepath, mask)


def mask_json_format2(input_dir: str, output_dir: str):
    """
    This function creates masks based on JSON that can be loaded using conventional means.
    :param input_dir: JSON directory
    :param output_dir: Mask directory
    """
    input_dir = input_dir.replace('\\', '/')
    output_dir = output_dir.replace('\\', '/')

    os.makedirs(output_dir, exist_ok=True)

    json_list = [each_json for each_json in os.listdir(input_dir) if each_json.endswith('.json')]
    print('Processing JSON...')
    for each_json in tqdm(json_list):

        input_path = f'{input_dir}/{each_json}'

        with open(input_path, 'r') as f:
            loaded_json = json.load(f)

        img_width = 0
        img_height = 0
        for key, val in loaded_json.items():
            if key == "imageWidth":
                img_width = int(val)
            elif key == "imageHeight":
                img_height = int(val)

        try:
            shape_json = loaded_json['shapes']
        except IndexError:
            mask = np.zeros([img_height, img_width, 3], dtype=np.uint8)
            file_basename = each_json.split('.')[0]
            img_savepath = f'{output_dir}/{file_basename}.png'
            cv2.imwrite(img_savepath, mask)
            continue

        all_hologram = []
        for each_item in shape_json:
            for key, val in each_item.items():
                if key == 'points':
                    each_hologram = [[int(i[0]), int(i[1])] for i in val]
                    all_hologram.append(each_hologram)

        mask = polygons_to_mask_array(all_hologram, width=img_width, height=img_height)

        file_basename = each_json.split('.')[0]
        img_savepath = f'{output_dir}/{file_basename}.png'
        cv2.imwrite(img_savepath, mask)


def mask_no_json(input_dir: str, output_dir: str):
    """
    This function creates empty mask for unlabelled images intended
    to be a 'background' class only. It searches all images and jsons by
    os.walk (search all subfolders). Then, it compares and determines
    which images where there are no json designated for it. The images
    with no designated json will be considered as the unlabelled images.

    :param input_dir: Image and JSON directory
    :param output_dir: Mask directory
    """
    input_dir = input_dir.replace('\\', '/')
    output_dir = output_dir.replace('\\', '/')

    os.makedirs(output_dir, exist_ok=True)

    all_file = []
    all_json = []
    for roots, dirs, files in os.walk(input_dir):
        for file in files:
            basename = file.split('.')[0]
            all_file.append(basename)

            if file.endswith('json'):
                json_file = file.split('.')[0]
                all_json.append(json_file)

    all_file = list(set(all_file))
    all_json = list(set(all_json))

    no_json_list = [i for i in all_file if i not in all_json]

    print('Processing for no json images...')
    for file_basename in tqdm(no_json_list):
        img_path = f'{input_dir}/{file_basename}.png'

        if not os.path.exists(img_path):
            img_path = f'{input_dir}/{file_basename}.jpg'

        img = cv2.imread(img_path)

        mask = np.zeros(img.shape, dtype=np.uint8)
        mask_savepath = f'{output_dir}/{file_basename}.png'
        cv2.imwrite(mask_savepath, mask)


def copy_files(input_dir: str, output_dir: str, file_ext: tuple or str):
    """
    Uses multithread to copy files based on file_ext to output_dir.
    WARNING: This function uses os.walk to search all the files in
    the input_dir and copy it to output_dir without rebuilding the
    subdirectories. Thus, ensure the filenames are unique to
    avoid overwriting files.

    Example of file_ext:
    for multiple file --> ('.jpg','.png')
    for single file --> '.jpg'

    :param input_dir: Files directory
    :param output_dir: Output copied files
    :param file_ext: list containing file selection
    """
    input_dir = input_dir.replace('\\', '/')
    output_dir = output_dir.replace('\\', '/')

    os.makedirs(output_dir, exist_ok=True)

    proc_list = []

    for roots, dirs, files in os.walk(input_dir):
        for file in files:
            if file.endswith(file_ext):
                input_path = f'{roots}/{file}'
                input_path = input_path.replace('\\', '/')
                output_path = f'{output_dir}/{file}'
                output_path = output_path.replace('\\', '/')
                proc_list.append([input_path, output_path])

    proc_amount = len(proc_list)

    def proc_copy(each_proc):
        in_path = each_proc[0]
        out_path = each_proc[1]
        shutil.copy2(in_path, out_path)

    with futures.ThreadPoolExecutor() as executor:
        print('Copying with multithread...')
        list(tqdm(executor.map(proc_copy, proc_list), total=proc_amount))

    print('Copying finished')


def move_files(input_dir: str, output_dir: str, file_ext: tuple or str):
    """
    Uses multithread to move files based on file_ext to output_dir.
    WARNING: This function uses os.walk to search all the files in
    the input_dir and move it to output_dir without rebuilding the
    subdirectories. Thus, ensure the filenames are unique to
    avoid overwriting files.

    Example of file_ext:
    for multiple file --> ('.jpg','.png')
    for single file --> '.jpg'

    :param input_dir: Files directory
    :param output_dir: Output copied files
    :param file_ext: list containing file selection
    """
    input_dir = input_dir.replace('\\', '/')
    output_dir = output_dir.replace('\\', '/')

    os.makedirs(output_dir, exist_ok=True)

    proc_list = []

    for roots, dirs, files in os.walk(input_dir):
        for file in files:
            if file.endswith(file_ext):
                input_path = f'{roots}/{file}'
                input_path = input_path.replace('\\', '/')
                output_path = f'{output_dir}/{file}'
                output_path = output_path.replace('\\', '/')
                proc_list.append([input_path, output_path])

    proc_amount = len(proc_list)

    def proc_move(each_proc):
        in_path = each_proc[0]
        out_path = each_proc[1]
        shutil.move(in_path, out_path)

    with futures.ThreadPoolExecutor() as executor:
        print('Moving files with multithread...')
        list(tqdm(executor.map(proc_move, proc_list), total=proc_amount))

    print('Moving finished')
