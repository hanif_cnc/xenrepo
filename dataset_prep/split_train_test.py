"""
Split dataset to train and test. Identify which images were labelled and not labelled for a
fair split ratio. Can handle image in png and jpg. Ensure json in same dir as images
"""
import os
import shutil
import random
from tqdm import tqdm


SPLIT_RATIO = 0.8
INPUT_DIR = 'F:/Xendity/LabelMe/dataset/all'
TRAIN_DIR = 'F:/Xendity/LabelMe/dataset/pre1_train'
TEST_DIR = 'F:/Xendity/LabelMe/dataset/pre1_test'


def copy_images(indir: str, outdir: str, basename: str, labelled: bool):
    if labelled:
        in_img = indir + f'/{basename}.png'
        file_ext = '.png'
        if not os.path.exists(in_img):
            in_img = indir + f'/{basename}.jpg'
            file_ext = '.jpg'

        in_json = indir + f'/{basename}.json'

        out_img = outdir + f'/{basename}{file_ext}'
        out_json = outdir + f'/{basename}.json'

        shutil.copy2(in_img, out_img)
        shutil.copy2(in_json, out_json)

    else:
        in_img = indir + f'/{basename}.png'
        file_ext = '.png'
        if not os.path.exists(in_img):
            in_img = indir + f'/{basename}.jpg'
            file_ext = '.jpg'

        out_img = outdir + f'/{basename}{file_ext}'

        shutil.copy2(in_img, out_img)


def split_dataset(input_path: str, output_train: str, output_test: str, ratio: float):
    # Format paths
    input_path = input_path.replace('\\', '/')
    output_train = output_train.replace('\\', '/')
    output_test = output_test.replace('\\', '/')

    # Make directory
    os.makedirs(output_train, exist_ok=True)
    os.makedirs(output_test, exist_ok=True)

    # Gather all basename in dataset
    all_file_list = [each_file.split('.')[0] for each_file in os.listdir(input_path)]
    all_file_list = list(set(all_file_list))
    labelled_list = [each_file.split('.')[0] for each_file in os.listdir(input_path) if each_file.endswith('.json')]
    unlabelled_list = [each_file for each_file in all_file_list if each_file not in labelled_list]

    # shuffle list
    random.shuffle(labelled_list)
    random.shuffle(unlabelled_list)

    # split list based on ratio
    total_labelled = len(labelled_list)
    labelled_train_amount = int(total_labelled * ratio)
    labelled_list_train = labelled_list[:labelled_train_amount]
    labelled_list_test = labelled_list[labelled_train_amount:]

    total_unlabelled = len(unlabelled_list)
    unlabelled_train_amount = int(total_unlabelled * ratio)
    unlabelled_list_train = unlabelled_list[:unlabelled_train_amount]
    unlabelled_list_test = unlabelled_list[unlabelled_train_amount:]

    # Print statistics
    total_files = len(all_file_list)
    print(f'Total images: {total_files:,}')
    print(f'Total labelled: {total_labelled}')
    print(f'Total unlabelled: {total_unlabelled}')
    print(f'---Train---')
    total_labelled_list_train = len(labelled_list_train)
    total_unlabelled_list_train = len(unlabelled_list_train)
    print(f'labelled images: {total_labelled_list_train}')
    print(f'unlabelled images: {total_unlabelled_list_train}')
    print(f'---Test---')
    total_labelled_list_test = len(labelled_list_test)
    total_unlabelled_list_test = len(unlabelled_list_test)
    print(f'labelled images: {total_labelled_list_test}')
    print(f'unlabelled images: {total_unlabelled_list_test}')

    # Prepare report
    report_file = 'train_test_report.txt'
    with open(report_file, 'w') as f:
        f.write('------------------------\n')
        f.write('Train Test Split Dataset Report\n')
        f.write(f'Total images: {total_files:,}\n')
        f.write(f'Total labelled: {total_labelled:,}\n')
        f.write(f'Total unlabelled: {total_unlabelled:,}\n')
        f.write(f'---Train---\n')
        f.write(f'labelled images: {total_labelled_list_train:,}\n')
        f.write(f'unlabelled images: {total_unlabelled_list_train:,}\n')
        f.write(f'---Test---\n')
        f.write(f'labelled images: {total_labelled_list_test:,}\n')
        f.write(f'unlabelled images: {total_unlabelled_list_test:,}\n')

    # Copy images to test and train folders
    print('')
    print('Copying files for train...')
    # For train
    print('For labelled')
    for each_file in tqdm(labelled_list_train):
        copy_images(INPUT_DIR, output_train, each_file, True)
    print('For unlabelled')
    for each_file in tqdm(unlabelled_list_train):
        copy_images(INPUT_DIR, output_train, each_file, False)

    print('Copying files for test')
    # For test
    print('For labelled')
    for each_file in tqdm(labelled_list_test):
        copy_images(INPUT_DIR, output_test, each_file, True)
    print('For unlabelled')
    for each_file in tqdm(unlabelled_list_test):
        copy_images(INPUT_DIR, output_test, each_file, False)


if __name__ == '__main__':
    split_dataset(INPUT_DIR, TRAIN_DIR, TEST_DIR, SPLIT_RATIO)
