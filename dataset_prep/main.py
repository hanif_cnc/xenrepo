import preprocess as prep
import augment as aug

TEST_INPUT_PATH = 'F:/Xendity/LabelMe/dataset/pre1_test'
TEST_PROB_PATH = 'F:/Xendity/LabelMe/dataset/pre1_test/prob_json'
TEST_MASK_PATH = 'F:/Xendity/LabelMe/dataset/pre2_test_mask'
TEST_IMAGE_PATH = 'F:/Xendity/LabelMe/dataset/pre2_test_image'
TEST_AUG_IMAGE_PATH = 'F:/Xendity/LabelMe/dataset/test_aug_image'
TEST_AUG_MASK_PATH = 'F:/Xendity/LabelMe/dataset/test_aug_mask'
FINAL_TEST_IMG = 'F:/Xendity/LabelMe/dataset/test_img'
FINAL_TEST_MASK = 'F:/Xendity/LabelMe/dataset/test_mask'

TRAIN_INPUT_PATH = 'F:/Xendity/LabelMe/dataset/pre1_train'
TRAIN_PROB_PATH = 'F:/Xendity/LabelMe/dataset/pre1_train/prob_json'
TRAIN_MASK_PATH = 'F:/Xendity/LabelMe/dataset/pre2_train_mask'
TRAIN_IMAGE_PATH = 'F:/Xendity/LabelMe/dataset/pre2_train_image'
TRAIN_AUG_IMAGE_PATH = 'F:/Xendity/LabelMe/dataset/train_aug_image'
TRAIN_AUG_MASK_PATH = 'F:/Xendity/LabelMe/dataset/train_aug_mask'
FINAL_TRAIN_IMG = 'F:/Xendity/LabelMe/dataset/train_img'
FINAL_TRAIN_MASK = 'F:/Xendity/LabelMe/dataset/train_mask'

if __name__ == '__main__':

    prep.find_prob_json(TRAIN_INPUT_PATH, TRAIN_PROB_PATH)
    prep.mask_json_format1(TRAIN_PROB_PATH, TRAIN_MASK_PATH)
    prep.mask_json_format2(TRAIN_INPUT_PATH, TRAIN_MASK_PATH)
    prep.mask_no_json(TRAIN_INPUT_PATH, TRAIN_MASK_PATH)

    prep.find_prob_json(TEST_INPUT_PATH, TEST_PROB_PATH)
    prep.mask_json_format1(TEST_PROB_PATH, TEST_MASK_PATH)
    prep.mask_json_format2(TEST_INPUT_PATH, TEST_MASK_PATH)
    prep.mask_no_json(TEST_INPUT_PATH, TEST_MASK_PATH)

    prep.copy_files(TEST_INPUT_PATH, TEST_IMAGE_PATH, ('.png', '.jpg'))
    prep.copy_files(TRAIN_INPUT_PATH, TRAIN_IMAGE_PATH, ('.png', '.jpg'))

    # current_filename = aug.get_highest_file_index([TEST_IMAGE_PATH, TRAIN_IMAGE_PATH])
    # aug.convert_jpg_to_png(TRAIN_IMAGE_PATH)
    # aug.convert_jpg_to_png(TEST_IMAGE_PATH)
    #
    # print(f'{current_filename}'.zfill(7))
    # aug.augment_img_mask(img_dir=TRAIN_IMAGE_PATH, mask_dir=TRAIN_MASK_PATH,
    #                      img_out=TRAIN_AUG_IMAGE_PATH,
    #                      mask_out=TRAIN_AUG_MASK_PATH,
    #                      current_basename=current_filename, zero_fill=7)

    prep.move_files(TRAIN_IMAGE_PATH, FINAL_TRAIN_IMG, file_ext='.png')
    prep.move_files(TRAIN_AUG_IMAGE_PATH, FINAL_TRAIN_IMG, file_ext='.png')
    prep.move_files(TRAIN_MASK_PATH, FINAL_TRAIN_MASK, file_ext='.png')
    prep.move_files(TRAIN_AUG_MASK_PATH, FINAL_TRAIN_MASK, file_ext='.png')

    prep.move_files(TEST_IMAGE_PATH, FINAL_TEST_IMG, file_ext='.png')
    prep.move_files(TEST_AUG_IMAGE_PATH, FINAL_TEST_IMG, file_ext='.png')
    prep.move_files(TEST_MASK_PATH, FINAL_TEST_MASK, file_ext='.png')
    prep.move_files(TEST_AUG_MASK_PATH, FINAL_TEST_MASK, file_ext='.png')
