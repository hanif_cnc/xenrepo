"""
Copy images and json from different folders and rename them by 7 digits. Keep a record of the name of files.
Be careful not to use imagePath in the JSON files as the names had been changed.
Search for jpg, json and png files.
"""
import os
import shutil
from tqdm import tqdm

# LIST_OF_DIR = ['F:/Xendity workspace/LabelMe/to_label_physical2_lrgb/Done',
#                'F:/Xendity workspace/LabelMe/to_label_physical_lrgb/Done',
#                'F:/Xendity workspace/LabelMe/to_label_grayscale_lrgb/Done',
#                'F:/Xendity workspace/LabelMe/batch_4_lrgb/Done']
# OUTPUT_DIR = 'F:/Xendity workspace/LabelMe/dataset/all'

LIST_OF_DIR = ['F:/Xendity/LabelMe/to_label_physical2_lrgb/Done',
               'F:/Xendity/LabelMe/to_label_physical_lrgb/Done',
               'F:/Xendity/LabelMe/batch_4_lrgb/Done',
               'F:/Xendity/LabelMe/hologram_dataset_split/hologram',
               'F:/Xendity/LabelMe/hologram_dataset_split/no_holo']
OUTPUT_DIR = 'F:/Xendity/LabelMe/dataset/all'


def group_files(list_dir: list, outdir: str):
    all_img_count = 0
    all_json_count = 0
    report_list = []
    os.makedirs(outdir, exist_ok=True)

    for ind_folder, each_folder in enumerate(list_dir, start=1):
        folder_img_count = 0
        folder_json_count = 0
        files_copied = []

        # Reformat filepath if necessary
        each_folder = each_folder.replace('\\', '/')
        outdir = outdir.replace('\\', '/')

        print(f"Processing ({ind_folder}/{len(list_dir)}) for {each_folder}")

        for each_file in tqdm(os.listdir(each_folder)):
            if each_file.endswith(('.jpg', '.png')):
                # Add to counter
                all_img_count += 1
                folder_img_count += 1

                # Prepare path for image copy and output
                img_copy_path = each_folder + f'/{each_file}'
                file_ext = each_file.split('.')[-1]
                outfile_basename = f'{all_img_count}'.zfill(7)
                img_out_path = f'{outdir}/{outfile_basename}.{file_ext}'

                # Copy image to output folder
                shutil.copy2(img_copy_path, img_out_path)
                files_copied.append(f'{each_file} --> {outfile_basename}.{file_ext}')

                # Find json if exist
                json_basename = each_file.split('.')[0]
                json_copy_path = each_folder + f'/{json_basename}.json'

                if os.path.exists(json_copy_path):
                    # add json counter if found
                    all_json_count += 1
                    folder_json_count += 1
                    json_out_path = f'{outdir}/{outfile_basename}.json'
                    shutil.copy2(json_copy_path, json_out_path)
                    files_copied.append(f'{json_basename}.json --> {outfile_basename}.json')

        # After finishing a folder, keep record of statistics
        report_list.append([each_folder, folder_img_count, folder_json_count, files_copied])

    # Prepare report
    report_file = 'dataset_report.txt'
    with open(report_file, 'w') as f:
        f.write('------------------------\n')
        f.write('Dataset Report\n')
        f.write(f'Total directory: {len(list_dir)}\n')
        f.write('Directory list:\n')
        for each_folder in list_dir:
            f.write(f'{each_folder}\n')
        f.write('\n')
        f.write(f'Total images copied:{all_img_count:,}\n')
        f.write(f'Total json copied:{all_json_count:,}\n')
        f.write(f'Total unlabelled images:{all_img_count - all_json_count:,}\n')
        f.write('------------------------\n')
        f.write('\n')

        for each_report in report_list:
            f.write(f'-----{each_report[0]}-----\n')
            f.write(f'Images copied from directory: {each_report[1]:,}\n')
            f.write(f'Json copied from directory: {each_report[2]:,}\n')
            f.write(f'List of copied files:\n')
            for each_file in each_report[3]:
                f.write(f'{each_file}\n')
            f.write('------------------------\n')
            f.write('\n')


if __name__ == '__main__':
    group_files(LIST_OF_DIR, OUTPUT_DIR)
