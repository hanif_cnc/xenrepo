import os
from tqdm import tqdm
import json
import cv2
import numpy as np

INPUT_DIR = 'C:/Users/mdhan/Xendity/Dataset/66-dataset/train'
OUTPUT_DIR = 'C:/Users/mdhan/Xendity/Dataset/66-dataset/train_labelled'


def proc_shapes(shape_list, indir, outdir, img_base, json_basename, all_lable):
    cond_img = ""
    cond_json = ""

    # Iterate each shape item
    for each_item in shape_list:
        # Gather label and points info
        obj_label = each_item['label']
        x1, y1 = each_item['points'][0]
        x2, y2 = each_item['points'][1]

        # Recheck max and min values of points xy
        xmin = int(min([x1, x2]))
        xmax = int(max([x1, x2]))
        ymin = int(min([y1, y2]))
        ymax = int(max([y1, y2]))

        # Output dir
        output_folder = outdir + f'/{obj_label}'

        # Store info on lable amount
        if obj_label not in all_lable:
            # For new lable, to create new key and output box
            all_lable[obj_label] = {}
            all_lable[obj_label]['amt'] = 1
            all_lable[obj_label]['color'] = list(np.random.choice(range(256), size=3))
            os.makedirs(output_folder, exist_ok=True)
        else:
            # If key present, update amount of label
            all_lable[obj_label]['amt'] = all_lable[obj_label]['amt'] + 1

        # Draw and output box. Also check if image empty
        img_path = f'{indir}/{img_base}'
        img = cv2.imread(img_path)

        if img is None:
            print(f"WARNING: Empty image. Skipped {img_base}. Problematic json: {json_basename}")
            cond_img = img_base
            cond_json = json_basename
        else:
            rgb = all_lable[obj_label]['color']
            color = (int(rgb[0]), int(rgb[1]), int(rgb[2]))
            thickness = 3
            img = cv2.rectangle(img, (xmin, ymin), (xmax, ymax), color, thickness)
            current_label_amt = all_lable[obj_label]['amt']

            # prepare new basename
            out_base1, out_ext = img_base.split('.')
            out_basename = f'{out_base1}_{obj_label}_{current_label_amt}.{out_ext}'
            out_path = f'{output_folder}/{out_basename}'

            cv2.imwrite(out_path, img)

    return all_lable, cond_img, cond_json


def main(indir, outdir):
    # Ensure formatting of input and output dir
    indir = indir.replace('\\', '/')
    outdir = outdir.replace('\\', '/')

    # Get path of all json files
    json_list = [f'{INPUT_DIR}/{i}' for i in os.listdir(indir) if i.endswith('.json')]

    # Store types of labels and problematic images
    all_lable = {}
    prob_result = []

    # Iterate each json
    for each_json in tqdm(json_list):
        with open(each_json, 'r') as f:
            data = json.load(f)

            json_basename = each_json.split('/')[-1]

            # get json "shapes"
            data_shapes = data["shapes"]

            # If "shapes" not empty, process it
            if len(data_shapes) != 0:
                # Get image basename
                data_base = data["imagePath"].replace('\\', '/')
                img_base = data_base.split('/')[-1]

                all_lable, cond_img, cond_json = proc_shapes(data_shapes, indir, outdir,
                                                             img_base, json_basename, all_lable)

                if cond_img:
                    prob_result.append([cond_img, cond_json])

    # Printout faulty images
    if prob_result:
        json_list = [i[1] for i in prob_result]
        json_unique = list(set(json_list))
        prob_all = []
        for each_json in json_unique:
            img_unique = [i[0] for i in prob_result if i[1] == each_json][0]
            prob_all.append([img_unique, each_json])

        print("List of faulty images with related json:")
        for each_img, each_json in prob_all:
            print(f'Image: {each_img}, Json: {each_json}')


if __name__ == '__main__':
    main(INPUT_DIR, OUTPUT_DIR)
