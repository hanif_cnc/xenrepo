check_dataset.py:
Certain json filename and 'imagePath' did not correlate together. This code aims to find those mismatch jsons.

draw_class_dataset.py:
Draw each class/landmark from json to the images and split into subfolders according to class. 
Reads image basename from json 'imagePath'.

calc_dataset.py:
Get statistics for Front and Back Mykad dataset. All calculations are based on JSON files only.

create_pbtxt_csv.py:
from dataset, create labelling map in form of pbtxt or csv file

create_tf_record.py:
Create TFrecord from JSON files

extract_tfrecord.py:
Draw out the images in TFrecords together with the bounding boxes



