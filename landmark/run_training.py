import os
from tqdm import tqdm
from model_main_tf2 import main2

CONFIG_DIR_FRONT = '/data/hanif/ai_internal-master/pretrained_model/1.config/front_mykad'
CONFIG_DIR_BACK = '/data/hanif/ai_internal-master/pretrained_model/1.config/back_mykad'
MODEL_OUT = '/data/hanif/ai_internal-master/trained_model'


def main(config_front, config_back, outdir):
    # Check formatting
    config_front = config_front.replace('\\', '/')
    config_back = config_back.replace('\\', '/')
    outdir = outdir.replace('\\', '/')

    # Get config files
    config_all_front = [f"{config_front}/{i}" for i in os.listdir(config_front)]
    config_all_back = [f"{config_back}/{i}" for i in os.listdir(config_back)]

    # Process for front training
    print("START TRAINING FOR FRONT MYKAD")
    for idx, config_path in enumerate(config_all_front, start=1):
        model_base = config_path.split('/')[-1].split('.')[0]
        model_dir = f'{outdir}/front_mykad/{model_base}'
        os.makedirs(model_dir, exist_ok=True)
        print(f"----------------------------------------------------")
        print(f"Training model {idx} out of {len(config_all_front)}")
        print(f"Data type: Front Mykad")
        print(f"Model Name: {model_base}")
        print(f"----------------------------------------------------")
        main2(model_dir=model_dir, pipeline_config_path=config_path)

    # Process for back training
    print("START TRAINING FOR BACK MYKAD")
    for idx, config_path in enumerate(config_all_back, start=1):
        model_base = config_path.split('/')[-1].split('.')[0]
        model_dir = f'{outdir}/back_mykad/{model_base}'
        os.makedirs(model_dir, exist_ok=True)
        print(f"----------------------------------------------------")
        print(f"Training model {idx} out of {len(config_all_front)}")
        print(f"Data type: Back Mykad")
        print(f"Model Name: {model_base}")
        print(f"----------------------------------------------------")
        main2(model_dir=model_dir, pipeline_config_path=config_path)


if __name__ == '__main__':
    main(CONFIG_DIR_FRONT, CONFIG_DIR_BACK, MODEL_OUT)
