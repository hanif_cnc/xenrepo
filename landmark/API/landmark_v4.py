"""
1/3/2022
Landmark Verification v4
The latest neural network version will be the defacto choice of running inference
if no model version declared in modelVer request parameter

--Request--
1)imageFile: cropped Mykad image to be processed
2)docType: Indicate type of document
3)modelVer(optional): manually select model version for document if available
4)detThresh(optional): set new confidence threshold for all objects
5)outImg(optional): output image result. Can be string of
the following: TRUE, true, 1, yes, y (Capital letters are lowered). Wrong or
no input will by default not draw any output image.

--Return--
1) status: status of return
2) data: result of detections
3) prediction_img: image with bounding box detections. Only availabel if outImg is True
4) document_type_id: to indicate which document ID that was processed
5) model_version: to indicate the neural network model version
6) conf_threshold: to indicate the confidence interval that was set
7) output_image: To indicate if image output was requested
8) time_taken: Time taken to complete request process
"""
import io
import os
import re
import cv2
import json
import time
import grpc
import base64
import psutil
import falcon
import requests
import pandas as pd
import numpy as np
import settings as s
from PIL import Image
import tensorflow as tf
from tensorflow_serving.apis import predict_pb2
from concurrent.futures import ThreadPoolExecutor
from tensorflow_serving.apis import prediction_service_pb2_grpc
import configparser

# TODO
# Settings
LANDMARK_VERIFICATION_VER = '4.0'
IP_ADDRESS = 'localhost'  # For tensorflow serving ip
PORT_NO = '8601'  # For tensorflow serving port number
USE_GRCP = False  # If false, to use REST API method
MODEL_INPUT_NAME = 'input_1'  # For GRCP


class LandmarkVerify:
    def __init__(self, config_path='landmark_v4/landmark.ini'):
        # Load configurations
        self.config = configparser.ConfigParser()
        self.config_path = config_path
        self.config_dict = self.get_config()
        self.list_doc_type = [int(i) for i in self.config_dict]

    def on_post_single(self, req, resp):
        """
        For request and return json explanation, see the above documentation
        """
        # Initialization
        doc_type = 0
        doc_type_availability = True
        model_ver = 0
        model_ver_param = False  # User defined own model version
        model_availability = True
        selected_model_ver = []
        img_resize_width = 0
        img_resize_height = 0
        conf_threshold = .0
        conf_threshold_cond = True
        out_image = False

        on_post_time_start = time.time()
        print(f'Running single frame landmark verification v{LANDMARK_VERIFICATION_VER}')

        ############################################################################
        """
        Check and validate JSON Request
        """
        # Read image from request
        try:
            body = req.stream.read().decode('utf-8')
            img1 = req.get_param('imageFile')
            if body != '':
                img1 = self.upload_application_json(body)
            elif img1 is not None:
                img1 = img1.file.read()
            else:
                resp.status = falcon.HTTP_400
        except AttributeError:
            img1 = None
            print("ERROR: imageFile parameter is empty. Please upload image.")

        # Determine document type
        if 'docType' in req.params.keys():
            doc_type = int(req.get_param('docType'))
        else:
            print("ERROR: docType is empty. Please provide document type.")

        # Check if document type is available
        if doc_type:
            if doc_type not in self.list_doc_type:
                doc_type_availability = False
                print("ERROR: Document type ID is not available.")

        # Determine model version
        if doc_type and doc_type_availability:
            if 'modelVer' in req.params.keys():
                model_ver = int(req.get_param('modelVer'))
                model_ver_param = True
            else:
                selected_model_list = list(self.config_dict[doc_type]['models'])
                model_ver = max([i['model_ver'] for i in selected_model_list])
                img_resize_height, img_resize_width = self.get_resize(doc_type, model_ver)

        # Check if model version is available in config
        if doc_type_availability and model_ver_param:
            selected_model_list = list(self.config_dict[doc_type]['models'])
            selected_model_ver = [i['model_ver'] for i in selected_model_list]
            if model_ver not in selected_model_ver:
                model_availability = False
                print("ERROR: Model version for document is not available.")
            else:
                img_resize_height, img_resize_width = self.get_resize(doc_type, model_ver)

        # Determine confidence threshold
        if doc_type_availability and model_availability and doc_type:
            if 'detThresh' in req.params.keys():
                try:
                    conf_threshold = float(req.get_param('detThresh'))
                    print(f"Using confidence threshold: {conf_threshold}")
                except ValueError:
                    conf_threshold_cond = False
                    print("ERROR: detThresh value is not a valid float.")
            else:
                proc_dict = list(self.config_dict[doc_type]['models'])
                model_dict = [i for i in proc_dict if i['model_ver'] == model_ver][0]
                conf_threshold = float(model_dict['threshold'])

        # Determine if image should be outputted
        if 'outImg' in req.params.keys():
            output_request = req.get_param('outImg')
            out_image = output_request.lower() in ['true', '1', 't', 'y', 'yes']

        ############################################################################
        """
        Process JSON Request
        """
        # Process request
        if img1 is None:
            status = re.search('\\d+\\s(.*)', resp.status,
                               re.IGNORECASE).group(1)
            resp.body = json.dumps(
                {
                    "status": status,
                    "error": "imageFile parameter is empty. Please upload image."
                }
            )
        elif not doc_type:
            resp.status = falcon.HTTP_400
            status = re.search('\\d+\\s(.*)', resp.status,
                               re.IGNORECASE).group(1)
            resp.body = json.dumps(
                {
                    "status": status,
                    "error": "docType is empty. Please provide document type."
                }
            )
        elif not doc_type_availability:
            resp.status = falcon.HTTP_400
            status = re.search('\\d+\\s(.*)', resp.status,
                               re.IGNORECASE).group(1)
            resp.body = json.dumps(
                {
                    "status": status,
                    "error": "Document type ID is not available.",
                    "list_available_document_id": self.list_doc_type
                }
            )
        elif not model_availability:
            resp.status = falcon.HTTP_400
            status = re.search('\\d+\\s(.*)', resp.status,
                               re.IGNORECASE).group(1)
            resp.body = json.dumps(
                {
                    "status": status,
                    "error": "Model version for document is not available.",
                    "selected_document_id": doc_type,
                    "list_available_version": selected_model_ver
                }
            )
        elif not conf_threshold_cond:
            resp.status = falcon.HTTP_400
            status = re.search('\\d+\\s(.*)', resp.status,
                               re.IGNORECASE).group(1)
            resp.body = json.dumps(
                {
                    "status": status,
                    "error": "detThresh value is not a valid float.",
                }
            )
        else:
            print("Landmark verification request OK. Processing image")
            # Decode image received
            np_arr = np.fromstring(img1, np.uint8)
            img_read = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
            img_read = cv2.cvtColor(img_read, cv2.COLOR_RGB2BGR)

            # Perform prediction
            img_out, data_out = self.predict_landmark(img_read, (img_resize_height, img_resize_width),
                                                      doc_type, model_ver, conf_threshold, out_image)

            # Memory profiling
            # print("PID: {}".format(os.getpid()))
            # mem_usage = memory_usage(os.getpid(), interval=1, timeout=1)
            # with open("holo_segm_v2_2_tfserving_memory_profiler_{}.log".format(os.getpid()), "a+") as f:
            #     f.write(str(mem_usage[0])+",")
            # f.close()

            if out_image:
                # Convert mask image to base64
                img_out = self.np_to_jpeg(img_out)
                img_out = self.jpeg_to_base64(img_out)

                resp.body = json.dumps(
                    {
                        "status": 'OK',
                        "data": data_out,
                        "prediction_img": img_out.decode('utf-8'),
                        "document_type_id": doc_type,
                        "model_version": model_ver,
                        "conf_threshold": conf_threshold,
                        "output_image": out_image,
                        "general_version": s.get_str("VER_GENERAL"),
                        "api_version": s.get_str("VER_SEGM_HOLO_V2_2"),
                        "time_taken": f'{time.time() - on_post_time_start:.2f}'
                    }
                )
            else:
                resp.body = json.dumps(
                    {
                        "status": 'OK',
                        "data": data_out,
                        "document_type_id": doc_type,
                        "model_version": model_ver,
                        "conf_threshold": conf_threshold,
                        "output_image": out_image,
                        "general_version": s.get_str("VER_GENERAL"),
                        "api_version": s.get_str("VER_SEGM_HOLO_V2_2"),
                        "time_taken": f'{time.time() - on_post_time_start:.2f}'
                    }
                )

    def predict_landmark(self, img, img_resize_dim: tuple, doc_type, model_ver, threshold, output_img):
        """
        Process to predict landmarks in the image
        
        :param img: image array
        :param img_resize_dim: image resize dimension
        :param doc_type: ID document type
        :param model_ver: Neural network version model to use
        :param threshold: COnfidence threshold set to remove detections below threshold
        :param output_img: Status wheter output image with detection box needs to be prepared
        """

        # Obtain original size and resize dimension
        img_height, img_width, __ = img.shape
        img_resize_height, img_resize_width = img_resize_dim

        # Resize image to input to neural network
        img_tf_resized = tf.image.resize(img, (img_resize_height, img_resize_width))
        img_tf = tf.expand_dims(img_tf_resized, 0)

        # Prepare model name
        model_type = self.config_dict[doc_type]['document_type']
        model_naming = f'{model_type}/versions/{model_ver}'

        prediction = self.tf_serve_img(img_tf, model_naming)
        prediction = prediction['predictions'][0]

        # Filter results
        filtered_result_dict = {}
        for each_box, each_class, each_score in zip(prediction['detection_boxes'],
                                                    prediction['detection_classes'], prediction['detection_scores']):
            each_class = int(each_class)
            each_score_percent = each_score * 100
            # Skip any result with less than set confidence threshold
            if each_score_percent < threshold:
                continue

            # Keep bounding box that have the highest confidence for each unique class (one class one bounding box)
            # Also convert box float to box coordinates that corresponds to original image dimension
            if each_class not in filtered_result_dict:
                filtered_result_dict[each_class] = {'box': [int((each_box[0] * img_resize_height) *
                                                                img_height/img_resize_height),
                                                            int((each_box[1] * img_resize_width) *
                                                                img_width/img_resize_width),
                                                            int((each_box[2] * img_resize_height) *
                                                                img_height/img_resize_height),
                                                            int((each_box[3] * img_resize_width) *
                                                            img_width/img_resize_width)],
                                                    'score': each_score_percent}
            else:
                if filtered_result_dict[each_class]['score'] < each_score:
                    filtered_result_dict[each_class] = {'box': [int((each_box[0] * img_resize_height) *
                                                                img_height/img_resize_height),
                                                                int((each_box[1] * img_resize_width) *
                                                                img_width/img_resize_width),
                                                                int((each_box[2] * img_resize_height) *
                                                                img_height/img_resize_height),
                                                                int((each_box[3] * img_resize_width) *
                                                                img_width/img_resize_width)],
                                                        'score': each_score_percent}

        # Prepare class id
        class_id = self.config_dict[doc_type]['document_class']

        # Draw boxes to original image if requested
        if output_img:
            for each_key in filtered_result_dict.keys():
                box_coord = filtered_result_dict[each_key]['box']
                score_amount = filtered_result_dict[each_key]['score']
                class_name = class_id[each_key - 1]

                font = cv2.FONT_HERSHEY_SIMPLEX
                coord = (box_coord[1], box_coord[0] - 5)
                font_scale = 0.7
                color = (255, 0, 0)
                thickness = 2
                line_type = 1
                img_txt = f'{class_name}, {score_amount:.2f}'

                img = cv2.rectangle(img, (box_coord[1], box_coord[0]), (box_coord[3], box_coord[2]), color, 2)

                img = cv2.putText(img, img_txt, coord, font, font_scale, color, thickness, line_type)

            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        # Prepare data output in similar format to v3.3
        data_out = []
        for each_key in filtered_result_dict.keys():
            box_coord = filtered_result_dict[each_key]['box']
            score_amount = filtered_result_dict[each_key]['score']
            class_name = class_id[each_key - 1]

            data_out.append({"landmark": class_name,
                             "confidence": score_amount,
                             "xmin": box_coord[1],
                             "ymin": box_coord[0],
                             "xmax": box_coord[3],
                             "ymax": box_coord[2]})

        return img, data_out

    def get_resize(self, doc_id, model_ver):
        """
        Get image resize parameters
        :param doc_id: Document ID
        :param model_ver: Model version
        :return:
        resize_height, resize_width
        """
        proc_dict = list(self.config_dict[doc_id]['models'])
        resize_dim = [i for i in proc_dict if i['model_ver'] == model_ver][0]
        resize_height = int(resize_dim['heigth_resize'])
        resize_width = int(resize_dim['width_resize'])

        return resize_height, resize_width

    def get_config(self):
        """
        Read and prepare configuration settings
        :return:
        dict configuration settings
        """
        self.config.read(self.config_path)
        config_dict = {}
        for each_doc in self.config["DocumentType"]:
            # Obtain document type and ID
            doc_id = int(self.config["DocumentType"][each_doc])

            # Read csv containing class ID
            csv_path = self.config["DocumentPath"][each_doc]
            df_class = pd.read_csv(csv_path)
            class_list = df_class['classes'].tolist()

            # Store info to dict
            config_dict[doc_id] = {'document_type': each_doc, 'document_class': class_list, 'models': []}

            # Select models that corresponds to the document type
            selected_model = []
            for each_model in self.config:
                if each_model.split('_')[0].isdigit():
                    model_doc = int(each_model.split('_')[0])
                    if model_doc == doc_id:
                        selected_model.append(each_model)

            # Store model version and size criteria
            for each_model in selected_model:
                model_config = self.config[each_model]
                current_config = dict()
                current_config['model_ver'] = int(each_model.split('_')[-1])
                current_config['heigth_resize'] = model_config['img_resize_height']
                current_config['width_resize'] = model_config['img_resize_width']
                current_config['threshold'] = float(model_config['threshold'])

                config_dict[doc_id]['models'].append(current_config)

        return config_dict

    @staticmethod
    def upload_application_json(body):
        """
        Read request that is in json format

        :param body: json request that contains image in base64
        :return: decoded based64 image
        """
        body = body.replace('\n', '')
        body_as_json = json.loads(body)
        image_file_as_base64 = body_as_json['image1']

        image_file_as_base64 = image_file_as_base64.split('base64,')
        image = base64.b64decode(image_file_as_base64[1])

        return image

    @staticmethod
    def tf_serve_img(image, model_name):
        """
        Based on condition, to transfer image to tensorflow serving for prediction either by GRCP or REST API method
        """
        if USE_GRCP:
            # TF Serving gRPC request
            channel_str = f'{IP_ADDRESS}:{PORT_NO}'
            channel = grpc.insecure_channel(channel_str)
            stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)
            grpc_request = predict_pb2.PredictRequest()
            grpc_request.model_spec.name = model_name
            grpc_request.model_spec.signature_name = 'serving_default'
            grpc_request.inputs[MODEL_INPUT_NAME].CopyFrom(tf.make_tensor_proto(image, shape=image.shape))
            result = stub.Predict(grpc_request, 90.0)  # 90sec timeout
            outputs_tensor_proto = result.outputs["sigmoid"]
            shape = tf.TensorShape(outputs_tensor_proto.tensor_shape)
            prediction_result = tf.constant(outputs_tensor_proto.float_val, shape=shape)
        else:
            # TF serving REST request
            data = json.dumps({"instances": image.numpy().astype(np.uint8).tolist()})
            request_str = f'http://{IP_ADDRESS}:{PORT_NO}/v1/models/{model_name}:predict'
            resp = requests.post(request_str, data=data)
            prediction_result = resp.json()

        return prediction_result

    @staticmethod
    def np_to_jpeg(image):
        """
        :param image: Image in np array format
        :return: Image as JPEG.
        """
        return Image.fromarray(image[:, :, ::-1])

    @staticmethod
    def jpeg_to_base64(image):
        """
        :param image: Image in jpeg format.
        :return: Image in Base64 format.
        """
        buffer_ = io.BytesIO()
        image.save(buffer_, format="JPEG")
        return base64.b64encode(buffer_.getvalue())

if __name__ == '__main__':
    LandmarkVerify()