#!/usr/bin/env python
""" This script is used as a controller to manage all the available API """

import json
import falcon
from falcon_multipart.middleware import MultipartMiddleware
from datetime import datetime
import settings as s
# from classify_document_landmark_mp import ClassifyDocumentLandmarkMP
# from ocrICNumber import OcrICNumber
# import session
# import createSessionIdResources
# import ocrMultiFrameDocumentResources
# import getAverageResources
# import removeSessionResources
# import uploadImageResources
# import classifyMultiFrameHologram
# import classifyHologramData
# import ocr_single_frame_document_resources
# import classify
# import classify_document_version
# import classify_document_landmark
# import classify_single_frame_hologram
# import classify_card_color
# import classify_card_material_vgen
# import classify_card_material_vigen
# import classify_card_material_viigen
# import classify_passport_color
# import crop_landmark
# import detect_text_ocr
# import detect_serial_number
# import comb_img_txt_ldmk

# import landmark_regions_crop


API = APPLICATION = falcon.API(middleware=[MultipartMiddleware()])

# sessions = session.Session()

# if s.get_bool("CR_SESS_ID"):
#     api.add_route(
#         '/createSessionId',
#         createSessionIdResources.CreateSessionIdResources())

# if s.get_bool("ST_STAT_OCR"):
#     api.add_route(
#         '/startStaticOcr',
#         uploadAndClassifyResources.UploadAndClassifyResources(
#         'images', sessions))

# if s.get_bool("OCR_SNGL_FRM_DOC"):
#     API.add_route(
#         '/ocrSingleFrameDocument',
#         ocr_single_frame_document_resources.OcrSingleFrameDocumentResources())

# if s.get_bool("OCR_MLT_FRM_DOC"):
#     api.add_route(
#         '/ocrMultiFrameDocument',
#         ocrMultiFrameDocumentResources.OcrMultiFrameDocumentResources(
#             'videos/uploaded', card_detector, sessions))

# if s.get_bool("DET_DOC_MOD_001"):
#     from card_detector import CardDetector
#     from object_detector import ObjectDetector
#
#     OBJECT_DETECTOR_001 = ObjectDetector(s.get_str("OBJ_DET_001"))
#     CARD_DETECTOR_001 = CardDetector(OBJECT_DETECTOR_001, error_log=True,
#                                      debug_log=True, debug_sides=True,
#                                      debug_lines=True, debug_points=True,
#                                      debug_detected_card=True,
#                                      debug_corrected_card=True,
#                                      debug_detect_lines_bilateral=True,
#                                      debug_detect_lines_otsu=True)
#     CARD_DETECTOR_001.load_card_set(s.get_str("CARD_SETT"))
#     API.add_route(
#         '/detectDocument_model_001',
#         detect_document_resources.DetectDocumentResources(CARD_DETECTOR_001))
#
# if s.get_bool("DET_DOC_MOD_002"):
#     from card_detector import CardDetector
#     from object_detector import ObjectDetector
#
#     OBJECT_DETECTOR_002 = ObjectDetector(s.get_str("OBJ_DET_002"))
#     CARD_DETECTOR_002 = CardDetector(OBJECT_DETECTOR_002, error_log=True,
#                                      debug_log=True, debug_sides=True,
#                                      debug_lines=True, debug_points=True,
#                                      debug_detected_card=True,
#                                      debug_corrected_card=True,
#                                      debug_detect_lines_bilateral=True,
#                                      debug_detect_lines_otsu=True)
#     CARD_DETECTOR_002.load_card_set(s.get_str("CARD_SETT"))
#     API.add_route(
#         '/detectDocument_model_002',
#         detect_document_resources.DetectDocumentResources(CARD_DETECTOR_002))
#
# if s.get_bool("DET_DOC_MOD_003"):
#     from card_detector import CardDetector
#     from object_detector import ObjectDetector
#
#     OBJECT_DETECTOR_003 = ObjectDetector(s.get_str("OBJ_DET_003"))
#     CARD_DETECTOR_003 = CardDetector(OBJECT_DETECTOR_003, error_log=True,
#                                      debug_log=True, debug_sides=True,
#                                      debug_lines=True, debug_points=True,
#                                      debug_detected_card=True,
#                                      debug_corrected_card=True,
#                                      debug_detect_lines_bilateral=True,
#                                      debug_detect_lines_otsu=True)
#     CARD_DETECTOR_003.load_card_set(s.get_str("CARD_SETT"))
#     API.add_route(
#         '/detectDocument_model_003',
#         detect_document_resources.DetectDocumentResources(CARD_DETECTOR_003))
#
# if s.get_bool("DET_DOC_MOD_004"):
#     from card_detector import CardDetector
#     from object_detector import ObjectDetector
#
#     OBJECT_DETECTOR_004 = ObjectDetector(s.get_str("OBJ_DET_004"),
#                                          s.get_int("MY_OD_IN_LYR"))
#     CARD_DETECTOR_004 = CardDetector(OBJECT_DETECTOR_004, error_log=True,
#                                      debug_log=True, debug_sides=True,
#                                      debug_lines=True, debug_points=True,
#                                      debug_detected_card=True,
#                                      debug_corrected_card=True,
#                                      debug_detect_lines_bilateral=True,
#                                      debug_detect_lines_otsu=True)
#     CARD_DETECTOR_004.load_card_set(s.get_str("CARD_SETT"))
#     API.add_route(
#         '/detectDocument_model_004',
#         detect_document_resources.DetectDocumentResources(CARD_DETECTOR_004))

# if s.get_bool("GET_AVG"):
#     api.add_route(
#         '/getAverage',
#         getAverageResources.GetAverageResources(sessions))

# if s.get_bool("RMV_SESS"):
#     api.add_route(
#         '/removeSession',
#         removeSessionResources.RemoveSessionResources(sessions))


# if s.get_bool("UL_IMG"):
#     api.add_route('/uploadImage',
#                   uploadImageResources.UploadImageResources('images'))


# if s.get_bool("CL_DOC_VER"):
#     VERSION_MODEL = classify.Classify(s.get_str("VER_MDL"),
#                                       input_layer_size=s.get_int("VER_IN_LYR"))
#     API.add_route('/classifyDocumentVersion',
#                   classify_document_version.ClassifyDocumentVersion(
#                       VERSION_MODEL))
#
# if s.get_bool("CL_DOC_VER_V2"):
#     from version_detector import VersionDetector as VD
#     from classify_document_version_v2 import ClassifyDocumentVersion as CDV
#
#     start_load_ver_model = datetime.now()
#     ver_det = VD(2, 'tcnn_wavelet_01', 1, saved_model=s.get_str("VER_MDL_V2"))
#     end_load_ver_model = datetime.now()
#     elapsed_load_ver_model = end_load_ver_model - start_load_ver_model
#     print('Time to load version model V2 {}'.format(elapsed_load_ver_model))
#     API.add_route('/classifyDocumentVersionV2', CDV(ver_det))
#
# if s.get_bool("CL_DOC_LDMK"):
#     NONTEXT_MODEL = classify.Classify(s.get_str("NTXT_MDL"),
#                                       input_layer_size=s.get_int("NTXT_IN_LYR"))
#     TEXT_MODEL = classify.Classify(
#         s.get_str("TXT_MDL"), input_layer_size=s.get_int("TXT_IN_LYR"))
#
#     CLR_CHK_MODEL = classify.Classify(
#         s.get_str("CLR_CHK_MDL"), input_layer_size=s.get_int("CLR_CHK_IN_LYR"),
#         named_layer=False)
#
#     API.add_route('/classifyDocumentLandmark',
#                   classify_document_landmark.ClassifyDocumentLandmark(
#                       NONTEXT_MODEL, TEXT_MODEL, CLR_CHK_MODEL))
#
# if s.get_bool("CL_SNGL_FRM_HOLO"):
#     LABEL_MODEL = classify.Classify(s.get_str("LBL_MDL"),
#                                     input_layer_size=s.get_int("LBL_IN_LYR"))
#     API.add_route('/classifySingleFrameHologram',
#                   classify_single_frame_hologram.ClassifySingleFrameHologram(
#                       LABEL_MODEL))
#
# if s.get_bool("DET_TEXT_OCR"):
#     API.add_route('/detectTextOcr', detect_text_ocr.DetectTextOcr('images'))
#
# if s.get_bool("DET_8KD_MOD"):
#     import sys
#     import os
#     import pandas as pd
#     import detect_document_resources
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#     from card_detector import CardDetector
#     from detectLandmarkUtil import DetectLandmarkUtil
#     from eightKeypointDetector import EightKeypointDetector
#
#     score_8kpd = list(pd.read_csv(s.get_str("LDMK_THRESHOLD_V2_1"))['0'])
#
#     yolo_8kpd = YOLO(
#         **{
#             "model_path": s.get_str("LDMK_MODEL_WEIGHTS_V2_1"),
#             "anchors_path": s.get_str("ANCHORS"),
#             "classes_path": s.get_str("LDMK_MODEL_CLASSES_V2_1"),
#             # "score": s.get_float("MY_8KD_YOLO_THRESHOLD"),
#             "score": score_8kpd,
#         }
#     )
#     start_load_yolo_8kpd = datetime.now()
#     verify_8kpd = DetectLandmarkUtil(
#         model=yolo_8kpd,
#         model_weights=s.get_str("LDMK_MODEL_WEIGHTS_V2_1"),
#         anchors=s.get_str("ANCHORS"),
#         model_classes=s.get_str("LDMK_MODEL_CLASSES_V2_1"),
#         # threshold=s.get_float("MY_8KD_YOLO_THRESHOLD"),
#         threshold=score_8kpd,
#         save_img=False)
#     end_load_yolo_8kpd = datetime.now()
#     elapsed_load_yolo_8kpd = end_load_yolo_8kpd - start_load_yolo_8kpd
#     print('Time to load object detection landmark model V2 on 8kpd: {}'.format(
#         elapsed_load_yolo_8kpd))
#
#     EIGHT_KD_DETECTOR = EightKeypointDetector(s.get_str("MY_8KD_MDL"),
#                                               s.get_int("MY_8KD_IN_LYR"),
#                                               cl=s.get_bool("MY_8KD_CONF_LVL"),
#                                               ct=s.get_bool("MY_8KD_CARD_TYPE"))
#     EIGHT_KEYPOINT_DETECTOR = CardDetector(EIGHT_KD_DETECTOR, error_log=True,
#                                            debug_log=True, debug_sides=False,
#                                            debug_lines=False,
#                                            debug_points=False,
#                                            debug_detected_card=True,
#                                            debug_corrected_card=True,
#                                            debug_detect_lines_bilateral=False,
#                                            debug_detect_lines_otsu=False)
#     EIGHT_KEYPOINT_DETECTOR.load_card_set(s.get_str("CARD_SETT"))
#
#     API.add_route(
#         '/detectDocument_8KD',
#         detect_document_resources.DetectDocumentResources(
#             EIGHT_KEYPOINT_DETECTOR, landmark_det=verify_8kpd))

# if s.get_bool("CL_CARD_CLR"):
#     API.add_route('/classifyCardColor',
#                   classify_card_color.ClassifyCardColor())
#
# if s.get_bool("CL_PASS_CLR"):
#     API.add_route('/classifyPassportColor',
#                   classify_passport_color.ClassifyPassportColor())
#
# if s.get_bool("CRP_LDMK"):
#     API.add_route('/cropLandmark',
#                   crop_landmark.CropLandmark())
#
# if s.get_bool("CL_CARD_MAT"):
#     import material_detector
#
#     start_load_model = datetime.now()
#     pretrained_output_shape = (28, 28, 128)
#     resnet = material_detector.MaterialDetector(nb_classes=4, model_name=s.get_str("MAT_PRETRAINED_MDL_NAME"),
#                                                 seq_length=4)
#     modelv1 = material_detector.MaterialDetector(nb_classes=4, model_name=s.get_str("MDL_NAME_V1"),
#                                                seq_length=4,
#                                                input_shape=pretrained_output_shape,
#                                                saved_model=s.get_str("MAT_MDL_PTH_V1"))
#     end_load_model = datetime.now()
#     elapsed_load_model = end_load_model - start_load_model
#     print('Time to load material model V1 {}'.format(elapsed_load_model))
#     API.add_route('/classifyCardMaterial', classify_card_material_vgen.ClassifyCardMaterial(resnet=resnet, model=modelv1, version=1))
#
# if s.get_bool("CL_CARD_MAT_V1_2"):
#     import classify_card_material_viiigen
#     import material_detector_v1_2 as md_v1_2
#     import model_cnn
#
#     start_load_model = datetime.now()
#     model_v1_2 = model_cnn.get_compiled_model()
#     input_shape = (32, 32, 3)
#
#     # load weights
#     model_v1_2.load_weights(s.get_str("MAT_MDL_PTH_V1_2"))
#     loaded_modelv1_2 = md_v1_2.MaterialDetector(model=model_v1_2, input_shape=input_shape)
#
#     end_load_model = datetime.now()
#     elapsed_load_model = end_load_model - start_load_model
#     print('Time to load material model V1.2 {}'.format(elapsed_load_model))
#     API.add_route('/classifyCardMaterialV1_2', classify_card_material_viiigen.ClassifyCardMaterial(model=loaded_modelv1_2, version=1.2))
#
# if s.get_bool("CL_CARD_MAT_V2"):
#     import material_detector_v2
#
#     start_load_modelv2 = datetime.now()
#     pretrained_input_shape = (224, 224, 3)
#     modelv2 = material_detector_v2.MaterialDetector(nb_classes=4, model_name=s.get_str("MDL_NAME_V2"),
#                                                seq_length=4,
#                                                input_shape=pretrained_input_shape,
#                                                saved_model=s.get_str("MAT_MDL_PTH_V2"))
#     end_load_modelv2 = datetime.now()
#     elapsed_load_modelv2 = end_load_modelv2 - start_load_modelv2
#     print('Time to load material model V2 {}'.format(elapsed_load_modelv2))
#     API.add_route('/classifyCardMaterialV2', classify_card_material_vgen.ClassifyCardMaterial(model=modelv2, version=2))
#
# if s.get_bool("CL_CARD_MAT_V3"):
#     import material_detector_v3
#
#     start_load_modelv3 = datetime.now()
#     pretrained_input_shape = (224, 224, 3)
#     modelv3 = material_detector_v3.MaterialDetector(nb_classes=5, model_name=s.get_str("MDL_NAME_V3"),
#                                                seq_length=4,
#                                                input_shape=pretrained_input_shape,
#                                                saved_model=s.get_str("MAT_MDL_PTH_V3"))
#     end_load_modelv3 = datetime.now()
#     elapsed_load_modelv3 = end_load_modelv3 - start_load_modelv3
#     print('Time to load material model V3 {}'.format(elapsed_load_modelv3))
#     API.add_route('/classifyCardMaterialV3', classify_card_material_vgen.ClassifyCardMaterial(model=modelv3, version=3))
#
# if s.get_bool("CL_CARD_MAT_V4"):
#     import material_detector_v4_1_2
#
#     start_load_modelv4 = datetime.now()
#     pretrained_input_shape = (224, 224, 3)
#     modelv4 = material_detector_v4_1_2.MaterialDetector(nb_classes=2, model_name=s.get_str("MDL_NAME_V4"),
#                                                seq_length=4,
#                                                input_shape=pretrained_input_shape,
#                                                saved_model=s.get_str("MAT_MDL_PTH_V4"))
#     end_load_modelv4 = datetime.now()
#     elapsed_load_modelv4 = end_load_modelv4 - start_load_modelv4
#     print('Time to load material model V4 {}'.format(elapsed_load_modelv4))
#     API.add_route('/classifyCardMaterialV4', classify_card_material_vgen.ClassifyCardMaterial(model=modelv4, version=4))
#
# if s.get_bool("CL_CARD_MAT_V4_3"):
#     # from material_detector_v4_3_1a import MaterialDetector as MD_a
#     from material_detector_v4_3_1b import MaterialDetector as MD_b
#
#     start_load_modelv4_3 = datetime.now()
#     detector_a = MD_b(s.get_str("MDL_NAME_V4_3_A"), saved_model=s.get_str("MAT_MDL_PTH_V4_3_A"),
#                       pretrained_modelname=s.get_str("MAT_MDL_B_PRETRAIN_NAME"), nb_classes=2)
#     detector_b = MD_b(s.get_str("MDL_NAME_V4_3_B"), saved_model=s.get_str("MAT_MDL_PTH_V4_3_B"),
#                       pretrained_modelname=s.get_str("MAT_MDL_B_PRETRAIN_NAME"))
#
#     end_load_modelv4_3 = datetime.now()
#     elapsed_load_modelv4_3 = end_load_modelv4_3 - start_load_modelv4_3
#     print('Time to load material model V4.3 {}'.format(elapsed_load_modelv4_3))
#     API.add_route('/classifyCardMaterialV4_3', classify_card_material_vigen.ClassifyCardMaterial(modelA=detector_a, modelB=detector_b, version=4.3))
#
# if s.get_bool("CL_CARD_MAT_V4_4"):
#     from material_detector_v4_4 import MaterialDetector as MD_4_4
#
#     start_load_modelv4_4 = datetime.now()
#     model_V4_4 = MD_4_4(s.get_str("MDL_NAME_V4_4"), saved_model=s.get_str("MAT_MDL_PTH_V4_4"),
#                       pretrained_modelname=s.get_str("MAT_MDL_PRETRAIN_NAME_V4_4"), nb_classes=4)
#     end_load_modelv4_4 = datetime.now()
#     elapsed_load_modelv4_4 = end_load_modelv4_4 - start_load_modelv4_4
#     print('Time to load material model V4.4 {}'.format(elapsed_load_modelv4_4))
#     API.add_route('/classifyCardMaterialV4_4', classify_card_material_viigen.ClassifyCardMaterial(model=model_V4_4, version=4.4))
#
# if s.get_bool("CL_CARD_MAT_V4_5"):
#     import os
#     import sys
#     import pandas as pd
#     import classify_card_material_9gen
#     from material_detector_v4_5 import MaterialDetector as MD_4_5
#
#     os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#
#     start_load_modelv4_5 = datetime.now()
#
#     score_v4_5 = list(pd.read_csv(s.get_str("THRESHOLD_V4_5"))['0'])
#     yolo_V4_5 = YOLO(
#         **{
#             "model_path": s.get_str("MODEL_WEIGHTS_V4_5"),
#             "anchors_path": s.get_str("ANCHORS_V4_5"),
#             "classes_path": s.get_str("MODEL_CLASSES_V4_5"),
#             # "score": s.get_float("THRESHOLD_V4_5"),
#             "score": score_v4_5,
#             "model_image_size": (416, 416),
#         }
#     )
#
#     model_V4_5 = MD_4_5(s.get_str("MDL_NAME_V4_5"),
#                         saved_model=s.get_str("MAT_MDL_PTH_V4_5"),
#                         pretrained_modelname=s.get_str("MAT_MDL_PRETRAIN_NAME_V4_5"),
#                         nb_classes=4)
#     end_load_modelv4_5 = datetime.now()
#     elapsed_load_modelv4_5 = end_load_modelv4_5 - start_load_modelv4_5
#     print('Time to load material model V4.5 {}'.format(elapsed_load_modelv4_5))
#     API.add_route('/classifyCardMaterialV4_5', classify_card_material_9gen.ClassifyCardMaterial(yolo_V4_5, model=model_V4_5, version=4.5))
#
# if s.get_bool("CL_SNGL_FRM_CARD_MAT"):
#     from classify_single_frame_card_material import ClassifyCardMaterial as CCM_V1
#     from material_detector_single_frame_v1 import MaterialDetector as MDSF_V1
#
#     start_load_mdsfv1_model = datetime.now()
#     model_mdsf_v1 = MDSF_V1(3, s.get_str("MDSF_MDL_NAME_V1"), 1, saved_model=s.get_str("MDSF_MDL_PTH_V1"))
#     end_load_mdsfv1_model = datetime.now()
#     elapsed_load_mdsfv1_model = end_load_mdsfv1_model - start_load_mdsfv1_model
#     print('Time to load single frame material model V1 {}'.format(elapsed_load_mdsfv1_model))
#     API.add_route('/classifySingleFrameCardMaterial', CCM_V1(model=model_mdsf_v1, version=1.0))
#
# if s.get_bool("CL_SNGL_FRM_CARD_MAT_V1_1"):
#     from classify_single_frame_card_material import ClassifyCardMaterial as CCM_V1_1
#     from material_detector_single_frame_v1 import MaterialDetector as MDSF_V1_1
#
#     start_load_mdsfv1_1_model = datetime.now()
#     model_mdsf_v1_1 = MDSF_V1_1(3, s.get_str("MDSF_MDL_NAME_V1_1"), 1, saved_model=s.get_str("MDSF_MDL_PTH_V1_1"))
#     end_load_mdsfv1_1_model = datetime.now()
#     elapsed_load_mdsfv1_1_model = end_load_mdsfv1_1_model - start_load_mdsfv1_1_model
#     print('Time to load single frame material model V1.1 {}'.format(elapsed_load_mdsfv1_1_model))
#     API.add_route('/classifySingleFrameCardMaterialV1_1', CCM_V1_1(model=model_mdsf_v1_1, version=1.1))
#
# if s.get_bool("CL_SNGL_FRM_CARD_MAT_V1_2"):
#     from classify_single_frame_card_material_v1_2 import ClassifyCardMaterial as CCM_V1_2
#     from material_detector_single_frame_v1_2 import MaterialDetector as MDSF_V1_2
#
#     start_load_mdsfv1_2_model = datetime.now()
#     model_mdsf_v1_2 = MDSF_V1_2(saved_model=s.get_str("MDSF_MDL_PTH_V1_2"))
#     end_load_mdsfv1_2_model = datetime.now()
#     elapsed_load_mdsfv1_2_model = end_load_mdsfv1_2_model - start_load_mdsfv1_2_model
#     print('Time to load single frame material model V1.2 {}'.format(elapsed_load_mdsfv1_2_model))
#     API.add_route('/classifySingleFrameCardMaterialV1_2', CCM_V1_2(model=model_mdsf_v1_2, version=1.2))
#
# if s.get_bool("OCR_IC_NUM"):
#     API.add_route('/ocrICNumber', OcrICNumber())
#
# if s.get_bool("LDMK_TXT_COMB"):
#     API.add_route('/combineLandmarkText',
#                   comb_img_txt_ldmk.CombineImageTxtBasedLdmk())
#
# if s.get_bool("CL_DOC_LDMK_MP"):
#     API.add_route('/classifyDocumentLandmarkMP', ClassifyDocumentLandmarkMP())
#
# if s.get_bool("SEGM_CR"):
#     import os
#     import pandas as pd
#     import sys
#     import time
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#     from detectLandmarkUtil import DetectLandmarkUtil
#     from segmentation_crop_text import SegmentationCropText
#     import mytensorpack.examples.FasterRCNN.load as load_segm
#     # import mytensorpack.examples.FasterRCNN.predict_ic as pred
#     import mytensorpack.examples.FasterRCNN.predict_ic_v1_1 as pred
#
#     os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
#     start_load_od_segm = datetime.now()
#     segm_score = list(pd.read_csv(s.get_str("LDMK_THRESHOLD_V2_1"))['0'])
#     yolo_segm = YOLO(
#         **{
#             "model_path": s.get_str("LDMK_MODEL_WEIGHTS_V2_1"),
#             "anchors_path": s.get_str("ANCHORS"),
#             "classes_path": s.get_str("LDMK_MODEL_CLASSES_V2_1"),
#             # "score": s.get_float("SEG_YOLO_THRESHOLD"),
#             "score": segm_score,
#         }
#     )
#
#     verify_segm = DetectLandmarkUtil(
#         model=yolo_segm,
#         model_weights=s.get_str("LDMK_MODEL_WEIGHTS_V2_1"),
#         anchors=s.get_str("ANCHORS"),
#         model_classes=s.get_str("LDMK_MODEL_CLASSES_V2_1"),
#         # threshold=s.get_float("SEG_YOLO_THRESHOLD"),
#         threshold=segm_score,
#         save_img=False
#     )
#     end_load_od_segm = datetime.now()
#     elapsed_load_od_segm = end_load_od_segm - start_load_od_segm
#     print('Time to load object detection landmark model V2 on segm: {}'.format(
#         elapsed_load_od_segm))
#
#     # SEGM_MODEL_1 = pred.SegmentationPredict(s.get_str("SEG_MDL"))
#     SEGM_MODEL_2 = pred.SegmentationPredict(s.get_str("SEG_MDL_PASP"))
#     load_segm.setup_predict_config(gpu=False)
#
#     # segmentation_crop = segmentation_crop_text.SegmentationCropText(SEGM_MODEL_1, SEGM_MODEL_2, pads_on_cropped=s.get_bool("PADS_SEGM_CR"))
#     segmentation_crop = SegmentationCropText(SEGM_MODEL_2,
#                                              pads_on_cropped=s.get_bool("PADS_SEGM_CR"),
#                                              landmark_det=verify_segm)
#     API.add_route('/docSegmentation', segmentation_crop)
#     segmentation_crop.load_card_set(s.get_str("CARD_SETT"))
#
# if s.get_bool("SEGM_CR_TF2"):
#     import segmentation_crop_text_tf2
#     # from imkeras_segmentation.keras_segmentation.predict import \
#     #     model_from_checkpoint_path, predict
#     from imkeras_segmentation.keras_segmentation.models.all_models import \
#         model_from_name
#
#     ## add text detection model
#     ## load model session before api
#     # SESS, INP_IMG_TENSOR, OUT_TENSOR = load_segm.load_session(s.get_str("SEG_MDL"))
#     # with open('tf_files/vgg_unet_1/vgg_unet_1_config.json', 'r') as f:
#     #     model_config = json.loads(f.read())
#     SEGM_V2_CFG = s.get_str("SEGM_V2_CONFIG")
#     SEGM_V2_MDL = s.get_str("SEGM_V2_MDL")
#     with open(SEGM_V2_CFG, 'r') as f:
#         model_config = json.loads(f.read())
#     model = model_from_name[model_config['model_class']](model_config['n_classes'], input_height=model_config['input_height'],input_width=model_config['input_width'])
#
#     model.load_weights(SEGM_V2_MDL)
#     # load_segm.setup_predict_config(gpu=False)
#     segmentation_crop_tf2 = segmentation_crop_text_tf2.SegmentationCropTextTF2(model)
#
#     API.add_route('/docSegmentationTF2', segmentation_crop_tf2)
#     segmentation_crop_tf2.load_card_set(s.get_str("CARD_SETT"))
#
# if s.get_bool("DET_SERIAL_NO"):
#     API.add_route('/detectSerialNumber', detect_serial_number.DetectSerialNumber())
#
#
# if s.get_bool("DET_DOC_LDMK"):
#
#     import detectDocumentLandmark
#     import sys
#     import os
#     import pandas as pd
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#
#     os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
#     score = list(pd.read_csv(s.get_str("LDMK_THRESHOLD"))['0'])
#
#     yolo = YOLO(
#         **{
#             "model_path": s.get_str("LDMK_MODEL_WEIGHTS"),
#             "anchors_path": s.get_str("ANCHORS"),
#             "classes_path": s.get_str("LDMK_MODEL_CLASSES"),
#             # "score": s.get_float("LDMK_THRESHOLD"),
#             "score": score,
#         }
#     )
#     start_load_ldmk_model = datetime.now()
#     verify = detectDocumentLandmark.detectDocumentLandmark(model=yolo,
#                                                            model_weights=s.get_str("LDMK_MODEL_WEIGHTS"), anchors=s.get_str("ANCHORS"),
#                                                            model_classes=s.get_str("LDMK_MODEL_CLASSES"), threshold=score,
#                                                            # threshold=s.get_float("LDMK_THRESHOLD"),
#                                                            save_img=False)
#     end_load_ldmk_model = datetime.now()
#     elapsed_load_ldmk_mdl = end_load_ldmk_model - start_load_ldmk_model
#     print('Time to load object detection landmark model {}'.format(elapsed_load_ldmk_mdl))
#
#     API.add_route('/detectDocumentLandmark', verify)
#
# if s.get_bool("DET_DOC_LDMK_V2_1"):
#
#     import detectDocumentLandmarkV2_1
#     import sys
#     import os
#     import pandas as pd
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#
#     os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
#     score = list(pd.read_csv(s.get_str("LDMK_THRESHOLD_V2_1"))['0'])
#
#     yolo_v2_1 = YOLO(
#         **{
#             "model_path": s.get_str("LDMK_MODEL_WEIGHTS_V2_1"),
#             "anchors_path": s.get_str("ANCHORS"),
#             "classes_path": s.get_str("LDMK_MODEL_CLASSES_V2_1"),
#             # "score": s.get_float("LDMK_THRESHOLD_V2_1"),
#             "score": score,
#         }
#     )
#     start_load_ldmk_model_v2 = datetime.now()
#     verify_v2_1 = detectDocumentLandmarkV2_1.detectDocumentLandmark(model=yolo_v2_1,
#                                                                     model_weights=s.get_str("LDMK_MODEL_WEIGHTS_V2_1"), anchors=s.get_str("ANCHORS"),
#                                                                     model_classes=s.get_str("LDMK_MODEL_CLASSES_V2_1"), threshold=score,
#                                                                     # threshold=s.get_float("LDMK_THRESHOLD_V2_1"),
#                                                                     save_img=True)
#     end_load_ldmk_model_v2 = datetime.now()
#     elapsed_load_ldmk_mdl_v2 = end_load_ldmk_model_v2 - start_load_ldmk_model_v2
#     print('Time to load object detection landmark model V2 {}'.format(elapsed_load_ldmk_mdl_v2))
#
#     API.add_route('/detectDocumentLandmarkV2_1', verify_v2_1)
#
# if s.get_bool("DET_DOC_LDMK_V3"):
#
#     import tensorflow as tf
#     import pandas as pd
#     import sys
#     import time
#     import os
#     from detectDocumentLandmarkV3 import DetectDocumentLandmark as DDLV3
#     from object_detection.builders import model_builder
#     from object_detection.utils import label_map_util
#     from object_detection.utils import config_util
#
#     os.environ["CUDA_VISIBLE_DEVICES"] = "1"
#
#     start_time = time.time()
#     # Load pipeline config and build a detection model
#     configs = config_util.get_configs_from_pipeline_file(
#         s.get_str('PATH_TO_CFG_V3'))
#     model_config = configs['model']
#
#     detection_model = model_builder.build(
#         model_config=model_config, is_training=False)
#
#     # Restore checkpoint
#     ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)
#     ckpt.restore(os.path.join(
#         s.get_str("PATH_TO_CKPT_V3"), 'ckpt-0')).expect_partial()
#
#     category_index = label_map_util.create_category_index_from_labelmap(
#         s.get_str('PATH_TO_LABELS_V3'), use_display_name=True)
#
#     end_time = time.time()
#     elapsed_time = end_time - start_time
#     print('Took {} seconds to load landmark v3 model'.format(elapsed_time))
#
#     detectV3 = DDLV3(category_index=category_index,
#                                     config_path=configs,
#                                     threshold=s.get_float("LDMK_THRESHOLD_V3"),
#                                     detection_model=detection_model,
#                                     version=s.get_str("VER_DET_DOC_LDMK_V3"))
#
#     API.add_route('/detectDocumentLandmarkV3', detectV3)
#
# if s.get_bool("DET_DOC_LDMK_V3_1"):
#
#     import tensorflow as tf
#     import pandas as pd
#     import sys
#     import time
#     import os
#     from detectDocumentLandmarkV3 import DetectDocumentLandmark as DDLV3
#     from object_detection.builders import model_builder
#     from object_detection.utils import label_map_util
#     from object_detection.utils import config_util
#
#     os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
#
#     start_time = time.time()
#     # Load pipeline config and build a detection model
#     configs = config_util.get_configs_from_pipeline_file(
#         s.get_str('PATH_TO_CFG_V3_1'))
#     model_config = configs['model']
#
#     detection_model = model_builder.build(
#         model_config=model_config, is_training=False)
#
#     # Restore checkpoint
#     ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)
#     ckpt.restore(os.path.join(
#         s.get_str("PATH_TO_CKPT_V3_1"), 'ckpt-0')).expect_partial()
#
#     category_index = label_map_util.create_category_index_from_labelmap(
#         s.get_str('PATH_TO_LABELS_V3_1'), use_display_name=True)
#
#     end_time = time.time()
#     elapsed_time = end_time - start_time
#     print('Took {} seconds to load landmark v3.1 model'.format(elapsed_time))
#
#     detectV3_1 = DDLV3(category_index=category_index,
#                                     config_path=configs,
#                                     threshold=s.get_float("LDMK_THRESHOLD_V3_1"),
#                                     detection_model=detection_model,
#                                     version=s.get_str("VER_DET_DOC_LDMK_V3_1"))
#
#     API.add_route('/detectDocumentLandmarkV3_1', detectV3_1)
#
# if s.get_bool("DET_DOC_LDMK_V3_2"):
#
#     import tensorflow as tf
#     import pandas as pd
#     import sys
#     import time
#     import os
#     from detectDocumentLandmarkV3 import DetectDocumentLandmark as DDLV3
#     from object_detection.builders import model_builder
#     from object_detection.utils import label_map_util
#     from object_detection.utils import config_util
#
#     os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
#
#     start_time = time.time()
#     # Load pipeline config and build a detection model
#     configs = config_util.get_configs_from_pipeline_file(
#         s.get_str('PATH_TO_CFG_V3_2'))
#     model_config = configs['model']
#
#     detection_model = model_builder.build(
#         model_config=model_config, is_training=False)
#
#     # Restore checkpoint
#     ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)
#     ckpt.restore(os.path.join(
#         s.get_str("PATH_TO_CKPT_V3_2"), 'ckpt-0')).expect_partial()
#
#     category_index = label_map_util.create_category_index_from_labelmap(
#         s.get_str('PATH_TO_LABELS_V3_2'), use_display_name=True)
#
#     end_time = time.time()
#     elapsed_time = end_time - start_time
#     print('Took {} seconds to load landmark v3.2 model'.format(elapsed_time))
#
#     detectV3_2 = DDLV3(category_index=category_index,
#                                     config_path=configs,
#                                     threshold=s.get_float("LDMK_THRESHOLD_V3_2"),
#                                     detection_model=detection_model,
#                                     version=s.get_str("VER_DET_DOC_LDMK_V3_2"))
#
#     API.add_route('/detectDocumentLandmarkV3_2', detectV3_2)
#
# if s.get_bool("DET_LABEL"):
#
#     import detectLabel
#     import pandas as pd
#     import sys
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#
#     label_score = list(pd.read_csv(s.get_str("LABEL_THRESHOLD"))['0'])
#     yolo_label = YOLO(
#         **{
#             "model_path": s.get_str("LABEL_MODEL_WEIGHTS"),
#             "anchors_path": s.get_str("LABEL_ANCHORS"),
#             "classes_path": s.get_str("LABEL_MODEL_CLASSES"),
#             # "score": s.get_float("LABEL_THRESHOLD"),
#             "score": label_score,
#         }
#     )
#     start_load_label_model = datetime.now()
#     verify_label = detectLabel.detectLabel(model=yolo_label,
#                                               model_weights=s.get_str("LABEL_MODEL_WEIGHTS"), anchors=s.get_str("LABEL_ANCHORS"),
#                                               model_classes=s.get_str("LABEL_MODEL_CLASSES"), # threshold=s.get_float("LABEL_THRESHOLD"),
#                                               threshold=label_score,
#                                               save_img=False)
#     end_load_label_model = datetime.now()
#     elapsed_load_label_mdl = end_load_label_model - start_load_label_model
#     print('Time to load label detection model {}'.format(elapsed_load_label_mdl))
#
#     API.add_route('/detectLabel', verify_label)
#
# if s.get_bool("DET_LABEL_V50"):
#
#     import detectLabelV50 as dlV50
#     import pandas as pd
#     import sys
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#
#     labelv50_score = list(pd.read_csv(s.get_str("LABEL_THRESHOLD_V50"))['0'])
#
#     yolo_label_v50 = YOLO(
#         **{
#             "model_path": s.get_str("LABEL_MODEL_WEIGHTS_V50"),
#             "anchors_path": s.get_str("LABEL_ANCHORS"),
#             "classes_path": s.get_str("LABEL_MODEL_CLASSES_V50"),
#             # "score": s.get_float("LABEL_THRESHOLD_V50"),
#             "score": labelv50_score,
#         }
#     )
#     start_load_label_model_v50 = datetime.now()
#     verify_label_v50 = dlV50.detectLabel(model=yolo_label_v50,
#                                               model_weights=s.get_str("LABEL_MODEL_WEIGHTS_V50"), anchors=s.get_str("LABEL_ANCHORS"),
#                                               model_classes=s.get_str("LABEL_MODEL_CLASSES_V50"), # threshold=s.get_float("LABEL_THRESHOLD_V50"),
#                                               threshold=labelv50_score,
#                                               save_img=True)
#     end_load_label_model_v50 = datetime.now()
#     elapsed_load_label_mdl_v50 = end_load_label_model_v50 - start_load_label_model_v50
#     print('Time to load label detection model v50 : {}'.format(elapsed_load_label_mdl_v50))
#
#     API.add_route('/detectLabelV50', verify_label_v50)
#
# if s.get_bool("DET_LABEL_V1_3"):
#
#     import tensorflow as tf
#     import pandas as pd
#     import sys
#     import time
#     import os
#     from detectLabelV1_3 import detectObjectV3
#     from object_detection.builders import model_builder
#     from object_detection.utils import label_map_util
#     from object_detection.utils import config_util
#
#     os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
#
#     start_time = time.time()
#     # Load pipeline config and build a detection model
#     configs = config_util.get_configs_from_pipeline_file(
#         s.get_str('PATH_TO_CFG_V76'))
#     model_config = configs['model']
#
#     detection_model = model_builder.build(
#         model_config=model_config, is_training=False)
#
#     # Restore checkpoint
#     ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)
#     ckpt.restore(os.path.join(
#         s.get_str("PATH_TO_CKPT_V76"), 'ckpt-0')).expect_partial()
#
#     category_index = label_map_util.create_category_index_from_labelmap(
#         s.get_str('PATH_TO_LABELS_V76'), use_display_name=True)
#
#     end_time = time.time()
#     elapsed_time = end_time - start_time
#     print('Took {} seconds to load label model v1.3'.format(elapsed_time))
#
#     det_label_v76 = detectObjectV3(category_index=category_index,
#                                     config_path=configs,
#                                     threshold=s.get_float("LABEL_THRESHOLD_V76"),
#                                     save_img=True, detection_model=detection_model)
#
#     API.add_route('/detectLabelV1_3', det_label_v76)
#
# if s.get_bool("DET_LABEL_V1_4"):
#     import tensorflow as tf
#     import pandas as pd
#     import sys
#     import time
#     import os
#     from detectLabelV1_4 import detectLabelV1_4
#     from object_detection.builders import model_builder
#     from object_detection.utils import label_map_util
#     from object_detection.utils import config_util
#
#     os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
#
#     start_time = time.time()
#
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#
#     labelv50_score = list(pd.read_csv(s.get_str("LABEL_THRESHOLD_V50"))['0'])
#
#     yolo_label_v50 = YOLO(
#         **{
#             "model_path": s.get_str("LABEL_MODEL_WEIGHTS_V50"),
#             "anchors_path": s.get_str("LABEL_ANCHORS"),
#             "classes_path": s.get_str("LABEL_MODEL_CLASSES_V50"),
#             # "score": s.get_float("LABEL_THRESHOLD_V50"),
#             "score": labelv50_score,
#         }
#     )
#
#     end_time = time.time()
#     elapsed_time = end_time - start_time
#     print('Took {} seconds to load label model v1.4'.format(elapsed_time))
#
#     det_label_v76 = detectLabelV1_4(
#         thresholdv1_2=labelv50_score,
#         save_img=True,
#         modelv_2=yolo_label_v50,
#         model_weights=s.get_str("LABEL_MODEL_WEIGHTS_V50"),
#         anchors=s.get_str("LABEL_ANCHORS"),
#         model_classes=s.get_str("LABEL_MODEL_CLASSES_V50"))
#
#     API.add_route('/detectLabelV1_4', det_label_v76)
#
# if s.get_bool("DET_MYTENTERA_ID"):
#
#     from detectMyTenteraID import DetectMyTenteraID
#     import pandas as pd
#     import sys
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#
#     ldmk_score = list(pd.read_csv(s.get_str("LDMK_THRESHOLD"))['0'])
#     yolo_tentera = YOLO(
#         **{
#             "model_path": s.get_str("LDMK_MODEL_WEIGHTS"),
#             "anchors_path": s.get_str("ANCHORS"),
#             "classes_path": s.get_str("LDMK_MODEL_CLASSES"),
#             # "score": s.get_float("LDMK_THRESHOLD"),
#             "score": ldmk_score,
#         }
#     )
#     start_load_mdl_tentera = datetime.now()
#     verify_mytentera = DetectMyTenteraID(model=yolo_tentera, model_weights=s.get_str("LDMK_MODEL_WEIGHTS"),
#                  anchors=s.get_str("ANCHORS"),
#                  model_classes=s.get_str("LDMK_MODEL_CLASSES"),
#                  threshold=ldmk_score, save_img=False)
#     end_load_mdl_tentera = datetime.now()
#     elapsed_load_mdl_tentera = end_load_mdl_tentera - start_load_mdl_tentera
#     print('Time to load MyTentera Yolo Model {}'.format(elapsed_load_mdl_tentera))
#
#     API.add_route('/detectMyTenteraID', verify_mytentera)
#
# if s.get_bool("CL_IDV_HOLO"):
#     from classifyIndividualHologram import ClassifyIndividualHologram
#
#     ih_ghost = classify.Classify(s.get_str("HOLO_GHOST_MDL"), input_layer_size=224, hologram=True)
#     ih_name = classify.Classify(s.get_str("HOLO_NAME_MDL"), input_layer_size=224, hologram=True)
#     ih_icnumber_left = classify.Classify(s.get_str("HOLO_IC_LEFT_MDL"),
#                                          input_layer_size=224, named_layer=True, hologram=True)
#     ih_icnumber_right = classify.Classify(s.get_str("HOLO_IC_RIGHT_MDL"),
#                                           input_layer_size=224, named_layer=True, hologram=True)
#
#     API.add_route('/classifyIndividualHologram', ClassifyIndividualHologram(ih_ghost, ih_icnumber_left, ih_icnumber_right, ih_name, 'images'))
#
# if s.get_bool("CL_IDV_HOLO_V2"):
#     import classifyIndividualHologram_V2
#     import landmark_regions_crop
#     import pandas as pd
#     import sys
#
#     sys.path.append(s.get_str("SRC_PATH"))
#     from keras_yolo3.yolo import YOLO
#
#     holo_score = list(pd.read_csv(s.get_str("LDMK_THRESHOLD"))['0'])
#     yolo_holo = YOLO(
#         **{
#             "model_path": s.get_str("LDMK_MODEL_WEIGHTS"),
#             "anchors_path": s.get_str("ANCHORS"),
#             "classes_path": s.get_str("LDMK_MODEL_CLASSES"),
#             "score":holo_score,
#         }
#     )
#
#     verify_holo_V2 = landmark_regions_crop.detectDocumentLandmark(model=yolo_holo,
#                                                           model_weights=s.get_str("LDMK_MODEL_WEIGHTS"),
#                                                           anchors=s.get_str("ANCHORS"),
#                                                           model_classes=s.get_str("LDMK_MODEL_CLASSES"),
#                                                           # threshold=s.get_float("THRESHOLD"),
#                                                           threshold=holo_score,
#                                                           save_img=False)
#
#     ih_ghost = classify.Classify(s.get_str("HOLO_GHOST_MDL"), input_layer_size=224, hologram=True)
#     ih_name = classify.Classify(s.get_str("HOLO_NAME_MDL"), input_layer_size=224, hologram=True)
#     ih_icnumber_left = classify.Classify(s.get_str("HOLO_IC_LEFT_MDL_V2"),
#                                          input_layer_size=224, named_layer=True, hologram=True)
#     ih_icnumber_right = classify.Classify(s.get_str("HOLO_IC_RIGHT_MDL_V2"),
#                                           input_layer_size=224, named_layer=True, hologram=True)
#
#     API.add_route('/classifyIndividualHologramV2',
#                   classifyIndividualHologram_V2.ClassifyIndividualHologram(verify,
#                                                                            ih_ghost, ih_icnumber_left,
#                                                                            ih_icnumber_right, ih_name, 'images'))
#
# if s.get_bool("SEGM_SNGL_FRM_HOLOGRAM"):
#     import mytensorpack.examples.FasterRCNN.load as load_segm
#     import mytensorpack.examples.FasterRCNN.predict_hologram as pred
#     import segmentation_hologram
#     print('Initiate...')
#     SEGM_MODEL_HOLO = pred.SegmentationPredict(s.get_str("SEGM_HOLO_MDL"))
#     load_segm.setup_predict_config(gpu=False, segm_type=1)
#     # segmentation_crop = segmentation_crop_text.SegmentationCropText(SEGM_MODEL_1, SEGM_MODEL_2, pads_on_cropped=s.get_bool("PADS_SEGM_CR"))
#     segm_holo = segmentation_hologram.SegmentationHologram(SEGM_MODEL_HOLO)
#     API.add_route('/segmentSingleFrameHologram', segm_holo)
#     # segmentation_crop.load_card_set(s.get_str("CARD_SETT"))

# if s.get_bool("SEGM_SNGL_FRM_HOLOGRAM_V2_1"):
#     import mytensorpack.examples.FasterRCNN.load as load_segm
#     import mytensorpack.examples.FasterRCNN.predict_hologram as pred
#     import segmentation_hologramv2_1
#     print('Initiate...')
#     SEGM_MODEL_HOLO = pred.SegmentationPredict(s.get_str("SEGM_HOLO_MDL_V2_1"))
#     load_segm.setup_predict_config(gpu=False, segm_type=1)
#     # segmentation_crop = segmentation_crop_text.SegmentationCropText(SEGM_MODEL_1, SEGM_MODEL_2, pads_on_cropped=s.get_bool("PADS_SEGM_CR"))
#     segm_holo = segmentation_hologramv2_1.SegmentationHologram(SEGM_MODEL_HOLO)
#     API.add_route('/segmentSingleFrameHologramV2_1', segm_holo)

# if s.get_bool("SEGM_HOLOGRAM_V2_3"):
#     """
#     ------------------------
#     Hologram Segmentation v2.3
#     ------------------------
#     Parameters from .env:
#     SEGM_HOLOGRAM_V3: if True, to make this API functional
#     SEGM_HOLO_MDL_V3: location of neural network (before version folder)
#     SEGM_HOLO_MDL_V3_VERSION_CONTROL: specify which neural network version to use
#     SEGM_HOLO_V3_RESIZE: Image input resize. Ensure it matches with neural network
#     input layer
#     SEGM_HOLO_V3_SCORE: Hologram score threshold
#     """
#     from segmentation_hologram_v2_3 import SegmentationHologram as holo
#     print('Initiate single-frame segmentation hologram v2_3...')
#     segm_holo_resp = holo(input_resize=s.get_int("SEGM_HOLO_V2_3_RESIZE"),
#                           holo_score=s.get_float("SEGM_HOLO_V2_3_SCORE"),
#                           pixel_thresh=s.get_float("SEGM_HOLO_V2_3_PIXEL"),
#                           nonoverlap_thresh=s.get_float("SEGM_HOLO_V2_3_NONOVERLAP"))
#     API.add_route('/segmentHologram_v2_3/single', segm_holo_resp, suffix='single')
#     API.add_route('/segmentHologram_v2_3/multi', segm_holo_resp, suffix='multi')

if s.get_bool("LDMARK_VERIFY_V4"):
    """
    ------------------------
    Landmark Verification V4.0
    ------------------------
    Parameters from .env:
    LDMARK_VERIFY_V4: if True, to make this API functional
    """
    from landmark_v4 import LandmarkVerify as landmarkver
    print('Initiate Landmark Verification version 4.0 ...')
    segm_holo_resp = landmarkver(config_path='landmark_v4/landmark.ini')
    API.add_route('/landmarkVerify_v4/single', segm_holo_resp, suffix='single')


# if s.get_bool("SEGM_HOLOGRAM_V2_2_1"):
#     """
#     ------------------------
#     Hologram Segmentation v2_2_1
#     ------------------------
#     Parameters from .env:
#     SEGM_HOLOGRAM_V2_2_1: if True, to make this API functional
#     SEGM_HOLO_221_RESIZE: Image input resize. Ensure it matches with neural network input layer
#     """
#     from segmentation_hologram_v2_2_1 import SegmentationHologram as holo
#     print('Initiate single-frame segmentation hologram v3...')
#     segm_holo_resp = holo(s.get_int("SEGM_HOLO_221_RESIZE"))
#     API.add_route('/segmentHologram_v2_2_1/single', segm_holo_resp, suffix='single')



# if s.get_bool("SEGM_MULTI_FRM_HOLOGRAM_V3"):
#     from segmentation_hologram_v3 import SegmentationHologram as holo
#     print('Initiate multi-frame segmentation hologram v3...')
#     segm_holo_resp = holo(s.get_str("SEGM_HOLO_MDL_V3"),
#                           s.get_int("SEGM_HOLO_MDL_V3_VERSION_CONTROL"),
#                           s.get_int("SEGM_HOLO_V3_RESIZE"),
#                           s.get_float("SEGM_HOLO_V3_SCORE"))
#     API.add_route('/segmentSingleFrameHologramV3', segm_holo_resp)

# if s.get_bool("SEGM_MULTI_FRM_HOLOGRAM"):
#     import mytensorpack.examples.FasterRCNN.load as load_segm
#     import mytensorpack.examples.FasterRCNN.predict_hologram as pred
#     import segmentation_multi_hologram
#     print('Initiate...')
#     SEGM_MODEL_HOLO = pred.SegmentationPredict(s.get_str("SEGM_HOLO_MDL_V2_1"))
#     load_segm.setup_predict_config(gpu=False, segm_type= 1)
#     # segmentation_crop = segmentation_crop_text.SegmentationCropText(SEGM_MODEL_1, SEGM_MODEL_2, pads_on_cropped=s.get_bool("PADS_SEGM_CR"))
#     segm_holo = segmentation_multi_hologram.SegmentationHologram(SEGM_MODEL_HOLO)
#     API.add_route('/segmentMultiFrameHologram', segm_holo)
#     # segmentation_crop.load_card_set(s.get_str("CARD_SETT"))

# if s.get_bool("CKS_MRZ"):
#     from checksum_mrz import ChecksumMRZ
#
#     API.add_route('/checksumMRZ', ChecksumMRZ())
#
# if s.get_bool("MRZ_SPLIT"):
#     from preprocess_MRZ import MRZSplit
#
#     mrzSplit = MRZSplit()
#     API.add_route('/mrzSplit', mrzSplit)
#
# if s.get_bool("QUALITY_CHECK"):
#     from qualityControlCheck import QualityControlCheck
#
#     qcCheck = QualityControlCheck()
#     API.add_route('/qualityControlCheck', qcCheck)
