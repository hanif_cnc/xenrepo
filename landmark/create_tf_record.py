"""
Reads all kinds of image file format but keeps it as jpg
when converting to tf record
"""
import os
import io
import json
import contextlib2
import pandas as pd
from PIL import Image
from tqdm import tqdm
import tensorflow as tf
from concurrent import futures
from object_detection.utils import dataset_util
from object_detection.dataset_tools import tf_record_creation_util

NUM_IMAGES = 500  # How many images per shard tfrecord (aim to get each shard about 100mb)
INPUT_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/67_dataset/back_mykad/train'
# ensure naming in OUTPUT_FILEBASE reflects data type (train, val)
OUTPUT_FILEBASE = 'F:/Xendity/Project_LandmarkMyKad/data/67_dataset_tfrecord/back_mykad/train/train_dataset.record'
CLASS_CSV = 'back_mykad.csv'


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    chunked_list = []
    for i in range(0, len(lst), n):
        chunked_list.append(lst[i:i + n])

    return chunked_list


def json_to_example(json_folder, df_class):
    # Gather all json paths
    all_json = [f'{json_folder}/{i}' for i in os.listdir(json_folder) if i.endswith('.json')]

    #######################################################
    """
    Multithread function
    """
    def read_json(each_json):
        with open(each_json, 'r') as f:
            data = json.load(f)

            # get json "shapes"
            data_shapes = data["shapes"]

            all_shapes = []
            for each_shape in data_shapes:
                # Gather label and points info
                obj_label = each_shape['label']
                x1, y1 = each_shape['points'][0]
                x2, y2 = each_shape['points'][1]

                # Get ID
                id_no = df_class.loc[df_class['classes'] == obj_label]['id'].tolist()[0]

                # Recheck max and min values of points xy
                xmin = int(min([x1, x2]))
                xmax = int(max([x1, x2]))
                ymin = int(min([y1, y2]))
                ymax = int(max([y1, y2]))
                all_shapes.append([obj_label, id_no, xmin, xmax, ymin, ymax])

        return [each_json, all_shapes]

    #######################################################
    # Read jsons and obtain info for classes
    print("Reading JSONs")
    with futures.ThreadPoolExecutor() as executor:
        return_list = list(tqdm(executor.map(read_json, all_json), total=len(all_json)))

    return return_list


def create_tf_example(example):
    img_path = example[0]
    shape_lists = example[1]

    # Get filename
    img_base = img_path.split('/')[-1].replace(".json", '')
    to_remove = f'{img_base}.json'
    folder_path = img_path.replace(f'{to_remove}', '')

    # Search image
    ext_list = ['.jpg', '.jpeg', '.png']
    img_path = ''
    for each_ext in ext_list:
        test_path = f'{folder_path}{img_base}{each_ext}'
        if os.path.exists(test_path):
            img_path = test_path

    # Read image
    with tf.io.gfile.GFile(img_path, 'rb') as fid:
        encoded_jpg = fid.read()
    encoded_jpg_io = io.BytesIO(encoded_jpg)
    image = Image.open(encoded_jpg_io)
    width, height = image.size

    # Prepare variables
    filename = img_path.split('/')[-1].encode('utf8')
    image_format = b'jpg'
    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []
    classes_text = []
    classes = []

    for each_shape in shape_lists:
        xmins.append(each_shape[2]/width)
        xmaxs.append(each_shape[3] / width)
        ymins.append(each_shape[4] / height)
        ymaxs.append(each_shape[5] / height)
        classes_text.append(each_shape[0].encode('utf8'))
        classes.append(each_shape[1])

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_jpg),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))

    return tf_example


def main(indir: str, outdir: str, img_shards: int, csv_path: str):
    # Ensure correct format
    indir = indir.replace('\\', '/')
    outdir = outdir.replace('\\', '/')

    # Create output folder

    out_toremove = outdir.split('/')[-1]
    out_folder = outdir.replace(f'/{out_toremove}', '')
    os.makedirs(out_folder, exist_ok=True)

    # Read csv
    df_class = pd.read_csv(csv_path)

    # Create examples (data regarding classes)
    examples = json_to_example(indir, df_class)

    # Split list
    examples = chunks(examples, img_shards)
    amt_examples = len(examples)

    # Create TFrecords
    print("Creating TF records")
    with contextlib2.ExitStack() as tf_record_close_stack:
        output_tfrecords = tf_record_creation_util.open_sharded_output_tfrecords(
            tf_record_close_stack, outdir, amt_examples)
        for idx_grp, each_group in tqdm(enumerate(examples), total=amt_examples):
            for each_example in each_group:
                tf_example = create_tf_example(each_example)
                output_shard_index = idx_grp
                output_tfrecords[output_shard_index].write(tf_example.SerializeToString())


if __name__ == '__main__':
    main(INPUT_DIR, OUTPUT_FILEBASE, NUM_IMAGES, CLASS_CSV)
