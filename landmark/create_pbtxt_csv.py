"""
Read JSON from dataset and create pbtxt or cvs from unique classes.
"""
import os
import json
from tqdm import tqdm
from concurrent import futures
from collections import Counter

JSON_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/67_dataset/front_mykad/train'
OUTPUT_PBTXT = 'front_mykad.pbtxt'
OUTPUT_CSV = 'front_mykad.csv'


def read_json(json_path):
    """
    For multithreading read json
    :param json_path: path to json
    :return:
    list of class OR int 0 if json label empty
    """
    with open(json_path, 'r') as f:
        data = json.load(f)
        data_shape = data['shapes']
        json_landmark = [i['label'] for i in data_shape]
        if json_landmark:
            return json_landmark
        else:
            return 0


def create_csv(json_dir: str, output: str):
    """
    Create csv based on unique class from all json in json_dir
    :param json_dir: Path to json directory
    :param output: Path to pbtxt
    """
    print("Running CSV creation function...")
    print("Reading JSON...")
    # Ensure formatting of inputs
    json_dir = json_dir.replace("\\", '/')
    output = output.replace("\\", '/')

    # Obtain all json paths
    json_list = [f'{json_dir}/{i}' for i in os.listdir(json_dir) if i.endswith('.json')]

    # Read and store amount of landmarks
    with futures.ThreadPoolExecutor() as executor:
        all_landmarks = list(tqdm(executor.map(read_json, json_list), total=len(json_list)))

    # Remove empty sublist and flatten list
    all_landmarks = [i for i in all_landmarks if i]
    all_landmarks = [item for sublist in all_landmarks for item in sublist]

    # Get unique classes
    list_landmark = Counter(all_landmarks).keys()

    print("List of all class:")
    for index, each_landmark in enumerate(list_landmark, start=1):
        print(f"  {index}: {each_landmark}")
    print(f"Total class: {len(list_landmark):,}")

    # Create pbtxt file
    with open(output, "w") as file:
        file.write(f"id,classes\n")
        for index, each_landmark in enumerate(list_landmark, start=1):
            file.write(f"{index},{each_landmark}\n")

    print(f"Finished creating {output}!")


def create_pbtxt(json_dir: str, output: str):
    """
    Create pbtxt based on unique class from all json in json_dir
    :param json_dir: Path to json directory
    :param output: Path to pbtxt
    """
    print("Running PBTXT creation function...")
    print("Reading JSON...")
    # Ensure formatting of inputs
    json_dir = json_dir.replace("\\", '/')
    output = output.replace("\\", '/')

    # Obtain all json paths
    json_list = [f'{json_dir}/{i}' for i in os.listdir(json_dir) if i.endswith('.json')]

    # Read and store amount of landmarks
    with futures.ThreadPoolExecutor() as executor:
        all_landmarks = list(tqdm(executor.map(read_json, json_list), total=len(json_list)))

    # Remove empty sublist and flatten list
    all_landmarks = [i for i in all_landmarks if i]
    all_landmarks = [item for sublist in all_landmarks for item in sublist]

    # Get unique classes
    list_landmark = Counter(all_landmarks).keys()

    print("List of all class:")
    for index, each_landmark in enumerate(list_landmark, start=1):
        print(f"  {index}: {each_landmark}")
    print(f"Total class: {len(list_landmark):,}")

    # Create pbtxt file
    with open(output, "w") as file:
        for index, each_landmark in enumerate(list_landmark, start=1):
            file.write("item {\n")
            file.write(f"    id: {index}\n")
            file.write(f"    name: '{each_landmark}'\n")
            file.write("}\n")
            file.write("\n")

    print(f"Finished creating {output}!")


def main():
    create_pbtxt(JSON_DIR, OUTPUT_PBTXT)
    create_csv(JSON_DIR, OUTPUT_CSV)


if __name__ == '__main__':
    main()

