import json
import os
from tqdm import tqdm

INPUT_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/66-dataset/validation'

json_list = [f'{INPUT_DIR}/{i}' for i in os.listdir(INPUT_DIR) if i.endswith('.json')]
img_list = [f'{INPUT_DIR}/{i}' for i in os.listdir(INPUT_DIR) if i.endswith(('.png', '.jpg', '.png'))]

# Check within JSON wrong imagePath and its filename.
faulty_json = []
for each_json in tqdm(json_list):
    with open(each_json, 'r') as f:
        data = json.load(f)

        image_raw = data["imagePath"]
        image_path = image_raw.replace('\\', '/')

        image_path_basename = image_path.split('/')[-1].split('.')[0]
        image_file_basename = each_json.split('/')[-1].split('.')[0]

        if not image_path_basename == image_file_basename:
            faulty_json.append(each_json)

for each_faulty in faulty_json:
    print(each_faulty)

print(len(faulty_json))



