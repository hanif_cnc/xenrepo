"""
Extract and draw images with boxes from TFrecords

The drawing is based on TFrecord following parameters:
'image/encoded'
'image/object/bbox/xmin'
'image/object/bbox/xmax'
'image/object/bbox/ymin'
'image/object/bbox/ymax'
'image/object/class/text'
'image/object/class/label'
'image/height'
'image/width'
'image/filename'

Ensure in the input folders are only Tf record files
In the output image, the number represents the assigned class ID
"""
import os
from tqdm import tqdm
import numpy as np
import cv2
import tensorflow as tf

INPUT_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/67_dataset_tfrecord/back_mykad/val'
OUTPUT_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/67_dataset_drawn/back_mykad/val'


def parse_tfr_element(element):
    data = {
        'image/encoded': tf.io.FixedLenFeature([], tf.string),
        'image/object/bbox/xmin': tf.io.VarLenFeature(tf.float32),
        'image/object/bbox/xmax': tf.io.VarLenFeature(tf.float32),
        'image/object/bbox/ymin': tf.io.VarLenFeature(tf.float32),
        'image/object/bbox/ymax': tf.io.VarLenFeature(tf.float32),
        'image/object/class/text': tf.io.VarLenFeature(tf.string),
        'image/object/class/label': tf.io.VarLenFeature(tf.int64),
        'image/height': tf.io.FixedLenFeature([], tf.int64),
        'image/width': tf.io.FixedLenFeature([], tf.int64),
        'image/filename': tf.io.FixedLenFeature([], tf.string),
    }

    sample = tf.io.parse_single_example(element, data)

    return sample


def proc_tf(tf_path, outdir, all_label: dict):
    # Read and parse dataset
    dataset = tf.data.TFRecordDataset(tf_path)
    dataset = dataset.map(parse_tfr_element)

    total_data = [1 for i in dataset]

    # Go through each image in dataset
    for next_element in tqdm(dataset, total=len(total_data)):
        # Extract
        xmin_list = next_element['image/object/bbox/xmin'].values.numpy()
        xmax_list = next_element['image/object/bbox/xmax'].values.numpy()
        ymin_list = next_element['image/object/bbox/ymin'].values.numpy()
        ymax_list = next_element['image/object/bbox/ymax'].values.numpy()
        text_list = next_element['image/object/class/text'].values.numpy()
        id_list = next_element['image/object/class/label'].values.numpy()
        img_height = next_element['image/height'].numpy().item()
        img_width = next_element['image/width'].numpy().item()
        img_data = next_element['image/encoded'].numpy()
        img_filename = next_element['image/filename'].numpy()

        # Convert image bytes to numpy array
        jpg_as_np = np.frombuffer(img_data, dtype=np.uint8)
        img = cv2.imdecode(jpg_as_np, flags=1)

        # Process for each class
        for xmin, xmax, ymin, ymax, text, class_id in zip(xmin_list,
                                                          xmax_list,
                                                          ymin_list,
                                                          ymax_list,
                                                          text_list,
                                                          id_list):
            # Convert box coordinates to integer
            xmin = int(xmin * img_width)
            xmax = int(xmax * img_width)
            ymin = int(ymin * img_height)
            ymax = int(ymax * img_height)

            # Convert bytes to string
            text = text.decode("utf-8")

            # Get color for class object
            if text not in all_label:
                all_label[text] = {}
                box_color = list(np.random.choice(range(256), size=3))
                all_label[text]['color'] = box_color
            else:
                box_color = all_label[text]['color']

            # Draw box
            rgb = box_color
            color = (int(rgb[0]), int(rgb[1]), int(rgb[2]))
            thickness = 1
            img = cv2.rectangle(img, (xmin, ymin), (xmax, ymax), color, thickness)

            # Draw text
            # Window name in which image is displayed
            font = cv2.FONT_HERSHEY_SIMPLEX
            box_text = f'{class_id}:{text}'
            coord = (xmin, ymin + 12)
            fontscale = 0.8
            thickness = 2
            img = cv2.putText(img, box_text, coord, font,
                              fontscale, color, thickness, cv2.LINE_AA)

        # Convert bytes to string
        img_filename = img_filename.decode("utf-8")

        # Output image
        out_path = f'{outdir}/{img_filename}'
        cv2.imwrite(out_path, img)

    return all_label


def extract_tfrecord(indir: str, outdir: str):
    # Ensure format of indir and outdir
    indir = indir.replace("\\", '/')
    outdir = outdir.replace("\\", '/')

    # Make outdir
    os.makedirs(outdir, exist_ok=True)

    # Get all TFrecord paths
    tf_list = [f'{indir}/{i}' for i in os.listdir(indir)]

    # Store color label for each classes
    all_label = {}

    # Process each TFrecords
    for ind_tf, each_tf in enumerate(tf_list, start=1):
        print(f"Processing TFrecord {ind_tf} out of {len(tf_list)}")
        all_label = proc_tf(each_tf, outdir, all_label)


def main():
    extract_tfrecord(INPUT_DIR, OUTPUT_DIR)


if __name__ == '__main__':
    main()
