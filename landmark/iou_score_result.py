import os
import cv2
import csv
import json
import base64
import glob
import requests
import numpy as np
import pandas as pd
from tqdm import tqdm
from matplotlib import pyplot as plt
from concurrent.futures import ThreadPoolExecutor
from requests.adapters import HTTPAdapter, Retry

# URL = "http://52.77.71.52:8005/detectDocumentLandmarkV3_3"
URL = "http://localhost:8001/landmarkVerify_v4/single"
TEST_IN_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/67_dataset/front_mykad/test'
# TEST_IN_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/67_dataset/back_mykad/test'
# RESULT_OUT_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/iou_output/front_iou0.5_conf10_ssdmobilenetsmallv2'
RESULT_OUT_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/iou_output/testing'
DOC_TYPE = 2
MODEL_VER = 8
THRESH_CONFIDENCE = 10  # range of 100%
THRESH_IOU = .5


def get_iou(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
        in [0, 1]
    """
    # assert bb1['x1'] < bb1['x2']
    # assert bb1['y1'] < bb1['y2']
    # assert bb2['x1'] < bb2['x2']
    # assert bb2['y1'] < bb2['y2']

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    # assert iou >= 0.0
    # assert iou <= 1.0
    return iou


def proc_json(each_proc):
    """
    :param each_proc: process list
    :return:
    list consisting of the following: [Gt, Detection boxes]
    """
    each_json = each_proc[0]
    url_proc = each_proc[1]
    doc_proc = each_proc[2]
    model_proc = each_proc[3]
    conf_proc = each_proc[4]
    iou_proc = each_proc[5]
    output_proc = each_proc[6]

    # Get image path relating to json. Img basename must be similar to json.
    json_basename = each_json.split('/')[-1]
    json_no_ext = json_basename.replace('.json', '')
    img_dir = each_json.replace(f'/{json_basename}', '')
    possible_img_ext = ['.jpeg', '.jpg', '.png']
    img_list = [f'{img_dir}/{json_no_ext}{i}' for i in possible_img_ext if
                os.path.exists(f'{img_dir}/{json_no_ext}{i}')]

    if len(img_list) == 0:
        print(f"Cannot find image for {json_basename}")
        return 0
    img_path = img_list[0]

    ###############################################################################################
    # Store ground truth into pc memory
    gt_dt = []
    with open(each_json, 'r') as f:
        load_gt = json.load(f)
        data_shapes = load_gt["shapes"]

        for each_id, each_shape in enumerate(data_shapes):
            # Gather label and points info
            obj_label = each_shape['label']
            x1, y1 = each_shape['points'][0]
            x2, y2 = each_shape['points'][1]
            # Recheck max and min values of points xy
            xmin = int(min([x1, x2]))
            xmax = int(max([x1, x2]))
            ymin = int(min([y1, y2]))
            ymax = int(max([y1, y2]))
            gt_dt.append([obj_label, xmin, xmax, ymin, ymax, False, each_id])

    ###############################################################################################
    # Obtain result from api
    req_json = {'imageFile': open(img_path, 'rb'),
                'docType': (None, f'{doc_proc}'),
                'modelVer': (None, f'{model_proc}'),
                'detThresh': (None, f'{conf_proc}'),
                'outImg': (None, 'True'),
                'coordinates': (None, 'True')}

    s = requests.Session()
    retries = Retry(total=10,
                    backoff_factor=0.1,
                    status_forcelist=[500, 502, 503, 504])

    s.mount('http://', HTTPAdapter(max_retries=retries))
    response = s.post(url_proc, files=req_json, timeout=10_000)

    if response.status_code == 200:
        body = json.loads(response.text)
        det_json = body['data']
    else:
        print(f"Error getting prediction for {json_basename}")
        return 0

    det_dt = []
    for each_id, each_shape in enumerate(det_json):
        det_dt.append([str(each_shape['landmark']), int(each_shape['xmin']),
                       int(each_shape['xmax']), int(each_shape['ymin']),
                       int(each_shape['ymax']), float(each_shape['confidence']), .0, False, each_id])

    # Remove detections lower than Threshold set
    det_dt = [i for i in det_dt if i[5] > conf_proc]

    ###############################################################################################
    # Process IOU
    for gt_id, gt_box in enumerate(gt_dt):
        # Filter detection
        det_to_process = [i for i in det_dt if i[0] == gt_box[0]]
        det_to_process = [i for i in det_to_process if not i[7]]

        # Skip process if no detection for ground truth
        if not len(det_to_process):
            continue

        # Prep to calculate iou
        iou_each_obj = []
        for each_det in det_to_process:

            bb1 = {'x1': each_det[1], 'x2': each_det[2], 'y1': each_det[3], 'y2': each_det[4]}
            bb2 = {'x1': gt_box[1], 'x2': gt_box[2], 'y1': gt_box[3], 'y2': gt_box[4]}

            iou_each_obj.append(get_iou(bb1, bb2))

        # Choose the highest iou and update detection box and ground truth
        id_max_iou = iou_each_obj.index(max(iou_each_obj))
        iou_val = iou_each_obj[id_max_iou]
        if iou_val > iou_proc:
            id_box = det_to_process[id_max_iou][-1]
            # Mark detection box with highest iou score as true detection
            det_dt[id_box][-2] = True
            det_dt[id_box][-3] = iou_val
            # Mark ground truth as correctly detected
            gt_dt[gt_id][-2] = True

            # Update other detections with highest iou if available
            for each_id, each_val in enumerate(iou_each_obj):
                if each_id != id_max_iou:
                    id_box = det_to_process[each_id][-1]
                    current_iou = det_dt[id_box][-3]
                    if current_iou < iou_val:
                        det_dt[id_box][-3] = iou_val
        else:
            # Update other detections with highest iou if available
            for each_id, each_val in enumerate(iou_each_obj):
                id_box = det_to_process[each_id][-1]
                current_iou = det_dt[id_box][-3]
                if current_iou < iou_val:
                    det_dt[id_box][-3] = iou_val

    ###############################################################################################
    # Store detection and ground truth results as csv
    gt_dir = f'{output_proc}/ground_truth'
    det_dir = f'{output_proc}/detection'
    os.makedirs(gt_dir, exist_ok=True)
    os.makedirs(det_dir, exist_ok=True)
    gt_csv_path = f'{gt_dir}/{json_no_ext}.csv'
    det_csv_path = f'{det_dir}/{json_no_ext}.csv'

    with open(gt_csv_path, 'w', newline='') as csv_file:
        csv_reader = csv.writer(csv_file, delimiter=',')
        csv_reader.writerow(['object_class', 'xmin', 'xmax', 'ymin', 'ymax', 'gt_detected', 'id'])
        for each_row in gt_dt:
            csv_reader.writerow(each_row)

    with open(det_csv_path, 'w', newline='') as csv_file:
        csv_reader = csv.writer(csv_file, delimiter=',')
        csv_reader.writerow(['object_class', 'xmin', 'xmax', 'ymin', 'ymax', 'confidence_score',
                             'iou_score', 'correct_detection', 'id'])
        for each_row in det_dt:
            csv_reader.writerow(each_row)

    ###############################################################################################
    # Draw boxes and save image
    img_dir = f'{output_proc}/img_result'
    os.makedirs(img_dir, exist_ok=True)
    img_outpath = f'{img_dir}/{json_no_ext}.jpg'

    # Read image
    img = cv2.imread(img_path)

    # Process for ground truth
    # Make the ground truth boxes slightly transparent
    color = (255, 255, 255)
    thickness = 1
    font = cv2.FONT_HERSHEY_SIMPLEX
    for each_row in gt_dt:
        shapes = np.zeros_like(img, np.uint8)
        shapes = cv2.rectangle(shapes, (each_row[1], each_row[3]), (each_row[2], each_row[4]), color, thickness)
        box_text = f'{each_row[0]},{each_row[5]}'
        coord = (each_row[1] + 2, each_row[3] + 20)
        fontscale = 0.8
        thickness = 2
        shapes = cv2.putText(shapes, box_text, coord, font, fontscale, color, thickness, cv2.LINE_AA)
        img = cv2.addWeighted(img, 1, shapes, -0.3, .0)

    # Process for Detection
    for each_row in det_dt:
        if each_row[-2]:
            color = (0, 255, 64)
        else:
            color = (0, 0, 255)
        img = cv2.rectangle(img, (each_row[1], each_row[3]), (each_row[2], each_row[4]), color, thickness)

        box_text = f'{each_row[-1]}: iou {each_row[-3]:.1f}, conf {each_row[-4]:.1f}'
        coord = (each_row[1], each_row[3] + 18)
        fontscale = 0.8
        thickness = 2
        img = cv2.putText(img, box_text, coord, font,
                          fontscale, color, thickness, cv2.LINE_AA)

    cv2.imwrite(img_outpath, img)


def evaluate_api(url_str: str, indir: str, outdir: str, doc_id: int, mod_ver: int, conf_thresh: float, iou_max: float):
    # Ensure similar string format
    indir = indir.replace('\\', '/')
    outdir = outdir.replace('\\', '/')

    # prep output folder
    os.makedirs(outdir, exist_ok=True)

    # Prepare processing list
    json_list = [[f'{indir}/{i}', url_str, doc_id, mod_ver, conf_thresh, iou_max, outdir]
                 for i in os.listdir(indir) if i.endswith('.json')]

    # FOR TESTING PURPOSES
    # json_list = json_list[0:10]

    with ThreadPoolExecutor(max_workers=4) as executor:
        # Run evaluation and obtain result for each image
        print("Running evaluation...")
        list(tqdm(executor.map(proc_json, json_list), total=len(json_list)))

        # Read results from detection and ground truths
        gt_dir = f'{outdir}/ground_truth'
        det_dir = f'{outdir}/detection'
        df_gt = pd.concat(map(pd.read_csv, glob.glob(f'{gt_dir}/*.csv')))
        df_det = pd.concat(map(pd.read_csv, glob.glob(f'{det_dir}/*.csv')))

        print("Calculating results and preparing result output...")
        # Get unique object classes
        obj_list = df_gt['object_class'].unique().tolist()

    # Go through each objects
    eval_result = []
    total_obj = 0
    total_tp = 0
    total_fn = 0
    total_fp = 0
    for each_obj in obj_list:
        # Calculate total ground truth object
        obj_amount = len(df_gt[df_gt['object_class'] == each_obj])
        total_obj += obj_amount

        # Calculate true positive
        tp_val = len(df_gt[(df_gt['object_class'] == each_obj) & (df_gt['gt_detected'] == True)])
        total_tp += tp_val

        # Calculate false negative
        fn_val = len(df_gt[(df_gt['object_class'] == each_obj) & (df_gt['gt_detected'] == False)])
        total_fn += fn_val

        # Calculate false positive
        fp_val = len(df_det[(df_det['object_class'] == each_obj) & (df_det['correct_detection'] == False)])
        total_fp += fp_val

        # Calculate precision, recall and f1 score
        try:
            obj_precision = round(tp_val/(tp_val + fp_val) * 100, 2)
        except ZeroDivisionError:
            obj_precision = 0
        obj_recall = round(tp_val/(tp_val + fn_val) * 100, 2)
        try:
            obj_f1 = round(2 * ((obj_precision * obj_recall) / (obj_precision + obj_recall)), 0)
        except ZeroDivisionError:
            obj_f1 = 0

        eval_result.append([each_obj, obj_amount, tp_val, fn_val, fp_val, obj_precision, obj_recall, obj_f1])

    # Calculate for overall objects
    try:
        overall_precision = round(total_tp/(total_tp + total_fp) * 100, 2)
    except ZeroDivisionError:
        overall_precision = 0
    overall_recall = round(total_tp / (total_tp + total_fn) * 100, 2)
    try:
        overall_f1 = round(2 * ((overall_precision * overall_recall) / (overall_precision + overall_recall)), 2)
    except ZeroDivisionError:
        overall_f1 = 0
    eval_result.append(["OVERALL OBJECTS", total_obj, total_tp,
                        total_fn, total_fp, overall_precision, overall_recall, overall_f1])

    model_basename = outdir.split('/')[-1]

    # Print out csv
    csv_outpath = f'{outdir}/eval_{model_basename}.csv'
    with open(csv_outpath, 'w', newline='') as csv_file:
        csv_reader = csv.writer(csv_file, delimiter=',')
        csv_reader.writerow(['Object Type', 'Object amount',
                             'True Positive', 'False Negative',
                             'False Positive', 'Precision',
                             'Recall', 'F1 Score'])
        for each_row in eval_result:
            csv_reader.writerow(each_row)

    # Plot graphs
    # Plot ground truths amount
    obj_list = [i[0] for i in eval_result][:-1]
    gt_amount_list = [i[1] for i in eval_result][:-1]
    bar_color = 'green'
    fig, ax = plt.subplots(figsize=(10, 5))
    plt.barh(obj_list, gt_amount_list, color=bar_color)
    plt.title(f'Ground Truth Amounts for {model_basename}', fontsize=20)
    plt.xlabel('Object Amount', fontsize=15, labelpad=15)
    plt.ylabel('Object Type', fontsize=15, labelpad=15)
    for i, v in enumerate(gt_amount_list):
        ax.text(v + 0.05, i, str(v), color='black', fontweight='bold', fontdict=dict(fontsize=10))
    plt.gcf().set_size_inches(25, 10)
    plt.savefig(f'{outdir}/1.Ground Truths_{model_basename}.png', dpi=100)
    plt.close()

    # Plot Precision amount
    precision_amount_list = [i[-3] for i in eval_result][:-1]
    bar_color = 'blue'
    fig, ax = plt.subplots(figsize=(10, 5))
    plt.barh(obj_list, precision_amount_list, color=bar_color)
    plt.title(f'Precision Percentage for {model_basename}', fontsize=20)
    plt.xlabel('Precision (%)', fontsize=15, labelpad=15)
    plt.xlim([0, 110])
    plt.ylabel('Object Type', fontsize=15, labelpad=15)
    for i, v in enumerate(precision_amount_list):
        ax.text(v + 0.05, i, str(v), color='black', fontweight='bold', fontdict=dict(fontsize=10))
    plt.gcf().set_size_inches(25, 10)
    plt.figtext(0.5, 0.01, f"Overall Precision: {overall_precision}%", ha="center", fontsize=18,
                bbox={"facecolor": bar_color, "alpha": 0.5, "pad": 5})
    plt.savefig(f'{outdir}/2.Precision_{model_basename}.png', dpi=100)
    plt.close()

    # Plot Recall amount
    recall_amount_list = [i[-2] for i in eval_result][:-1]
    bar_color = 'orange'
    fig, ax = plt.subplots(figsize=(10, 5))
    plt.barh(obj_list, recall_amount_list, color=bar_color)
    plt.title(f'Recall Percentage for {model_basename}', fontsize=20)
    plt.xlabel('Recall (%)', fontsize=15, labelpad=15)
    plt.xlim([0, 110])
    plt.ylabel('Object Type', fontsize=15, labelpad=15)
    for i, v in enumerate(recall_amount_list):
        ax.text(v + 0.05, i, str(v), color='black', fontweight='bold', fontdict=dict(fontsize=10))
    plt.gcf().set_size_inches(25, 10)
    plt.figtext(0.5, 0.01, f"Overall Recall: {overall_recall}%", ha="center", fontsize=18,
                bbox={"facecolor": bar_color, "alpha": 0.5, "pad": 5})
    plt.savefig(f'{outdir}/3.Recall_{model_basename}.png', dpi=100)
    plt.close()

    # Plot F1 amount
    f1_amount_list = [i[-1] for i in eval_result][:-1]
    bar_color = 'red'
    fig, ax = plt.subplots(figsize=(10, 5))
    plt.barh(obj_list, f1_amount_list, color=bar_color)
    plt.title(f'F1 Score for {model_basename}', fontsize=20)
    plt.xlabel('F1 Score (%)', fontsize=15, labelpad=15)
    plt.xlim([0, 110])
    plt.ylabel('Object Type', fontsize=15, labelpad=15)
    for i, v in enumerate(f1_amount_list):
        ax.text(v + 0.05, i, str(v), color='black', fontweight='bold', fontdict=dict(fontsize=10))
    plt.gcf().set_size_inches(25, 10)
    plt.figtext(0.5, 0.01, f"Overall F1 score: {overall_f1}%", ha="center", fontsize=18,
                bbox={"facecolor": bar_color, "alpha": 0.5, "pad": 5})
    plt.savefig(f'{outdir}/4.F1 Score_{model_basename}.png', dpi=100)
    plt.close()

    print("Finished!")

    # For Prediction Confidence
    # Get mean average and standard deviation for true and false positive prediction confidence
    tp_total_list = []
    tp_mean_list = []
    tp_stdev_list = []
    fp_total_list = []
    fp_mean_list = []
    fp_stdev_list = []
    for each_obj in obj_list:
        true_pos_cat = df_det[(df_det['object_class'] == each_obj) & (df_det['correct_detection'] == True)]
        false_pos_cat = df_det[(df_det['object_class'] == each_obj) & (df_det['correct_detection'] != True)]

        if len(true_pos_cat) > 0:
            tp_total_list.append(len(true_pos_cat))
            conf_score_mean = true_pos_cat['confidence_score'].mean()
            tp_mean_list.append(conf_score_mean)
            conf_score_stdev = true_pos_cat['confidence_score'].std()
            tp_stdev_list.append(conf_score_stdev)
        else:
            tp_total_list.append(0)
            tp_mean_list.append(0)
            tp_stdev_list.append(0)

        if len(false_pos_cat) > 0:
            fp_total_list.append(len(false_pos_cat))
            conf_score_mean = false_pos_cat['confidence_score'].mean()
            fp_mean_list.append(conf_score_mean)
            conf_score_stdev = false_pos_cat['confidence_score'].std()
            fp_stdev_list.append(conf_score_stdev)
        else:
            fp_total_list.append(0)
            fp_mean_list.append(0)
            fp_stdev_list.append(0)

    # Plot total true positive and false positive detection
    ind = np.arange(len(obj_list))  # the y locations for the groups
    width = 0.35  # the width of the bars
    bar_color_true = 'green'
    bar_color_false = 'red'

    fig, ax = plt.subplots(figsize=(20, 15))
    rects1 = ax.bar(ind, tp_total_list, width, color=bar_color_true)
    rects2 = ax.bar(ind + width, fp_total_list, width, color=bar_color_false)
    plt.title(f'True and False Positive Detections for {model_basename}')
    plt.xlabel('Object Type', labelpad=15)
    plt.ylabel('Detection Amount', labelpad=15)
    max_tp = max(tp_total_list)
    max_fp = max(fp_total_list)
    max_overall = max([max_tp, max_fp])

    max_number = 500
    while True:
        if max_overall > max_number:
            max_number += 500
        else:
            break

    plt.ylim([0, max_number])
    ax.set_xticks(ind + width / 2)
    ax.set_xticklabels(obj_list)
    plt.setp(ax.get_xticklabels(), rotation=45, ha='right')
    ax.legend((rects1[0], rects2[0]), ('True Positive', 'False Positive'))

    def autolabel_total(rects, val_list):
        for rect, value in zip(rects, val_list):
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width() / 2., 1.05 * height,
                    str(value),
                    ha='center', va='bottom')

    autolabel_total(rects1, tp_total_list)
    autolabel_total(rects2, fp_total_list)

    plt.savefig(f'{outdir}/5.Total Detection_{model_basename}.png', dpi=100)
    plt.close()

    # Plot mean confidence score for true positive and false positive detection
    ind = np.arange(len(obj_list))  # the y locations for the groups
    width = 0.35  # the width of the bars
    bar_color_true = 'green'
    bar_color_false = 'red'

    fig, ax = plt.subplots(figsize=(20, 15))
    rects1 = ax.bar(ind, tp_mean_list, width, color=bar_color_true, yerr=tp_stdev_list)
    rects2 = ax.bar(ind + width, fp_mean_list, width, color=bar_color_false, yerr=fp_stdev_list)
    plt.title(f'Average Confidence Score for {model_basename}')
    plt.xlabel('Object Type', labelpad=15)
    plt.ylabel('Confidence Score Mean', labelpad=15)
    plt.ylim([0, 110])
    ax.set_xticks(ind + width / 2)
    ax.set_xticklabels(obj_list)
    plt.setp(ax.get_xticklabels(), rotation=45, ha='right')
    ax.legend((rects1[0], rects2[0]), ('True Positive', 'False Positive'))

    def autolabel_mean(rects, val_list, stdev_list):
        for rect, value, stdev in zip(rects, val_list, stdev_list):
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width() / 2., 1.05 * height,
                    f'{value:.2f} ± {stdev:.2f}',
                    ha='center', va='bottom')

    autolabel_mean(rects1, tp_mean_list, tp_stdev_list)
    autolabel_mean(rects2, fp_mean_list, fp_stdev_list)

    plt.savefig(f'{outdir}/6.Average Confidence Score_{model_basename}.png', dpi=100)
    plt.close()


def run_multiple():
    # URL Definition
    url_ver4 = "http://localhost:8001/landmarkVerify_v4/single"
    url_ver3 = "http://52.77.71.52:8005/detectDocumentLandmarkV3_3"

    # Front mykad
    doc_id = 2
    model_versions = [9, 10, 11, 5, 8, 7, 4]
    out_list = ['front_iou0.5_conf10_ssdmobilenet_v3',
                'front_iou0.5_conf10_ssdmobilenetsmall_v3',
                'front_iou0.5_conf10_ssdresnet_v3',
                'front_iou0.5_conf10_ssdmobilenet_v2',
                'front_iou0.5_conf10_ssdmobilenetsmall_v2',
                'front_iou0.5_conf10_ssdresnet_v2',
                'front_iou0.5_conf10_fasterrcnn_v2']
    out_list = [f'/data/hanif/landmark_dataset/iou_output/{i}' for i in out_list]
    indir = '/data/hanif/landmark_dataset/dataset_v4.0/front_mykad/test'

    total_model = len(model_versions)
    ind_list = [i for i, _ in enumerate(model_versions, start=1)]
    for idx, each_mod, each_out in zip(ind_list, model_versions, out_list):
        print("Evaluating Front Mykad model")
        model_name = each_out.split('/')[-1]
        print(f"Model name: {model_name}")
        print(f"Model {idx} out ot {total_model}")
        evaluate_api(url_str=url_ver4,
                     indir=indir,
                     outdir=each_out,
                     doc_id=doc_id,
                     mod_ver=each_mod,
                     conf_thresh=THRESH_CONFIDENCE,
                     iou_max=THRESH_IOU)

    # Back mykad
    doc_id = 3
    model_versions = [5, 6, 7, 2, 3, 4]
    out_list = ['back_iou0.5_conf10_ssdmobilenet_v3',
                'back_iou0.5_conf10_ssdmobilenetsmall_v3',
                'back_iou0.5_conf10_ssdresnet_v3',
                'back_iou0.5_conf10_ssdmobilenet_v2',
                'back_iou0.5_conf10_ssdmobilenetsmall_v2',
                'back_iou0.5_conf10_ssdresnet_v2'
                ]
    out_list = [f'/data/hanif/landmark_dataset/iou_output/{i}' for i in out_list]
    indir = '/data/hanif/landmark_dataset/dataset_v4.0/back_mykad/test'

    total_model = len(model_versions)
    ind_list = [i for i, _ in enumerate(model_versions, start=1)]
    for idx, each_mod, each_out in zip(ind_list, model_versions, out_list):
        print("Evaluating Back Mykad model")
        model_name = each_out.split('/')[-1]
        print(f"Model name: {model_name}")
        print(f"Model {idx} out ot {total_model}")
        evaluate_api(url_str=url_ver4,
                     indir=indir,
                     outdir=each_out,
                     doc_id=doc_id,
                     mod_ver=each_mod,
                     conf_thresh=THRESH_CONFIDENCE,
                     iou_max=THRESH_IOU)

    # Evaluate using previous API version
    print("Evaluating Version 3.3 Front Mykad")
    indir = '/data/hanif/landmark_dataset/dataset_v4.0/front_mykad/test'
    outdir = f'/data/hanif/landmark_dataset/iou_output/front_iou0.5_conf10_apiver3.3'
    doc_id = 2

    evaluate_api(url_str=url_ver3,
                 indir=indir,
                 outdir=outdir,
                 doc_id=doc_id,
                 mod_ver=1,
                 conf_thresh=THRESH_CONFIDENCE,
                 iou_max=THRESH_IOU)
    print("Evaluating Version 3.3 Back Mykad")
    indir = '/data/hanif/landmark_dataset/dataset_v4.0/back_mykad/test'
    outdir = f'/data/hanif/landmark_dataset/iou_output/back_iou0.5_conf10_apiver3.3'
    doc_id = 3
    evaluate_api(url_str=url_ver3,
                 indir=indir,
                 outdir=outdir,
                 doc_id=doc_id,
                 mod_ver=1,
                 conf_thresh=THRESH_CONFIDENCE,
                 iou_max=THRESH_IOU)


def run_multiple_onedrive():
    # Front mykad
    indir = 'F:/Xendity/Project_LandmarkMyKad/data/OneDrive_2022-03-08/Landmark images/Front Mykad'
    doc_id = 2
    url_ver3 = 'http://52.77.71.52:8005/detectDocumentLandmarkV3_3'
    url_ver4 = "http://localhost:8001/landmarkVerify_v4/single"
    out_ver3 = 'F:/Xendity/Project_LandmarkMyKad/data/onedrive_output/front_iou0.5_conf10_v3.3'
    model_versions = [7, 5, 8, 9, 10, 11]
    out_list = ['front_iou0.5_conf10_ssdresnetv2',
                'front_iou0.5_conf10_ssdmobilenetv2',
                'front_iou0.5_conf10_ssdmobilenetsmallv2',
                'front_iou0.5_conf10_fasterrcnnv2',
                'front_iou0.5_conf10_ssdmobilenetv3',
                'front_iou0.5_conf10_ssdmobilenetsmallv3',
                'front_iou0.5_conf10_ssdresnetv3']
    out_list = [f'F:/Xendity/Project_LandmarkMyKad/data/onedrive_output/{i}' for i in out_list]

    evaluate_api(url_str=url_ver3,
                 indir=indir,
                 outdir=out_ver3,
                 doc_id=doc_id,
                 mod_ver=1,
                 conf_thresh=THRESH_CONFIDENCE,
                 iou_max=THRESH_IOU)

    for each_mod, each_out in zip(model_versions, out_list):
        evaluate_api(url_str=url_ver4,
                     indir=indir,
                     outdir=each_out,
                     doc_id=doc_id,
                     mod_ver=each_mod,
                     conf_thresh=THRESH_CONFIDENCE,
                     iou_max=THRESH_IOU)

    # Back mykad
    doc_id = 3
    model_versions = [2, 3, 4, 5, 6, 7]
    out_list = ['back_iou0.5_conf10_ssdmobilenetv2',
                'back_iou0.5_conf10_ssdmobilenetsmallv2',
                'back_iou0.5_conf10_ssdresnetv2',
                'back_iou0.5_conf10_ssdmobilenetv3',
                'back_iou0.5_conf10_ssdmobilenetsmallv3',
                'back_iou0.5_conf10_ssdresnetv3']
    out_list = [f'F:/Xendity/Project_LandmarkMyKad/data/onedrive_output/{i}' for i in out_list]
    out_ver3 = 'F:/Xendity/Project_LandmarkMyKad/data/onedrive_output/back_iou0.5_conf10_v3.3'
    indir = 'F:/Xendity/Project_LandmarkMyKad/data/OneDrive_2022-03-08/Landmark images/Back Mykad'

    evaluate_api(url_str=url_ver3,
                 indir=indir,
                 outdir=out_ver3,
                 doc_id=doc_id,
                 mod_ver=1,
                 conf_thresh=THRESH_CONFIDENCE,
                 iou_max=THRESH_IOU)

    for each_mod, each_out in zip(model_versions, out_list):
        evaluate_api(url_str=url_ver4,
                     indir=indir,
                     outdir=each_out,
                     doc_id=doc_id,
                     mod_ver=each_mod,
                     conf_thresh=THRESH_CONFIDENCE,
                     iou_max=THRESH_IOU)


def main():
    evaluate_api(url_str=URL,
                 indir=TEST_IN_DIR,
                 outdir=RESULT_OUT_DIR,
                 doc_id=DOC_TYPE,
                 mod_ver=MODEL_VER,
                 conf_thresh=THRESH_CONFIDENCE,
                 iou_max=THRESH_IOU)


if __name__ == "__main__":
    # main()
    run_multiple()
    # run_multiple_onedrive()