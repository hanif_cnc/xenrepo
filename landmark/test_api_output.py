import os
import csv
import json
import requests
from concurrent.futures import ThreadPoolExecutor
import base64
from tqdm import tqdm

URL = "http://localhost:8001/landmarkVerify_v4/single"
IMG_IN = "F:/Xendity/Project_LandmarkMyKad/data/OneDrive_2022-03-08/Landmark images/Front Mykad"
# IMG_IN = "F:/Xendity/Project_LandmarkMyKad/data/OneDrive_2022-03-08/Landmark images/Back Mykad"
# IMG_IN = "F:/Xendity/Project_LandmarkMyKad/data/67_dataset/front_mykad/test"
IMG_OUT = "F:/Xendity/Project_LandmarkMyKad/data/onedrive_out/front_mykad_test_ssdv2"
DOC_TYPE = 2
MODEL_VER = 5
THRESH_SET = 10


def save_img(save_path, b64_img):
    imgdata = base64.b64decode(b64_img)
    filename = save_path
    with open(filename, 'wb') as f:
        f.write(imgdata)


def proc_img(img_path):
    req_json = {'imageFile': open(img_path, 'rb'),
                'docType': (None, f'{DOC_TYPE}'),
                'modelVer': (None, f'{MODEL_VER}'),
                'detThresh': (None, f'{THRESH_SET}'),
                'outImg': (None, 'True')}

    response = requests.post(URL, files=req_json, timeout=1000)

    if response.status_code == 200:
        # Img output path
        img_base = img_path.split('/')[-1]
        img_outpath = f'{IMG_OUT}/{img_base}'

        body = json.loads(response.text)

        save_img(img_outpath, body['prediction_img'])

        body = json.loads(response.text)
        det_json = body['data']

        det_dt = []
        for each_id, each_shape in enumerate(det_json):
            det_dt.append([str(each_shape['landmark']), int(each_shape['xmin']),
                           int(each_shape['xmax']), int(each_shape['ymin']),
                           int(each_shape['ymax']), float(each_shape['confidence'])])

        img_basename = img_path.split('/')[-1].replace('.PNG', '')
        text_out = f'{IMG_OUT}/{img_basename}.csv'

        with open(text_out, 'w', newline='') as csv_file:
            csv_reader = csv.writer(csv_file, delimiter=',')
            csv_reader.writerow(['object_class', 'xmin', 'xmax', 'ymin', 'ymax', 'confidence'])
            for each_row in det_dt:
                csv_reader.writerow(each_row)

    else:
        img_base = img_path.split('/')[-1]
        print(f"ERROR: Cannot output image {img_base}")


# prep output folder
os.makedirs(IMG_OUT, exist_ok=True)

# Get all image path
img_list = [f'{IMG_IN}/{i}' for i in os.listdir(IMG_IN) if i.endswith(('.jpg', '.jpeg', '.png', '.PNG'))]

with ThreadPoolExecutor(max_workers=4) as executor:
    list(tqdm(executor.map(proc_img, img_list), total=len(img_list)))
