"""
Splits original and augmented datasets into Training, Test and Validation sets.
Identify front and back Mykad json based on FRONT_MYKAD_PARAMS and BACK_MYKAD_PARAMS
One particular class defined in SPECIAL_PARAM will be split separately due to
its small amount (ensures even distribution)
The image relating to the json is determined from json basename
The image file should be in the same folder as the json file
"""
#######################################
import os
import shutil
import json
from tqdm import tqdm
from concurrent import futures
import random

# Settings
ORIGINAL_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/66-dataset'
AUGMENTED_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/augmented_data'
OUTPUT_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/67_dataset'
FRONT_MYKAD_PARAMS = ['my_address', 'my_avatar', 'my_flag', 'my_gender',
                      'my_ghost', 'my_hibiscus', 'my_icdigits',
                      'my_islam', 'my_kadpengenalanmalaysia',
                      'my_msc', 'my_mykadlogo', 'my_name',
                      'my_securitychip', 'my_warganegara']
BACK_MYKAD_PARAMS = ['my_80kchip', 'my_atm', 'my_back_ic_no', 'my_coatofarm',
                     'my_ketuapengarah', 'my_klcctower', 'my_malaysiaword',
                     'my_pendaftarannegara', 'my_serialnumber', 'my_signature',
                     'my_tengkolok', 'my_touchngo']
SPECIAL_PARAM = "my_back_ic_id"
SPECIAL_PARAM_BACK = True  # Declare if special parameter json belongs to back of MyKad
TRAIN_TEST_VAL_DIST = [70, 15, 15]  # Train, Test and Validation distribution percent, integer and summed to 100
IMG_EXT_LIST = ('.jpg', '.png', '.jpeg')  # image extension. Please include stop


def calc_data_separate(total_amount, dist_list):
    train = int(total_amount * (dist_list[0]/100))
    test = int(total_amount * (dist_list[1]/100))

    return train, test


def copy_files(each_proc):
    input_json = each_proc[0]
    input_img = each_proc[1]
    outdir_file = each_proc[2]

    json_basename = input_json.split('/')[-1]
    img_basename = input_img.split('/')[-1]

    output_json = f'{outdir_file}/{json_basename}'
    output_img = f'{outdir_file}/{img_basename}'

    shutil.copy(input_json, output_json)
    shutil.copy(input_img, output_img)


def spilt_mykad_data(ori_dir: str,
                     aug_dir: str,
                     out_dir: str,
                     front_param: list,
                     back_param: list,
                     spe_param: str,
                     spe_back: bool,
                     dist_list: list,
                     img_ext: tuple):
    """
    Split json and images of front and back MyKad to Train, Test and Validation
    :param ori_dir: parent directory of original json and images
    :param aug_dir: parent directory of augmented json and images
    :param out_dir: parent directory of output dir
    :param front_param: parameters classified as front mykad
    :param back_param: parameters classified as back mykad
    :param spe_param: special parameter for its own division
    :param spe_back: declare if special parameter belongs to Back Mykad
    :param dist_list: Train, Test and Validation distribution percentage
    :param img_ext: tuple of possible image extension
    """
    #########################################################
    """
    Prepare environment 
    """
    # Ensure path formats are correct
    ori_dir = ori_dir.replace('\\', '/')
    aug_dir = aug_dir.replace('\\', '/')
    out_dir = out_dir.replace('\\', '/')

    # Check if train, validation and test distribution is correct
    train_dist, test_dist, val_dist = dist_list
    if not train_dist + test_dist + val_dist == 100:
        print("ERROR: Train, test and validation distribution are incorrect. Ensure it is summed to 100")
        return False

    # Prepare output folders
    outdir_front_train = f'{out_dir}/front_mykad/train'
    outdir_front_test = f'{out_dir}/front_mykad/test'
    outdir_front_val = f'{out_dir}/front_mykad/val'
    outdir_back_train = f'{out_dir}/back_mykad/train'
    outdir_back_test = f'{out_dir}/back_mykad/test'
    outdir_back_val = f'{out_dir}/back_mykad/val'
    os.makedirs(outdir_front_train, exist_ok=True)
    os.makedirs(outdir_front_test, exist_ok=True)
    os.makedirs(outdir_front_val, exist_ok=True)
    os.makedirs(outdir_back_train, exist_ok=True)
    os.makedirs(outdir_back_test, exist_ok=True)
    os.makedirs(outdir_back_val, exist_ok=True)

    #########################################################
    """
    Multithreading functions inside this function to share 
    same variable
    """
    def categorize_json(each_json):
        """
        :param each_json: path to json
        :return:
        list with following:
        1) Path to json (str)
        2) True: landmark is special, False: landmark is not special
        3) True: Front Mykad, False: Back MyKad
        4) Path to image (str)
        OR
        return 0 if no no image is found or not within MyKad parameters
        """
        # Check if image can be found within the same folder as json
        basename = each_json.split('/')[-1]
        basename_nojson = basename.replace('.json', '')
        folder_path = each_json.replace(f'/{basename}', '')

        img_exist = [f'{folder_path}/{basename_nojson}{i}' for i in img_ext if
                     os.path.exists(f'{folder_path}/{basename_nojson}{i}')]

        if not img_exist:
            return 0

        # If image can be found, to check the landmarks in json
        with open(each_json, 'r') as f:
            data = json.load(f)
            data_shape = data['shapes']
            json_landmark = [i['label'] for i in data_shape]

            # Check parameters in JSON landmark
            if spe_param in json_landmark:
                if spe_back:
                    # Special JSON belongs to Back MyKad
                    val_return = [each_json, True, False, img_exist[0]]
                else:
                    # Special JSON belongs to Front MyKad
                    val_return = [each_json, True, True, img_exist[0]]

            else:
                # Check for front MyKad
                if not set(json_landmark).isdisjoint(front_param):
                    val_return = [each_json, False, True, img_exist[0]]
                # Check for back MyKad
                elif not set(json_landmark).isdisjoint(back_param):
                    val_return = [each_json, False, False, img_exist[0]]
                # None of the parameters belong to MyKad
                else:
                    val_return = 0

        return val_return

    #########################################################
    """
    Process for original dataset 
    """
    print(f"Processing Original Dataset in {ori_dir}...")
    # Gather paths of all json
    print("Gathering json paths")
    all_json_paths = []
    for roots, dirs, files in os.walk(ori_dir):
        for file in files:
            if file.endswith('.json'):
                json_path = f'{roots}/{file}'
                json_path = json_path.replace('\\', '/')
                all_json_paths.append(json_path)

    print(f"Total json found: {len(all_json_paths):,}")

    # Categorize jsons
    with futures.ThreadPoolExecutor() as executor:
        print("Categorizing json")
        cat_json_list = list(tqdm(executor.map(categorize_json,
                                               all_json_paths),
                                  total=len(all_json_paths)))

        cat_json_list = [i for i in cat_json_list if i]

        # Separate json with special param, front and back mykad
        spe_json_list = [i for i in cat_json_list if i[1]]
        non_spe_json_list = [i for i in cat_json_list if not i[1]]
        front_json_list = [i for i in non_spe_json_list if i[2]]
        back_json_list = [i for i in non_spe_json_list if not i[2]]
        total_spe = len(spe_json_list)
        total_front = len(front_json_list)
        total_back = len(back_json_list)
        print(f"Total special parameter json: {total_spe:,}")
        print(f"Total front MyKad json: {total_front:,}")
        print(f"Total back MyKad json: {total_back:,}")

        # Divide to train, test, val for spe
        train_spe_split, test_spe_split = calc_data_separate(total_spe, dist_list)
        random.shuffle(spe_json_list)
        train_spe_list = spe_json_list[:train_spe_split]
        spe_json_list = spe_json_list[train_spe_split:]
        test_spe_list = spe_json_list[:test_spe_split]
        val_spe_list = spe_json_list[test_spe_split:]

        # Copy spe files to specified output
        if spe_back:
            train_spe_list = [[i[0], i[-1], outdir_back_train] for i in train_spe_list]
            test_spe_list = [[i[0], i[-1], outdir_back_test] for i in test_spe_list]
            val_spe_list = [[i[0], i[-1], outdir_back_val] for i in val_spe_list]
        else:
            train_spe_list = [[i[0], i[-1], outdir_front_train] for i in train_spe_list]
            test_spe_list = [[i[0], i[-1], outdir_front_test] for i in test_spe_list]
            val_spe_list = [[i[0], i[-1], outdir_front_val] for i in val_spe_list]
        print("Copying json with special parameter to train")
        list(tqdm(executor.map(copy_files, train_spe_list), total=len(train_spe_list)))
        print("Copying json with special parameter to test")
        list(tqdm(executor.map(copy_files, test_spe_list), total=len(test_spe_list)))
        print("Copying json with special parameter to validation")
        list(tqdm(executor.map(copy_files, val_spe_list), total=len(val_spe_list)))

        # Divide to train, test, val for front MyKad
        train_front_split, test_front_split = calc_data_separate(total_front, dist_list)
        random.shuffle(front_json_list)
        train_front_list = front_json_list[:train_front_split]
        front_json_list = front_json_list[train_front_split:]
        test_front_list = front_json_list[:test_front_split]
        val_front_list = front_json_list[test_front_split:]
        train_front_list = [[i[0], i[-1], outdir_front_train] for i in train_front_list]
        test_front_list = [[i[0], i[-1], outdir_front_test] for i in test_front_list]
        val_front_list = [[i[0], i[-1], outdir_front_val] for i in val_front_list]
        print("Copying front MyKad to train")
        list(tqdm(executor.map(copy_files, train_front_list), total=len(train_front_list)))
        print("Copying front Mykad to test")
        list(tqdm(executor.map(copy_files, test_front_list), total=len(test_front_list)))
        print("Copying front MyKad to validation")
        list(tqdm(executor.map(copy_files, val_front_list), total=len(val_front_list)))

        # Divide to train, test, val for back MyKad
        train_back_split, test_back_split = calc_data_separate(total_back, dist_list)
        random.shuffle(back_json_list)
        train_back_list = back_json_list[:train_back_split]
        back_json_list = back_json_list[train_back_split:]
        test_back_list = back_json_list[:test_back_split]
        val_back_list = back_json_list[test_back_split:]
        train_back_list = [[i[0], i[-1], outdir_back_train] for i in train_back_list]
        test_back_list = [[i[0], i[-1], outdir_back_test] for i in test_back_list]
        val_back_list = [[i[0], i[-1], outdir_back_val] for i in val_back_list]
        print("Copying back MyKad to train")
        list(tqdm(executor.map(copy_files, train_back_list), total=len(train_back_list)))
        print("Copying back Mykad to test")
        list(tqdm(executor.map(copy_files, test_back_list), total=len(test_back_list)))
        print("Copying back MyKad to validation")
        list(tqdm(executor.map(copy_files, val_back_list), total=len(val_back_list)))

    #########################################################
    """
    Process for augmented dataset
    """
    print(f"Processing Augmented Dataset in {aug_dir}...")
    # Gather paths of all json
    print("Gathering json paths")
    all_json_paths = []
    for roots, dirs, files in os.walk(aug_dir):
        for file in files:
            if file.endswith('.json'):
                json_path = f'{roots}/{file}'
                json_path = json_path.replace('\\', '/')
                all_json_paths.append(json_path)

    print(f"Total json found: {len(all_json_paths):,}")

    # Categorize jsons
    with futures.ThreadPoolExecutor() as executor:
        print("Categorizing json")
        cat_json_list = list(tqdm(executor.map(categorize_json,
                                               all_json_paths),
                                  total=len(all_json_paths)))

        cat_json_list = [i for i in cat_json_list if i]

        # Separate json with special param, front and back mykad
        spe_json_list = [i for i in cat_json_list if i[1]]
        non_spe_json_list = [i for i in cat_json_list if not i[1]]
        front_json_list = [i for i in non_spe_json_list if i[2]]
        back_json_list = [i for i in non_spe_json_list if not i[2]]
        total_spe = len(spe_json_list)
        total_front = len(front_json_list)
        total_back = len(back_json_list)
        print(f"Total special parameter json: {total_spe:,}")
        print(f"Total front MyKad json: {total_front:,}")
        print(f"Total back MyKad json: {total_back:,}")

        # Divide to train, test, val for spe
        train_spe_split, test_spe_split = calc_data_separate(total_spe, dist_list)
        random.shuffle(spe_json_list)
        train_spe_list = spe_json_list[:train_spe_split]
        spe_json_list = spe_json_list[train_spe_split:]
        test_spe_list = spe_json_list[:test_spe_split]
        val_spe_list = spe_json_list[test_spe_split:]

        # Copy spe files to specified output
        if spe_back:
            train_spe_list = [[i[0], i[-1], outdir_back_train] for i in train_spe_list]
            test_spe_list = [[i[0], i[-1], outdir_back_test] for i in test_spe_list]
            val_spe_list = [[i[0], i[-1], outdir_back_val] for i in val_spe_list]
        else:
            train_spe_list = [[i[0], i[-1], outdir_front_train] for i in train_spe_list]
            test_spe_list = [[i[0], i[-1], outdir_front_test] for i in test_spe_list]
            val_spe_list = [[i[0], i[-1], outdir_front_val] for i in val_spe_list]
        print("Copying json with special parameter to train")
        list(tqdm(executor.map(copy_files, train_spe_list), total=len(train_spe_list)))
        print("Copying json with special parameter to test")
        list(tqdm(executor.map(copy_files, test_spe_list), total=len(test_spe_list)))
        print("Copying json with special parameter to validation")
        list(tqdm(executor.map(copy_files, val_spe_list), total=len(val_spe_list)))

        # Divide to train, test, val for front MyKad
        train_front_split, test_front_split = calc_data_separate(total_front, dist_list)
        random.shuffle(front_json_list)
        train_front_list = front_json_list[:train_front_split]
        front_json_list = front_json_list[train_front_split:]
        test_front_list = front_json_list[:test_front_split]
        val_front_list = front_json_list[test_front_split:]
        train_front_list = [[i[0], i[-1], outdir_front_train] for i in train_front_list]
        test_front_list = [[i[0], i[-1], outdir_front_test] for i in test_front_list]
        val_front_list = [[i[0], i[-1], outdir_front_val] for i in val_front_list]
        print("Copying front MyKad to train")
        list(tqdm(executor.map(copy_files, train_front_list), total=len(train_front_list)))
        print("Copying front Mykad to test")
        list(tqdm(executor.map(copy_files, test_front_list), total=len(test_front_list)))
        print("Copying front MyKad to validation")
        list(tqdm(executor.map(copy_files, val_front_list), total=len(val_front_list)))

        # Divide to train, test, val for back MyKad
        train_back_split, test_back_split = calc_data_separate(total_back, dist_list)
        random.shuffle(back_json_list)
        train_back_list = back_json_list[:train_back_split]
        back_json_list = back_json_list[train_back_split:]
        test_back_list = back_json_list[:test_back_split]
        val_back_list = back_json_list[test_back_split:]
        train_back_list = [[i[0], i[-1], outdir_back_train] for i in train_back_list]
        test_back_list = [[i[0], i[-1], outdir_back_test] for i in test_back_list]
        val_back_list = [[i[0], i[-1], outdir_back_val] for i in val_back_list]
        print("Copying back MyKad to train")
        list(tqdm(executor.map(copy_files, train_back_list), total=len(train_back_list)))
        print("Copying back Mykad to test")
        list(tqdm(executor.map(copy_files, test_back_list), total=len(test_back_list)))
        print("Copying back MyKad to validation")
        list(tqdm(executor.map(copy_files, val_back_list), total=len(val_back_list)))
    #########################################################
    print("")
    print("Finished copying process")
    print("Checking copy job")
    # Check copy conditions
    front_train_json_amt = len([i for i in os.listdir(outdir_front_train) if i.endswith(".json")])
    front_train_img_amt = len([i for i in os.listdir(outdir_front_train) if i.endswith(img_ext)])
    front_test_json_amt = len([i for i in os.listdir(outdir_front_test) if i.endswith(".json")])
    front_test_img_amt = len([i for i in os.listdir(outdir_front_test) if i.endswith(img_ext)])
    front_val_json_amt = len([i for i in os.listdir(outdir_front_val) if i.endswith(".json")])
    front_val_img_amt = len([i for i in os.listdir(outdir_front_val) if i.endswith(img_ext)])

    back_train_json_amt = len([i for i in os.listdir(outdir_back_train) if i.endswith(".json")])
    back_train_img_amt = len([i for i in os.listdir(outdir_back_train) if i.endswith(img_ext)])
    back_test_json_amt = len([i for i in os.listdir(outdir_back_test) if i.endswith(".json")])
    back_test_img_amt = len([i for i in os.listdir(outdir_back_test) if i.endswith(img_ext)])
    back_val_json_amt = len([i for i in os.listdir(outdir_back_val) if i.endswith(".json")])
    back_val_img_amt = len([i for i in os.listdir(outdir_back_val) if i.endswith(img_ext)])

    cond_front_train = front_train_json_amt == front_train_img_amt
    cond_front_test = front_test_json_amt == front_test_img_amt
    cond_front_val = front_val_json_amt == front_val_img_amt
    cond_back_train = back_train_json_amt == back_train_img_amt
    cond_back_test = back_test_json_amt == back_test_img_amt
    cond_back_val = back_val_json_amt == back_val_img_amt

    if cond_front_train and cond_front_test and cond_front_val and cond_back_train and cond_back_test and cond_back_val:
        overall_cond = True
    else:
        overall_cond = False

    print("")
    print("---Front MyKad----")
    print(f"Front train: image {front_train_json_amt:,}, json {front_train_img_amt:,}, {cond_front_train}")
    print(f"Front test: image {front_test_json_amt:,}, json {front_test_img_amt:,}, {cond_front_test}")
    print(f"Front validation: image {front_val_json_amt:,}, json {front_val_img_amt:,}, {cond_front_val}")
    print("---Back MyKad----")
    print(f"Back train: image {back_train_json_amt:,}, json {back_train_img_amt:,}, {cond_back_train}")
    print(f"Back test: image {back_test_json_amt:,}, json {back_test_img_amt:,}, {cond_back_test}")
    print(f"Back validation: image {back_val_json_amt:,}, json {back_val_img_amt:,}, {cond_back_val}")
    print("---Overall status----")
    print(f"All conditions: {overall_cond}")

    if overall_cond:
        return True
    else:
        return False


def main():
    status_split = spilt_mykad_data(ORIGINAL_DIR,
                                    AUGMENTED_DIR,
                                    OUTPUT_DIR,
                                    FRONT_MYKAD_PARAMS,
                                    BACK_MYKAD_PARAMS,
                                    SPECIAL_PARAM,
                                    SPECIAL_PARAM_BACK,
                                    TRAIN_TEST_VAL_DIST,
                                    IMG_EXT_LIST)
    if status_split:
        print("Split process finished succesfully!")
    else:
        print("ERROR: Split process failed to finish successfully.")

    return True


if __name__ == '__main__':
    main()
