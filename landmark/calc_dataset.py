"""
Get statistics for Front and Back Mykad dataset.
All calculations are based on JSON files only.
Ensure folder path in DATA_DIR are as follows

folder specified in DATA_DIR
    |_front_mykad
    |   |_test
    |   |_train
    |   |_val
    |_back_mykad
        |_test
        |_train
        |_val
"""
import os
import json
from tqdm import tqdm
from collections import Counter


# Settings
DATA_DIR = 'F:/Xendity/Project_LandmarkMyKad/data/67_dataset'
AUG_IDENTIFIER = ['_patches', '_flags', '_plain']
OUT_REPORT = '67_dataset_statistics.txt'


def cal_json(dataset_dir: str, aug_id: list, outpath: str):
    """
    :param dataset_dir: path to each dataset
    :param aug_id: list containing augmentation identifier
    :param outpath: path to report text
    """
    # Gather json
    json_list = []
    for file in os.listdir(dataset_dir):
        if file.endswith('.json'):
            json_path = f'{dataset_dir}/{file}'
            json_list.append(json_path)

    total_json = len(json_list)

    # Get augmented amount
    aug_list = [i for i in json_list if any(e in i for e in aug_id)]
    total_aug = len(aug_list)
    total_non_aug = total_json - total_aug

    # Read and store amount of landmarks
    all_landmarks = []
    for each_json in tqdm(json_list):
        with open(each_json, 'r') as f:
            data = json.load(f)
            data_shape = data['shapes']
            json_landmark = [i['label'] for i in data_shape]

        if json_landmark:
            all_landmarks.append(json_landmark)

    # Flatten list
    all_landmarks = [item for sublist in all_landmarks for item in sublist]

    # Calculate elements in list
    total_landmark = Counter(all_landmarks).keys()
    total_amt_landmark = Counter(all_landmarks).values()

    # Append result into text output
    with open(outpath, "a") as file:
        file.write(f"Total JSON: {total_json:,}\n")
        file.write(f"Total Augmented JSON: {total_aug:,}\n")
        file.write(f"Total Non-augmented JSON: {total_non_aug:,}\n")
        file.write(f"Total landmarks: {len(total_landmark)}\n")
        file.write(f"Amount for each landmark:-\n")

        for each_key, each_landmark in zip(total_landmark, total_amt_landmark):
            file.write(f"   {each_key}: {each_landmark:,}\n")


def get_stats(dirpath: str, aug_id_list: list, outpath: str):
    """
    :param dirpath: Directory containing front and back mykad
    :param aug_id_list: list containing augmentation identifier
    :param outpath: Output path for report text
    """
    # Get all dataset path
    dirpath = dirpath.replace('\\', '/')
    dataset_list = [f'{dirpath}/front_mykad', f'{dirpath}/back_mykad']
    folder_list = ['train', 'test', 'val']

    # Prepare output report
    with open(outpath, "w") as file:
        file.write("DATASET STATISTICS\n")
        file.write("\n")
        file.write("---FRONT MYKAD---\n")

    # Evaluate front mykad
    for each_type in folder_list:
        with open(outpath, "a") as file:
            file.write(f"{each_type} dataset\n")
        each_dataset = f'{dataset_list[0]}/{each_type}'
        print(f"processing for {each_dataset}")
        cal_json(each_dataset, aug_id_list, outpath)
        with open(outpath, "a") as file:
            file.write(f"-----------------------\n")

    # Evaluate back mykad
    with open(outpath, "a") as file:
        file.write("\n")
        file.write("---BACK MYKAD---\n")
    for each_type in folder_list:
        with open(outpath, "a") as file:
            file.write(f"{each_type} dataset\n")
        each_dataset = f'{dataset_list[1]}/{each_type}'
        print(f"processing for {each_dataset}")
        cal_json(each_dataset, aug_id_list, outpath)
        with open(outpath, "a") as file:
            file.write(f"-----------------------\n")





def main():
    get_stats(DATA_DIR, AUG_IDENTIFIER, OUT_REPORT)


if __name__ == '__main__':
    main()
